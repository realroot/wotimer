TARGET = wotimer
TEMPLATE = app

HEADERS += src/project.h
SOURCES += src/main.cpp src/project.cpp

QT += qml quick widgets #quickcontrols2

RESOURCES += src/resources.qrc

DISTFILES += \
    README.md

android: ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

contains(ANDROID_TARGET_ARCH, aarch64) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
    ANDROID_ABI = aarch64
}

#ANDROID_ABIS = aarch64
