# wotimer

QML timer with WorkOuts in mind.

Each project is one or more sets of sequentially countdown timers with alarm and ending animation.

Each timer can be automatically started after the previous one and have an additional alarm when it is close to the end.

Sets can be run multiple times.

Optional initial countdown for each project.

It can be use used also for cooking, delimiting computer time/breaks etc.

# Compiling

```
qmake6
make
```

# Packaging

## pacman (Artix/Arch Linux)

[wotimer](https://codeberg.org/realroot/artix-pkgbuilds/src/branch/main/wotimer/PKGBUILD)

[wotimer-git](https://codeberg.org/realroot/artix-pkgbuilds/src/branch/main/wotimer-git/PKGBUILD) (may contains experimental features that cause bugs)

## apk (Alpine Linux)
[wotimer](https://codeberg.org/realroot/apkbuilds/src/branch/main/wotimer/APKBUILD)

[wotimer-git](https://codeberg.org/realroot/apkbuilds/src/branch/main/wotimer-git/APKBUILD)


# Extra projects

To add extra [Darebee](https://darebee.com)©'s projects that cannot be included here because of the licensing you can use the [wotimer-darebee-config](https://codeberg.org/realroot/wotimer-darebee-config) repository.

# Theming

By default the app uses the system theme with an option to use a built-in black theme.

# Screenshots

![wotimer](https://codeberg.org/realroot/wotimer/raw/branch/main/screenshots/wotimer-example.png)

![wotimer-initial-countdown](https://codeberg.org/realroot/wotimer/raw/branch/main/screenshots/wotimer-initial-countdown.png)

# Shortcuts

```
Ctrl + Space           Confirm, play/pause timer
Ctrl + Enter           Confirm, play/pause timer
Space                  Confirm, play/pause timer
Enter                  Confirm, play/pause timer
Esc                    Quit page/app, confirm quit/restart pop-ups
Ctrl + Up              Move up project/timer
Ctrl + Shift + Up      Move to the start project/timer
Ctrl + Down            Move down project/timer
Ctrl + Shift + Down    Move to the end project/timer
Ctrl + A               Add new project/timer, add one set to the count
Ctrl + W               Open the quick timer pop-up
Ctrl + C               Copy project/timer
Ctrl + D               Delete project/timer, remove one set from the count
Ctrl + F               Toggle search
Ctrl + P               Toggle settings
Ctrl + Q               Open Darebee© workouts page
Ctrl + B               Open the expanded view of the project
Ctrl + Q               Quit app, close pop-ups
Ctrl + R               Open restart pop-up
// Zoom can be chaged in the page with the progress bars.
// After you need to quit the page and reopen it to fix the set and total timers that will disappear.
// TODO fix that
Ctrl + +               Zoom in
Ctrl + -               Zoom out
Ctrl + 0               Reset zoom

Up                     Restart current timer
Down                   End current timer
Left                   Send 10 seconds back the current timer
Right                  Send 10 seconds forward the current timer

Right click            Play/pause timer
Double left click      Play/pause timer
```
