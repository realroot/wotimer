import QtQuick
import QtQuick.Controls

MyHeaderFooter {
    id: exViewFooter

    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 6

        MyButton {
            id: addButton
            anchors.verticalCenter: parent.verticalCenter
            tip: qsTr("Add a new timer")
            text: "\u002B"
            pixelSide: 0
            onClicked: {
                add()
            }
        }

        MyButton {
            id: editButton
            anchors.verticalCenter: parent.verticalCenter
            text: "\uF013"
            tip: qsTr("Edit the selected timer")
            pixelSide: 8
            onClicked: {
                editWo()
            }
        }
/*
        MyButton {
            id: startButton
            text: "\uE0B0"
            onClicked: {
                start()
            }
        }
*/
        MyButton {
            id: backButton
            anchors.verticalCenter: parent.verticalCenter
            text: "\uF00D"
            tip: qsTr("Go back to the initial page")
            pixelSide: 8
            onClicked: {
                stack.currentItem.destroy()
                stack.pop()
            }
        }

        MyButton {
            text: "\uF019"
            anchors.verticalCenter: parent.verticalCenter
            tip: qsTr("DownLoad data from the Darebee© url")
            enabled: hasDarebeeUrl && !leaf.downloading
            onClicked: {
                download()
            }
        }

        BusyIndicator {
            id: busyInd
            running: leaf.downloading
            //running: true
            anchors.verticalCenter: parent.verticalCenter
        }
    }
}

