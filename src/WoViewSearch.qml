import QtQuick
import QtQuick.Controls

Page {
    id: woViewSearchLeaf
    width: parent.width
    height: parent.height
    property alias currentWo: row.currentWo
    property alias view: row.view
    property alias filter: row.filter
    property alias filterCol: row.filterCol
    //property alias progressRow: header.progressRow
    property bool isWoViewSearch: true
    property bool isTimerView: false
    property bool isQuickTimer: false
    background: Rectangle {
        color: "transparent"//palette.window
    }
    property bool d0: false
    property bool d1: false
    property bool d2: false
    property bool d3: false
    property bool d4: false
    property var difficulty: [false, false, false, false, false]
    property int fc: 0
    property int tp: 0
    property int tools: 0

    function diffChanged(i) {
        switch (i) {
        case 0:
            d0 = !d0
            break
        case 1:
            d1 = !d1
            break
        case 2:
            d2 = !d2
            break
        case 3:
            d3 = !d3
            break
        case 4:
            d4 = !d4
            break
        default:
            console.error( "ERROR: Invalid difficulty value passed")
            break
        }

        difficulty[i] = !difficulty[i]
        darebeeProxyModel.diffFilter = difficulty
        darebeeProxyModel.invalidate()
    }

    function removeDiff() {
        difficulty = [false, false, false, false, false]
        d0 = false
        d1 = false
        d2 = false
        d3 = false
        d4 = false
        darebeeProxyModel.diffFilter = difficulty
        darebeeProxyModel.invalidate()
    }

    function nameChanged(text) {
        darebeeProxyModel.nameFilter = text
        darebeeProxyModel.invalidate()
    }

    onFcChanged: {
        darebeeProxyModel.focusFilter = fc
        darebeeProxyModel.invalidate()
    }

    onTpChanged: {
        darebeeProxyModel.typeFilter = tp
        darebeeProxyModel.invalidate()
    }

    onToolsChanged: {
        darebeeProxyModel.toolsFilter = tools
        darebeeProxyModel.invalidate()
    }

    Rectangle {
        id: row
        property alias view: view
        property alias currentWo: view.currentItem
        property alias filter: filter
        property alias filterCol: filter.filterCol
        anchors.fill: parent
        anchors.margins: 15
        // view will have more space for some reason
        anchors.rightMargin: 10
        color: "transparent"

        Rectangle {
            id: filter
            anchors.top: parent.top
            anchors.left: parent.left
            width: 200
            height: 426
            color: palette.alternateBase
            //anchors.margins: 15
            property alias filterCol: filterCol

            Column {
                id: filterCol
                anchors.fill: parent
                anchors.margins: 5
                spacing: 6

                Text {
                    text: view.count + qsTr(" workouts")
                    leftPadding: 6
                    font.bold: true
                    color: palette.text
                }

                Rectangle {
                    color: palette.windowText
                    width: parent.width
                    height: 2
                }

                NameFilter {}

                Rectangle {
                    width: parent.width
                    height: 10
                    color: "transparent"

                    Rectangle {
                        anchors.centerIn: parent
                        width: parent.width
                        height: 2
                        color: palette.windowText
                    }
                }

                DifficultyFilter {}

                Rectangle {
                    width: parent.width
                    height: 10
                    color: "transparent"

                    Rectangle {
                        anchors.centerIn: parent
                        width: parent.width
                        height: 2
                        color: palette.windowText
                    }
                }

                FocusFilter {}

                Rectangle {
                    width: parent.width
                    height: 10
                    color: "transparent"

                    Rectangle {
                        anchors.centerIn: parent
                        width: parent.width
                        height: 2
                        color: palette.windowText
                    }
                }

                TypeFilter {}

                Rectangle {
                    width: parent.width
                    height: 10
                    color: "transparent"

                    Rectangle {
                        anchors.centerIn: parent
                        width: parent.width
                        height: 2
                        color: palette.windowText
                    }
                }

                ToolsFilter {}
            }
        }

        GridView {
            id: view
            //anchors.fill: parent
            //anchors.margins: 15
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: filter.right
            anchors.leftMargin: 15
            anchors.right: parent.right
            //spacing: 10
            cellWidth: 256
            cellHeight: 356
            model: darebeeProxyModel
            keyNavigationWraps: true
            //highlightMoveDuration: 150
            //highlightMoveVelocity: -1
            //focus: true

            delegate: //Component {
                      //id: delegateComp
                      WorkoutSearch {
                id: workoutSearch
                //}
            }
/*
            MouseArea {
                anchors.fill: parent
                propagateComposedEvents: true
                onClicked: {
                    view.forceActiveFocus()
                }
            }*/
        }
    }

    states: [
        State {
            name: ""

            PropertyChanges {
                target: filter
                height: 900
            }
            PropertyChanges {
                target: filterCol
                visible: true
            }
            PropertyChanges {
                target: view
                anchors.left: filter.right
                anchors.leftMargin: 15
            }
        },
        State {
            name: "closed"

            PropertyChanges {
                target: filter
                height: 0
            }
            PropertyChanges {
                target: filterCol
                visible: false
            }
            PropertyChanges {
                target: view
                anchors.left: parent.left
                anchors.leftMargin: 0
            }
        }
    ]

    transitions: Transition {
        NumberAnimation { properties: "height"; duration: 200 }
        NumberAnimation { properties: "visible"; duration: 200 }
        NumberAnimation { properties: "anchors.left"; duration: 350 }
        NumberAnimation { properties: "anchors.leftMargin"; duration: 350 }
    }

    function toggleFilter() {
        if (state === "closed") {
            state = ""
            view.forceActiveFocus()
        }
        else {
            state = "closed"
            view.forceActiveFocus()
        }
    }

    header: WoViewHeaderSearch { id: header }

    //footer: WoViewFooter { id: footer}

    property bool shortcutEnabled: stack.currentItem === woViewSearchLeaf ? true : false

    Component.onCompleted: {
        view.forceActiveFocus()
        darebeeProxyModel.ss = false
        darebeeProject.stopDownload = false
        var i = darebeeProject.getCount()
        if (darebeeWoLastIndex > i) {
            darebeeWoLastIndex = i
        }
        if (darebeeWoIndex > i) {
            darebeeWoIndex = i
        }
    }
    Component.onDestruction: {
        darebeeProxyModel.ss = true
        darebeeProject.stopDownload = true
        darebeeProxyModel.nameFilter = ""
        darebeeProxyModel.invalidate()
        leaf.downloading = false
    }
}
