import QtQuick
import QtQuick.Layouts

ColumnLayout {
    id: viewHeader
    spacing: 0

    property int sets
    property string name
    property string oldName: name
    property bool initPrep
    property bool defAutomatic

    function upd() {
        if (project.checkName(proIndex, name)) {
            project.setData(proIndex, name, defAutomatic, initPrep, sets)
            project.saveConf()
            oldName = name
        }
        else {
            proName.valueText = oldName
            ani.start()
        }
    }

    onSetsChanged: upd()
    onNameChanged: upd()
    onInitPrepChanged: upd()
    onDefAutomaticChanged: upd()
//add defPrepare

    property alias ani: warning.ani

    Rectangle {
        id: warning
        color: "red"
        border.color: "coral"
        border.width: 2
        Layout.fillWidth: true
        Layout.preferredHeight: 30
        visible: false
        property alias ani: ani

        Text {
            id: fieldText
            color: "yellow"
            text: "Name must be unique"
            height: parent.height - 2
            font.bold: true
            anchors.left: parent.left
            anchors.margins: 8
            width: parent.width - 10
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            elide: Text.ElideRight
        }

        SequentialAnimation {
            id: ani
            running: false
            ColorAnimation {
                target: warning
                property: "color"
                to: "red"
                duration: 400
            }
            ColorAnimation {
                target: warning
                property: "color"
                to: "transparent"
                duration: 400
            }
            ColorAnimation {
                target: warning
                property: "color"
                to: "red"
                duration: 400
            }
            ColorAnimation {
                target: warning
                property: "color"
                to: "transparent"
                duration: 400
            }
            onStarted: {
                warning.visible = true
                proName.visible = false
            }
            onStopped: {
                warning.visible = false
                proName.visible = true
            }
        }
    }

    Field {
        id: proName
        text: "Project name"
        valuePlaceholder: "New"
        valueText: name
        validator: null
        function updateValue(text) {
            name = text
        }
    }

    Field {
        text: "Sets"
        valueText: sets
        function updateValue(text) {
            sets = text
        }
    }

    Item {
        Layout.fillWidth: true
        Layout.preferredHeight: 13
    }

    SwitchField {
        text: "Initial countdown"
        checked: initPrep
        onCheckedChanged: {
            initPrep = checked
        }
    }

    SwitchField {
        text: "Def. Autostart"
        checked: defAutomatic
        onCheckedChanged: {
            defAutomatic = checked
        }
    }
/*
    SwitchField {
        text: "Def. Sound before end"
        checked: prepare
        onCheckedChanged: {
            prepare = checked
        }
    }
*/
    Item {
        Layout.fillWidth: true
        Layout.preferredHeight: 15
    }

    Component.onCompleted: {
        var data = project.getData(proIndex)
        sets = data.sets
        name = data.name
        initPrep = data.initPrep
        defAutomatic = data.defAutomatic
    }
}
