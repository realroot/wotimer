import QtQuick
import QtQuick.Controls

Page {
    id: timerViewLeaf
    width: parent.width
    height: parent.height
    property alias currentTimer: timerListView.currentItem
    property alias timerListView: pane.timerListView
    property alias model: timerListView.model
    //property alias delegate: timerListView.delegate
    property int currentSet: 1
    property bool multiset: sets > 1 ? true : false
    property bool initDone: false

    property bool defAutomatic
    property bool initPrep
    property int sets

    property int exTotal
    property int setTotal
    property int woTotal

    property string pdf

    //property bool collapsed: pdf === "" ? true : pane.pdfView.status !== 1 ? true : width > 900 ? false : true
    property bool collapsed: pdf === "" ? true : pdfView.status !== 1 ? true : width > 900 ? false : true
    //property bool collapsed: width > 900 ? false : true
    property bool isTimerView: true
    property bool isQuickTimer: false
    property alias prepTextBackground: prepTextBackground
    property alias quitPopup: quitPopup

    states: [
        State {
            name: "init"
        }
    ]

    Rectangle {
        id: background
        anchors.fill: parent
        color: "transparent"//palette.window
    }

    signal setCompleted()
    signal woCompleted()

    Connections {
        target: currentTimer
        property bool setEnded: false
        function onTimerCompleted() {
            bgCompletedAnimation.start()
            if (timerListView.currentIndex === timerListView.count - 1) {
                if (currentSet < sets) {
                    restartSet()
                    setEnded = true
                    setCompleted()
                }
                else {
                    woCompleted()
                    //header.text = "\"" + model.woName + "\"" + " completed"
                }
            }
            else {
                timerListView.incrementCurrentIndex()
                if (currentTimer.automatic) {
                    currentTimer.countdownTimer.start()
                }
                timerListView.footerItem.timerSet.checkSet()
                timerListView.footerItem.timerWo.checkTotalSetEnd()
            }
            if (customAction) {
                project.customAction()
            }
        }
    }
/*
    Pane {
        id: pane
        anchors.fill: parent
        property alias timerListView: scrollView.timerListView
        background: Rectangle {
            color: "transparent"
        }
        LayoutMirroring.enabled: Qt.application.layoutDirection === Qt.RightToLeft
        LayoutMirroring.childrenInherit: true

        ScrollView {
            id: scrollView
            property alias timerListView: timerListView
            anchors.fill: parent
            contentItem: timerListView
            TimerListView {
                id: timerListView
                property alias timerSet: footer.children
                footer: TimerListViewFooter { id: timerListViewfooter }
            }
        }
    }
*/
    Pane {
        id: pane
        //anchors.fill: parent
        anchors.top: parent.top
        //anchors.right: parent.right
        anchors.left: parent.left
        height: parent.height
        width: collapsed ? parent.width : timerViewLeaf.width > 1600 ? parent.width - 800 :
            parent.width / 2
        property alias timerListView: scrollView.timerListView
        //property alias pdfView: pdfView
        //property alias pdfStatus: pdfView.pdfStatus
        //property alias pdfStatus: pdfView.status
        background: Rectangle {
            color: "transparent"
        }
        LayoutMirroring.enabled: Qt.application.layoutDirection === Qt.RightToLeft
        LayoutMirroring.childrenInherit: true

        ScrollView {
            id: scrollView
            property alias timerListView: timerListView
            anchors.fill: parent
            contentItem: timerListView

            TimerListView {
                id: timerListView
                property alias timerSet: footer.children
                footer: TimerListViewFooter { id: timerListViewfooter }
            }
        }
    }

    Image {
        id: pdfView
        width: collapsed ? 0 : timerViewLeaf.width > 1600 ? 800 : pane.width - 20
        height: parent.height > 800 ? 800 : parent.height
        fillMode: Image.PreserveAspectFit
        source: pdf
        anchors.top: parent.top
        anchors.topMargin: 10
        //anchors.left: scrollView.right
        anchors.left: pane.right
        Component.onCompleted: {
            //console.log("pdf is: " + pdf)
            //console.log("source is: " + source)
            //console.log("status is: " + status)
            if (pdf === "" || status !== 1) {
                destroy()
            }
        }
    }

    property int initPrepare: defInitPrepVal

    Rectangle {
        id: prepTextBackground
        anchors.centerIn: parent
        width: 100
        height: 100
        color: palette.window
        border.color: palette.accent
        radius: 10
        visible: false
        Text {
            text: initPrepare.toFixed(0)
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            padding: 6
            font.pixelSize: 60
            color: palette.windowText
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                thaw()
            }
        }
    }

    Timer {
        id: prepTimer
        interval: 1000
        repeat: true
        running: false
        onTriggered: {
            if (initPrepare > 1) {
                initPrepare -= 1
            }
            else {
                player.play()
                thaw()
            }
        }
    }
    Popup {
        id: restartPopup
        modal: true
        anchors.centerIn: parent
        width: 100
        height: 100
        background: Rectangle {
            border.color: palette.accent
            color: palette.window
            radius: 10
        }
        enter: Transition {
            NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
        }
        exit: Transition {
            NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
        }
        Text {
            text: "Restart?"
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            font.pixelSize: 24
            color: palette.windowText
        }
        SmallButton {
            id: restartYesButton
            tipEnabled: false
            anchors.margins: 6
            anchors.leftMargin: 7
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            width: 32
            height: 32
            text: "\uF00C"
            onClicked: {
                resetWo()
                restartPopup.close()
            }
        }
        SmallButton {
            id: restartNoButton
            tipEnabled: false
            anchors.margins: 6
            anchors.right: parent.right
            anchors.rightMargin: 7
            anchors.bottom: parent.bottom
            width: 32
            height: 32
            text: "\uF00D"
            onClicked: {
                restartPopup.close()
            }
        }
        Shortcut {
            sequences: ["esc", "ctrl+q"]
            enabled: restartPopup.visible ? true : false
            onActivated: {
                resetWo()
                restartPopup.close()
            }
        }
    }

    Popup {
        id: quitPopup
        modal: true
        anchors.centerIn: parent
        width: 100
        height: 100
        background: Rectangle {
            border.color: palette.accent
            color: palette.window
            radius: 10
        }
        enter: Transition {
            NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
        }
        exit: Transition {
            NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
        }
        Text {
            text: "Quit?"
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            font.pixelSize: 30
            color: palette.windowText
        }
        SmallButton {
            id: yesButton
            tipEnabled: false
            anchors.margins: 6
            anchors.leftMargin: 7
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            width: 32
            height: 32
            text: "\uF00C"
            onClicked: {
                handleQuit()
            }
        }
        SmallButton {
            id: noButton
            tipEnabled: false
            anchors.margins: 6
            anchors.right: parent.right
            anchors.rightMargin: 7
            anchors.bottom: parent.bottom
            width: 32
            height: 32
            text: "\uF00D"
            onClicked: {
                quitPopup.close()
            }
        }

        Shortcut {
            sequences: ["esc", "ctrl+q"]
            enabled: quitPopup.visible ? true : false
            onActivated: {
                handleQuit()
            }
        }
    }

    header: TimerViewHeader { id: header }

    footer: TimerViewFooter { id: footer }

    function restartSet() {
        bgCompletedAnimation.restart()

        for (var i = 0; i <= timerListView.currentIndex; i++) {
            timerListView.itemAtIndex(i).resetTimer()
        }

        timerListView.currentIndex = 0
            if (currentTimer.automatic) {
                currentTimer.countdownTimer.start()
            }
        timerListView.footerItem.timerWo.checkTotalTimerEnd()
        currentSet++
    }

    function resetWo() {
        initPrepare = defInitPrepVal
        initDone = false
        currentSet = 1
        currentTimer.countdownTimer.stop()
        for (var i = 0; i <= timerListView.currentIndex; i++) {
            timerListView.itemAtIndex(i).resetTimer()
        }
        timerListView.footerItem.timerSet.resetTimer()
        timerListView.footerItem.timerWo.resetTimer()
        timerListView.currentIndex = 0
    }

    SequentialAnimation {
        id: bgCompletedAnimation
        running: false
        ColorAnimation {
            target: background
            property: "color"
            to: "red"
            duration: 400
        }
        ColorAnimation {
            target: background
            property: "color"
            to: "black"
            duration: 400
        }
        ColorAnimation {
            target: background
            property: "color"
            to: "red"
            duration: 400
        }
        ColorAnimation {
            target: background
            property: "color"
            to: "black"
            duration: 300
        }
    }
/*
    Shortcut {
        sequences: ["esc", "ctrl+q"]
        onActivated: {
            if (prepTextBackground.visible) {
                thaw()
            }
            else {
                handleQuit()
            }
        }
    }

    Shortcut {
        sequences: ["return", "space"]
        onActivated: {
            if (prepTextBackground.visible) {
                thaw()
                return
            }
            if (timerViewLeaf.state === "init") {
                return
            }
           play()
        }
    }*/
    Shortcut {
        sequence: "left"
        onActivated: {
            if (timerViewLeaf.state === "init")
                return
            backward()
        }
    }
    Shortcut {
        sequence: "right"
        onActivated: {
            if (timerViewLeaf.state === "init")
                return
            forward()
        }
    }
    Shortcut {
        sequence: "up"
        onActivated: {
            if (timerViewLeaf.state === "init")
                return
            restartTimer()
        }
    }
    Shortcut {
        sequence: "down"
        onActivated: {
            if (timerViewLeaf.state === "init")
                return
            endTimer()
        }
    }
    Shortcut {
        sequence: "ctrl+r"
        onActivated: {
            restartPopup.open()
        }
    }
    Shortcut {
        sequence: "ctrl+a"
        onActivated: {
            addSet()
        }
    }
    Shortcut {
        sequence: "ctrl+d"
        onActivated: {
            removeSet()
        }
    }
    Shortcut {
        sequence: "ctrl++"
        onActivated: {
            //if (zoom > 6.5)
            if (zoom > 2)
                return
            zoom += 0.1
        }
    }
    Shortcut {
        sequence: "ctrl+-"
        onActivated: {
            if (zoom < 0.8)
                return
            zoom -= 0.1
        }
    }
    Shortcut {
        sequence: "ctrl+0"
        onActivated: {
            zoom = 1
        }
    }

    function addSet() {
        if (currentSet < sets) {
            currentSet++
            timerListView.footerItem.timerWo.recalculate(-setTotal)
        }
    }

    function removeSet() {
        if (currentSet > 1) {
            currentSet--
            timerListView.footerItem.timerWo.recalculate(setTotal)
        }
    }

    function restartTimer() {
        if (currentTimer.value === 1) {
            return;
        }
        var diff = currentTimer.countdownValue - currentTimer.countdown

        currentTimer.value = 0
        currentTimer.countdown = currentTimer.countdownValue
        currentTimer.updateSets(diff)
    }

    function backward() {
        if (currentTimer.value === 1) {
            return;
        }
        var diff = currentTimer.countdownValue - currentTimer.countdown

        if (currentTimer.countdown > (currentTimer.countdownValue - 10.0)) {
            currentTimer.value = 0
            currentTimer.countdown = currentTimer.countdownValue
        }
        else {
            currentTimer.countdown += 10
            currentTimer.value -= currentTimer.to / currentTimer.countdownValue * 10
            diff = 10
        }
        currentTimer.updateSets(diff)
    }

    function play() {
        if (currentTimer.value === 1) {
            return;
        }

        if (currentTimer.countdownTimer.running) {
            currentTimer.countdownTimer.stop()
        }
        else {
            if (!initDone && initPrep) {
                prepTimer.start()
                prepTextBackground.visible = true
                freeze()
                initDone = true
            }
            else {
                currentTimer.countdownTimer.start()
            }
        }
    }

    function forward() {
        if (currentTimer.value === 1) {
            return;
        }
        var diff = currentTimer.countdown * -1

        if (currentTimer.countdown <= 10.0) {
            currentTimer.endTimer()
        }
        else {
            currentTimer.countdown -= 10
            currentTimer.value += currentTimer.to / currentTimer.countdownValue * 10
            diff = -10
        }
        currentTimer.updateSets(diff)
    }

    function endTimer() {
        if (currentTimer.value === 1) {
            return;
        }
        var diff = currentTimer.countdown * -1
        currentTimer.endTimer()
        currentTimer.updateSets(diff)
    }

    function thaw() {
        currentTimer.countdownTimer.start()
        bgCompletedAnimation.start()
        prepTextBackground.visible = false
        prepTimer.stop()
        timerViewLeaf.state = ""
        footer.enabled = true
    }

    function freeze() {
        timerViewLeaf.state = "init"
        footer.enabled = false
    }

    function handleQuit() {
        if (quitPopup.visible) {
            timerViewLeaf.destroy()
            stack.pop()
            stack.currentItem.header.closeSearch()
            //woViewEsc.enabled = true
            //shortcutEnabled = true
        }
        else {
            quitPopup.open()
        }
    }

    MouseArea {
        anchors.fill: parent
        propagateComposedEvents: true
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: (mouse) => {
            if (mouse.button === Qt.RightButton) {
                if (prepTextBackground.visible) {
                    thaw()
                    return
                }
                if (state === "init") {
                    return
                }
                play()
            }
            else if (mouse.button === Qt.LeftButton) {
                mouse.accepted = false
            }
        }
        onDoubleClicked: (mouse) => {
            if (mouse.button === Qt.LeftButton) {
                if (prepTextBackground.visible) {
                    thaw()
                    return
                }
                if (state === "init") {
                    return
                }
                play()
            }
        }
    }
}
