import QtQuick

Item {
    id: woLeaf
    width: 248
    height: 348

    Rectangle {
        id: wrapper
        color: currentWo == woLeaf ? useSystemTheme ? palette.highlight : palette.highlightedText :
        useSystemTheme ? palette.midlight : palette.mid
        border.color: currentWo == woLeaf ? (useSystemTheme ? palette.dark : "salmon") : palette.light
        border.width: 2
        width: parent.width
        height: parent.height

        Column {
            anchors.fill: parent
            anchors.bottomMargin: 41
            anchors.topMargin: 5
            spacing: 10

            Text {
                id: woText
                font.pixelSize: 10
                //anchors.top: parent.top
                //anchors.topMargin: 3
                //verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                color: useSystemTheme ? palette.text : palette.window
                elide: Text.ElideRight
                width: parent.width - 10
                text: model.name
            }

            Image {
                id: pdfImage
                width: parent.width
                height: parent.height - 10
                fillMode: Image.PreserveAspectFit
                source: model.pdf === "" ? "qrc:/pdfs/empty.pdf" : model.pdf

                Component.onCompleted: {
                    if ( status !== 1) {
                        source = "qrc:/pdfs/error.pdf"
                    }
                }
            }
        }
/*
        Row {
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 5

            SmallButton {
                id: expButton
                text: "\uF50C"
                tip: qsTr("WIP Open the expanded view of the selected project")
                onClicked: {
                    expand()
                }
            }
        }*/
    }

    function expand() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        // Will seg fault here
        //view.currentIndex = darebeeProxyModel.findIndex(index)
        var woExp = Qt.createComponent("WoExp.qml")
        if (woExp.status === Component.Ready) {
            mainConfirm.enabled = false
            stack.push(woExp.createObject(stack, {"proIndex": view.currentIndex, "name": model.name, "pdf": model.pdf,
                "url": model.url, "map": model.map, "descr": model.description, "ec": model.ec, "difficulty": model.difficulty,
                "fc": model.focus, "tp": model.type, "tools": model.tools, "isWoViewSearchExp": true}))
        }
        else {
            console.error("ERROR: Cannot create WoExp component: " + woExp.errorString())
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            view.forceActiveFocus()
            if (view.interactive) {
                view.currentIndex = index
            }
        }
        onDoubleClicked: {
            //start()
        }
        onPressAndHold: {
            view.forceActiveFocus()
            //start()
        }
    }
}
