#include <QApplication>
#include <QQmlApplicationEngine>
//#include <QCommandLineParser>
//#include <QQuickStyle>

#include "project.h"

int main(int argc, char *argv[]) {
    QCoreApplication::setApplicationVersion("0.1.0");
    QCoreApplication::setApplicationName("wotimer");
    QCoreApplication::setOrganizationName("realroot");
    QCoreApplication::setOrganizationDomain("com.realroot");

    QApplication app(argc, argv);
    //app.setProperty("KDE_COLOR_SCHEME_PATH", "no");
    //QQuickStyle::setStyle("Fusion");

    QQmlApplicationEngine engine;

    qmlRegisterType<Project>("com.realroot", 1, 0,"Project");
    qmlRegisterType<TimerClass>("com.realroot", 1, 0,"TimerClass");
    qmlRegisterType<ProxyModel>("com.realroot", 1, 0,"ProxyModel");
/*
    QCommandLineParser parser;
    parser.setApplicationDescription("Qml (from Qt) timer with WorkOuts in mind.");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption({{"t", "toggle"}, "Play/pause the current timer"});
    parser.process(app);

    bool toggleFlag = parser.isSet("t", "toggle");

    engine.rootContext()->setContextProperty("toggleFlag", toggleFlag);
*/
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
