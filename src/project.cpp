#include "project.h"

QHash<int, QByteArray> TimerClass::roleNames() const {
    QHash<int, QByteArray> map = {
        {NameRole, "name"},
        {AutomaticRole, "automatic"},
        {PrepareRole, "prepare"},
        {ValueRole, "countdownValue"},
    };
    return map;
}

int TimerClass::rowCount(const QModelIndex& parent) const {
    if (parent.isValid()) {
        return 0;
    }
    return m_timerData.count();
}


QVariant TimerClass::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }
    const TimerData& timerData = m_timerData.at(index.row());
    switch(role) {
    case NameRole:
        return timerData.name;
        break;
    case AutomaticRole:
        return timerData.automatic;
        break;
    case PrepareRole:
        return timerData.prepare;
        break;
    case ValueRole:
        return timerData.countdownValue;
        break;
    default:
        return QVariant();
        break;
    }
}

bool TimerClass::setData(const QModelIndex& index, const QVariant& value, int role) {
    if (!index.isValid() || role < Qt::UserRole)
        return false;

    int row = index.row();

    if (row < 0 || row >= m_timerData.size())
        return false;

    TimerData& timer = m_timerData[row];

    switch(role) {
    case NameRole:
        timer.name = value.toString();
        break;
    case AutomaticRole:
        timer.automatic = value.toBool();
        break;
    case ValueRole:
        timer.countdownValue = value.toInt();
        break;
    case PrepareRole:
        timer.prepare = value.toBool();
        break;
    default:
        return false;
        break;
    }

    emit dataChanged(index, index, {role});

    return true;
}

QVariant TimerClass::toQVariant() const {
    return QVariant::fromValue(this);
}

TimerClass::TimerClass(const QVariant& value) {
    if (value.canConvert<TimerClass>()) {
        m_timerData = value.value<TimerClass>().m_timerData;
    }
}

TimerClass& TimerClass::operator=(const TimerClass& other) {
    m_timerData.clear();
    for (auto& a : other.m_timerData) {
        m_timerData.append(a);
    }
    return *this;
}

TimerClass::TimerClass(const TimerClass& other) : TimerClass() {
    m_timerData = other.m_timerData;
}

void TimerClass::add(const TimerData &timerData) {
    const int rowOfInsert = m_timerData.count();
    beginInsertRows(QModelIndex(), rowOfInsert, rowOfInsert);
    m_timerData.insert(rowOfInsert, timerData);
    endInsertRows();
}

bool TimerClass::move(int sourceFirst, int dest) {

    bool ret = beginMoveRows(QModelIndex(), sourceFirst, sourceFirst, QModelIndex(), dest);

    if (!ret) {
        return ret;
    }

    if (sourceFirst < dest) {
        m_timerData.move(sourceFirst, dest - 1);
    }
    else {
        m_timerData.move(sourceFirst, dest);
    }

    endMoveRows();
    return ret;
}

void TimerClass::del(int first, int last) {
    beginRemoveRows(QModelIndex(), first, last);

    if (first == last) {
        m_timerData.remove(first);
    }
    else {
        m_timerData.remove(first, last);
    }

    endRemoveRows();
}

///////////////////////////////////////////////////////////////Project

bool Project::getStopDownload() const {
    return m_stopDownload;
}

int Project::getWoIndex() const {
    return m_woIndex;
}

int Project::getWoLastIndex() const {
    return m_woLastIndex;
}


void Project::setStopDownload(const bool b) {
    if (b != m_stopDownload) {
        m_stopDownload = b;
        emit stopDownloadChanged();
    }
}

void Project::setWoIndex(const int i) {
    if (i != m_woIndex) {
        m_woIndex = i;
        emit woIndexChanged();
    }
}

void Project::setWoLastIndex(const int i) {
    if (i != m_woLastIndex) {
        m_woLastIndex = i;
        emit woLastIndexChanged();
    }
}

QHash<int, QByteArray> Project::roleNames() const {
    QHash<int, QByteArray> map = {
        {NameRole, "name"},
        {AutomaticRole, "defAutomatic"},
        {PrepareRole, "defPrepare"},
        {InitRole, "initPrep"},
        {SetsRole, "sets"},
        {TimersRole, "timers"},
        {ExTotalRole, "exTotal"},
        {SetTotalRole, "setTotal"},
        {WoTotalRole, "woTotal"},
        {UrlRole, "url"},
        {DescriptionRole, "description"},
        {EcRole, "ec"},
        {DifficultyRole, "difficulty"},
        {FocusRole, "focus"},
        {TypeRole, "type"},
        {ToolsRole, "tools"},
        {MapRole, "map"},
        {PdfRole, "pdf"},
    };
    return map;
}

void Project::saveConf() {
    QJsonObject jsonObj;
    QJsonArray projectsArray;

    for (const Data& d : std::as_const(m_data)) {
        QJsonObject newProject;
        newProject["name"] = d.name;
        newProject["defAutomatic"] = d.defAutomatic;
        newProject["defPrepare"] = d.defPrepare;
        newProject["initPrep"] = d.initPrep;
        newProject["sets"] = d.sets;
        newProject["difficulty"] = d.difficulty;
        newProject["url"] = d.url;
        newProject["description"] = d.description;
        newProject["ec"] = d.ec;
        newProject["focus"] = static_cast<int>(d.focus);
        newProject["type"] = static_cast<int>(d.type);
        newProject["tools"] = static_cast<int>(d.tools);
        newProject["map"] = d.map;
        newProject["pdf"] = d.pdf;

        QJsonArray timersArray;
        for (const TimerData& td : d.timers.m_timerData) {
            QJsonObject newTimer;
            newTimer["name"] = td.name;
            newTimer["automatic"] = td.automatic;
            newTimer["countdownValue"] = td.countdownValue;
            newTimer["prepare"] = td.prepare;
            timersArray.append(newTimer);
        }
        newProject["timers"] = timersArray;
        projectsArray.append(newProject);
    }

    jsonObj["projects"] = projectsArray;
    QJsonDocument updatedJsonDoc(jsonObj);
    QByteArray updatedJsonData = updatedJsonDoc.toJson();

    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/projects.json");
    if (file.open(QIODevice::WriteOnly)) {
        file.write(updatedJsonData);
        file.close();
        qDebug() << "INFO: Saving projects configuration";
    }
}

void Project::saveDarebeeWoConf() {
    QJsonObject jsonObj;
    QJsonArray projectsArray;

    for (const Data& d : std::as_const(m_data)) {
        QJsonObject newProject;
        newProject["name"] = d.name;
        //newProject["defAutomatic"] = d.defAutomatic;
        //newProject["defPrepare"] = d.defPrepare;
        //newProject["initPrep"] = d.initPrep;
        //newProject["sets"] = d.sets;
        newProject["difficulty"] = d.difficulty;
        newProject["url"] = d.url;
        newProject["description"] = d.description;
        newProject["ec"] = d.ec;
        newProject["focus"] = static_cast<int>(d.focus);
        newProject["type"] = static_cast<int>(d.type);
        newProject["tools"] = static_cast<int>(d.tools);
        newProject["map"] = d.map;
        newProject["pdf"] = d.pdf;
        newProject["id"] = d.id;

        QJsonArray timersArray;/*
        for (const TimerData& td : d.timers.m_timerData) {
            QJsonObject newTimer;
            newTimer["name"] = td.name;
            newTimer["automatic"] = td.automatic;
            newTimer["countdownValue"] = td.countdownValue;
            newTimer["prepare"] = td.prepare;
            timersArray.append(newTimer);
        }*/
        newProject["timers"] = timersArray;
        projectsArray.append(newProject);
    }

    jsonObj["projects"] = projectsArray;
    QJsonDocument updatedJsonDoc(jsonObj);
    QByteArray updatedJsonData = updatedJsonDoc.toJson();

    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/darebee-wo.json");
    if (file.open(QIODevice::WriteOnly)) {
        file.write(updatedJsonData);
        file.close();
        qDebug() << "INFO: Saving Darebbe© workouts configuration";
    }
}

void Project::clearData() {
    const int to = m_data.count() - 1;
    if (to > -1) {
        beginRemoveRows(QModelIndex(), 0, to);
        m_data.clear();
        endRemoveRows();
        // Will messed up importConf for some reason once in a while
        //saveConf();
    }
}

void Project::readConf() {
    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/projects.json");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());
    file.close();

    if (!jsonDoc.isNull()) {
        QJsonObject jsonObj = jsonDoc.object();

        if (jsonObj.contains("projects") && jsonObj["projects"].isArray()) {
            QJsonArray itemsArray = jsonObj["projects"].toArray();

            clearData();

            for (const QJsonValue& itemValue : std::as_const(itemsArray)) {
                QJsonObject itemObj = itemValue.toObject();

                Data d;
                d.defAutomatic = itemObj["defAutomatic"].toBool();
                d.defPrepare = itemObj["defPrepare"].toBool();
                d.initPrep = itemObj["initPrep"].toBool();
                d.name = itemObj["name"].toString();
                d.sets = itemObj["sets"].toInt();
                d.difficulty = itemObj["difficulty"].toInt();
                d.url = itemObj["url"].toString();
                d.description = itemObj["description"].toString();
                d.ec = itemObj["ec"].toString();
                d.focus = static_cast<Data::Focus>(itemObj["focus"].toInt());
                d.type = static_cast<Data::Type>(itemObj["type"].toInt());
                d.tools = static_cast<Data::Tools>(itemObj["tools"].toInt());
                d.map = itemObj["map"].toString();
                d.pdf = itemObj["pdf"].toString();

                if (itemObj.contains("timers") && itemObj["timers"].isArray()) {
                    QJsonArray timersArray = itemObj["timers"].toArray();

                    for (const QJsonValue& timerValue : std::as_const(timersArray)) {
                        QJsonObject timerObj = timerValue.toObject();
                        TimerData td;
                        td.automatic = timerObj["automatic"].toBool();
                        td.countdownValue = timerObj["countdownValue"].toInt();
                        td.name = timerObj["name"].toString();
                        td.prepare = timerObj["prepare"].toBool();
                        d.timers.add(td);
                    }
                }
                d.update();
                add(d);
            }
        }
    }
}

void Project::readDarebeeWoConf() {
    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/darebee-wo.json");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());
    file.close();

    if (!jsonDoc.isNull()) {
        QJsonObject jsonObj = jsonDoc.object();

        if (jsonObj.contains("projects") && jsonObj["projects"].isArray()) {
            QJsonArray itemsArray = jsonObj["projects"].toArray();

            clearData();

            for (const QJsonValue& itemValue : std::as_const(itemsArray)) {
                QJsonObject itemObj = itemValue.toObject();

                Data d;
                //d.defAutomatic = itemObj["defAutomatic"].toBool();
                //d.defPrepare = itemObj["defPrepare"].toBool();
                //d.initPrep = itemObj["initPrep"].toBool();
                d.name = itemObj["name"].toString();
                //d.sets = itemObj["sets"].toInt();
                d.difficulty = itemObj["difficulty"].toInt();
                d.url = itemObj["url"].toString();
                d.description = itemObj["description"].toString();
                d.ec = itemObj["ec"].toString();
                d.focus = static_cast<Data::Focus>(itemObj["focus"].toInt());
                d.type = static_cast<Data::Type>(itemObj["type"].toInt());
                d.tools = static_cast<Data::Tools>(itemObj["tools"].toInt());
                d.map = itemObj["map"].toString();
                d.pdf = itemObj["pdf"].toString();
                d.id = itemObj["id"].toInt();
/*
                if (itemObj.contains("timers") && itemObj["timers"].isArray()) {
                    QJsonArray timersArray = itemObj["timers"].toArray();

                    for (const QJsonValue& timerValue : std::as_const(timersArray)) {
                        QJsonObject timerObj = timerValue.toObject();
                        TimerData td;
                        td.automatic = timerObj["automatic"].toBool();
                        td.countdownValue = timerObj["countdownValue"].toInt();
                        td.name = timerObj["name"].toString();
                        td.prepare = timerObj["prepare"].toBool();
                        d.timers.add(td);
                    }
                }*/
                //d.update();
                add(d);
            }
        }
    }
}

void Project::exportConf(const QString& filePath) {
    QFile file(filePath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return;
    }
    QTextStream stream(&file);

    QFile cfg(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/projects.json");
    if (!cfg.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return;
    }

    QTextStream s(&cfg);
    while (!s.atEnd())
    {
        QString line = s.readLine();
        stream << line << "\n";
    }

    cfg.close();
    file.close();
}

void Project::loadConf() {
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    const QString& filePath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/projects.json";

    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "ERROR:" << QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) << "folder could not be created";
        }
    }

    QFileInfo fileInfo(filePath);
    if (fileInfo.exists()) {
        return;
    }
    else {
        importConf();
    }
    if (!fileInfo.exists()) {
        qDebug() << "ERROR: Config not found and it could not be imported";
    }
}

void Project::downloadConf() {
    //todo add some feedback of the download
    qDebug() << "INFO: Starting the download of the config";
    m_manager->get(QNetworkRequest(QUrl(
        "https://codeberg.org/realroot/wotimer-darebee-config/raw/branch/main/projects.json")));
}

void Project::downloadDarebeeWo() {
    qDebug() << "INFO: Starting the download of the current Darebee© workout";
    QNetworkReply* reply = m_manager->get(QNetworkRequest(QUrl("https://darebee.com/media/com_jamegafilter/en_gb/1.json")));
    reply->setProperty("darebeeWo", true);
}

void Project::findDarebeeWoTools(int index) {
    qDebug() << "INFO: Starting the download of the current Darebee© workout";
    QNetworkReply* reply = m_manager->get(QNetworkRequest(QUrl("https://darebee.com/media/com_jamegafilter/en_gb/1.json")));
    reply->setProperty("findDarebeeTools", index);
}

void Project::downloadData(int index, bool isDarebee, bool check) {
    //todo add some feedback of the download
    qDebug() << "INFO: Starting the download of a Darebee© workout";
    QNetworkReply* reply = m_manager->get(QNetworkRequest(QUrl(m_data[index].url)));
    reply->setProperty("requestIndex", index);
    reply->setProperty("isDarebee", isDarebee);
    reply->setProperty("check", check);
}

void Project::downloadFinished(QNetworkReply *reply) {
    if (m_stopDownload) {
        qDebug() << "INFO: Download was stopped";
        return;
    }
    if (reply->property("requestIndex").isValid()) {
        if (reply->error() == QNetworkReply::NoError) {
            QString htmlContent = reply->readAll();
            int index = reply->property("requestIndex").toInt();
            processData(htmlContent, index, reply->property("isDarebee").toBool(), reply->property("check").toBool());
        }
        else {
            qDebug() << "ERROR: Download could not be completed";
            emit darebeeWoDownloadError();
        }
    }
    else if (reply->property("map").isValid()) {
        if (reply->error() == QNetworkReply::NoError) {
            QUrl url = reply->url();
            QString fileName = QFileInfo(url.path()).fileName();
            QFile file(reply->property("map").toString() + "/darebee/images/workouts/" + fileName);
            if (file.open(QIODevice::WriteOnly)) {
                file.write(reply->readAll());
                file.close();
                m_data[reply->property("index").toInt()].map = "file://" + file.fileName();
                //qDebug() << "INFO:" << m_data[reply->property("index").toInt()].map;

                if (reply->property("isDarebee").toBool()) {
                    m_activeDownloads--;
                    qDebug() << "INFO: One download completed, left: " << m_activeDownloads;
                    if (m_activeDownloads == 0) {
                        if (reply->property("check").toBool()) {
                            if (m_woIndex == m_data.size()) {
                                saveDarebeeWoConf();
                                emit darebeeWoDownloadDone();
                            }
                            else {
                                downloadDarebeeWoDataCheck();
                            }
                        }
                        else {
                            if (m_woIndex == m_data.size() - 1) {
                                m_woIndex = m_woLastIndex;
                                emit woIndexChanged();
                                saveDarebeeWoConf();
                                emit darebeeWoDownloadDone();
                            }
                            else {
                                downloadDarebeeWoData();
                            }
                        }
                    }
                }
            }
        }
        else {
            qDebug() << "ERROR: Download could not be completed";
        }
    }
    else if (reply->property("pdf").isValid()) {
        if (reply->error() == QNetworkReply::NoError) {
            QUrl url = reply->url();
            QString fileName = QFileInfo(url.path()).fileName();
            QFile file(reply->property("pdf").toString() + "/darebee/pdf/workouts/" + fileName);
            if (file.open(QIODevice::WriteOnly)) {
                file.write(reply->readAll());
                file.close();
                m_data[reply->property("index").toInt()].pdf = "file://" + file.fileName();

                if (reply->property("isDarebee").toBool()) {
                    m_activeDownloads--;
                    qDebug() << "INFO: One download completed, left: " << m_activeDownloads;
                    if (m_activeDownloads == 0) {
                        if (reply->property("check").toBool()) {
                            if (m_woIndex == m_data.size()) {
                                saveDarebeeWoConf();
                                emit darebeeWoDownloadDone();
                            }
                            else {
                                downloadDarebeeWoDataCheck();
                            }
                        }
                        else {
                            if (m_woIndex == m_data.size() - 1) {
                                m_woIndex = m_woLastIndex;
                                emit woIndexChanged();
                                saveDarebeeWoConf();
                                emit darebeeWoDownloadDone();
                            }
                            else {
                                downloadDarebeeWoData();
                            }
                        }
                    }
                }
            }
        }
    }
    else if (reply->property("darebeeWo").isValid()) {
        if (reply->error() == QNetworkReply::NoError) {
            QByteArray woJson = reply->readAll();
            parseDarebeeWoJson(woJson);
        }
        else {
            qDebug() << "ERROR: Download of the Darebee© workouts could not be completed";
            emit darebeeWoDownloadError();
        }
    }
    else if (reply->property("findDarebeeTools").isValid()) {
        if (reply->error() == QNetworkReply::NoError) {
            QByteArray woJson = reply->readAll();
            parseDarebeeWoJson(woJson, reply->property("findDarebeeTools").toInt());
        }
        else {
            qDebug() << "ERROR: Download of the Darebee© workout tools could not be completed";
            emit darebeeWoDownloadError();
        }
    }
    else {
        bool success = false;
        if (reply->error() == QNetworkReply::NoError) {
            success = true;
            emit downloadSuccess(success);
            qDebug() << "INFO: Download successful";
            QByteArray data = reply->readAll();
            QString filePath = QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/downloadedProjects.json";
            QFile file(filePath);
            if (file.open(QIODevice::WriteOnly)) {
                QTextStream out(&file);
                out << data;
                file.close();
                importConf(false, filePath);
            }
        }
        else {
            qDebug() << "ERROR: Download could not be completed";
            emit downloadSuccess(success);
        }
    }
    reply->deleteLater();
}

void Project::parseDarebeeWoJson(const QString& woJson, bool upd) {
    QJsonDocument jsonDoc = QJsonDocument::fromJson(woJson.toUtf8());
    QJsonObject jsonObj = jsonDoc.object();

    int items = 0;
    for (const QString& key : jsonObj.keys()) {
        if (key.startsWith("item_")) {
            items++;
        }
    }

    m_activeDownloads = 0;
    m_woIndex = 0;
    emit woIndexChanged();
    if (items > m_data.count()) {
        m_woLastIndex = items;
    }
    else {
        m_woLastIndex = m_data.count();
    }
    emit woLastIndexChanged();
    bool newData = false;
    int i = -1;

    for (const QString& key : jsonObj.keys()) {
        if (!key.startsWith("item_")) {
            continue;
        }
        QJsonObject item = jsonObj[key].toObject();
        int id = item["id"].toInt();
        QString name = item["name"].toString();
        if (name.size() > 8 && name.endsWith(" Workout")) {
            name.chop(8);
        }
        QString url = "https://darebee.com" + item["url"].toString();

        QJsonObject attributes = item["attr"].toObject();
        //QString focus = "";
        //QString diff = "";
        //QString type = "";
        QString tools = "";/*
        if (attributes.contains("ct10") && attributes["ct10"].isObject()) {
            focus = attributes["ct10"].toObject()["value"].toArray().toVariantList().first().toString();
        }
        if (attributes.contains("ct14") && attributes["ct14"].isObject()) {
            diff = attributes["ct14"].toObject()["value"].toArray().toVariantList().first().toString();
        }
        if (attributes.contains("ct16") && attributes["ct16"].isObject()) {
            type = attributes["ct16"].toObject()["value"].toArray().toVariantList().first().toString();
        }*/
        if (attributes.contains("ct19") && attributes["ct19"].isObject()) {
            tools = attributes["ct19"].toObject()["value"].toArray().toVariantList().first().toString();
        }

        int index = checkDarebeeWoId(id);
        //qDebug() << "Name" << name;
        //qDebug() << "Index " << index;
        if (index == -1) {
            addDarebeeWo(name, url, id);
            qDebug() << "INFO: Found new workout:" << name;
            index = m_data.count() - 1;
            if (!newData) {
                i = index;
                newData = true;
            }
        }
        else {
            qDebug() << "INFO: Workout was already parsed";
            if (upd) {
                continue;
            }
            //setData(index, name, false, false, false, 1, url);
        }

        if (tools == "") {
            m_data[index].tools = Data::Tools::unknown;
        }
        else if (tools == "none") {
            m_data[index].tools = Data::Tools::none;
        }
        else if (tools == "dumbbells") {
            m_data[index].tools = Data::Tools::dumbbels;
        }
        else if (tools == "bar") {
            m_data[index].tools = Data::Tools::pullUpBar;
        }
        else if (tools == "other") {
            m_data[index].tools = Data::Tools::other;
        }
    }

    saveDarebeeWoConf();
    qDebug() << "INFO: Starting the download of the Darebee© workout pages";

    if (upd) {
        if (i == -1) {
            qDebug() << "INFO: No new workout found";
            m_woIndex = m_woLastIndex;
            emit woIndexChanged();
            emit darebeeWoDownloadDone();
            return;
        }
        m_woIndex = i;
        emit woIndexChanged();

        while (m_woIndex <= i + 100) {
            if (m_woIndex == m_woLastIndex) {
                qDebug() << "INFO: Reached the end of the workout download index";
                return;
            }
            downloadData(m_woIndex, true);
            m_woIndex++;
            emit woIndexChanged();
            m_activeDownloads++;
        }
    }
    else {
        while (m_woIndex <= 100) {
            if (m_woIndex == m_woLastIndex) {
                m_woIndex++;
                emit woIndexChanged();
                qDebug() << "INFO: Reached the end of the workout download index";
                return;
            }
            downloadData(m_woIndex, true);
            m_woIndex++;
            emit woIndexChanged();
            m_activeDownloads++;
        }
    }
}

void Project::parseDarebeeWoJson(const QString& woJson, int index) {
    QJsonDocument jsonDoc = QJsonDocument::fromJson(woJson.toUtf8());
    QJsonObject jsonObj = jsonDoc.object();

    for (const QString& key : jsonObj.keys()) {
        if (!key.startsWith("item_")) {
            continue;
        }

        QJsonObject item = jsonObj[key].toObject();
        QString s = "https://darebee.com" + item["url"].toString();
        if (m_data[index].url != s) {
            continue;
        }

        QJsonObject attributes = item["attr"].toObject();
        QString tools = "";
        if (attributes.contains("ct19") && attributes["ct19"].isObject()) {
            tools = attributes["ct19"].toObject()["value"].toArray().toVariantList().first().toString();
        }

        if (tools == "") {
            m_data[index].tools = Data::Tools::unknown;
        }
        else if (tools == "none") {
            m_data[index].tools = Data::Tools::none;
        }
        else if (tools == "dumbbells") {
            m_data[index].tools = Data::Tools::dumbbels;
        }
        else if (tools == "bar") {
            m_data[index].tools = Data::Tools::pullUpBar;
        }
        else if (tools == "other") {
            m_data[index].tools = Data::Tools::other;
        }

        saveConf();
        emit darebeeWoDownloadDone();
        return;
    }

    qDebug() << "INFO: Could not find the tools of the Darebee© workout";
    saveConf();
    emit darebeeWoDownloadDone();
}

void Project::downloadDarebeeWoData() {
    saveDarebeeWoConf();
    if (m_woIndex == m_woLastIndex) {
        qDebug() << "INFO: The download of the workout pages is done";
        emit darebeeWoDownloadDone();
        return;
    }
    qDebug() << "INFO: Continuing the download of the workout pages index is" << m_woIndex;
    int limit = m_woIndex + 100 ;
    if (limit > m_data.size() - 1) {
        limit = m_data.size() - 1;
    }

    while (m_woIndex < limit) {
        downloadData(m_woIndex, true);
        m_woIndex++;
        emit woIndexChanged();
        m_activeDownloads++;
    }
    saveDarebeeWoConf();
}

void Project::downloadDarebeeWoDataCheck() {
    saveDarebeeWoConf();
    qDebug() << "INFO: Continuing the check of the workout pages index is" << m_woIndex << m_woLastIndex;

     while (m_woIndex < m_woLastIndex) {
        if (m_data[m_woIndex].description.isEmpty() || m_data[m_woIndex].ec.isEmpty() || m_data[m_woIndex].map.isEmpty()
            || m_data[m_woIndex].pdf.isEmpty() || m_data[m_woIndex].tools == Data::Tools::unknown || m_data[m_woIndex].focus == Data ::Focus::unknown
            || m_data[m_woIndex].type == Data::Type::unknown) {
            qDebug() << "INFO: Find incomplete workout index is" << m_woIndex;
            downloadData(m_woIndex, true, true);
            m_activeDownloads++;
        }
        m_woIndex++;
        emit woIndexChanged();
        if (m_activeDownloads > 150) {
            return;
        }
    }
}

void Project::checkDarebeeWo() {
    /*
            if (m_data[m_woIndex].description.isEmpty() || m_data[m_woIndex].ec.isEmpty() || m_data[m_woIndex].map.isEmpty()
            || m_data[m_woIndex].pdf.isEmpty() || m_data[m_woIndex].tools == Data::Tools::unknown || m_data[m_woIndex].focus == Data ::Focus::unknown
            || m_data[m_woIndex].type == Data::Type::unknown)*/

    m_activeDownloads = 0;
    m_woIndex = 0;
    emit woIndexChanged();

    m_woLastIndex = m_data.size();
    emit woLastIndexChanged();

    for (const auto& d : std::as_const(m_data)) {
        if (d.description.isEmpty() || d.ec.isEmpty() || d.map.isEmpty()
            || d.pdf.isEmpty() || d.tools == Data::Tools::unknown || d.focus == Data ::Focus::unknown
            || d.type == Data::Type::unknown) {
        //if (d.description.isEmpty()) {
            qDebug() << "INFO: Find incomplete workout index is" << m_woIndex;
            downloadData(m_woIndex, true, true);
            m_activeDownloads++;
        }
        m_woIndex++;
        emit woIndexChanged();
        if (m_activeDownloads > 150) {
            return;
        }
    }
}

void Project::processData(QString &htmlContent, int index, bool isDarebee, bool check) {
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));// + "/darebee");
    if (!dir.exists()) {
        dir.mkpath(dir.absolutePath());
    }
    QStringList subdirs = {"darebee/images/workouts", "darebee/pdf/workouts"};
    for (const QString& subdir : subdirs) {
        QDir subDir(dir.absoluteFilePath(subdir));
        if (!subDir.exists()) {
            subDir.mkpath(subDir.absolutePath());
        }
    }

    QString filePath = dir.absolutePath();
;
    const QString mapIni = "<div class=\"infobox-map\"><img src=\"";
    const QString focusIni = "<div class=\"infobox-works\"><img src=\"/images/infobox/focus-";
    const QString typeIni = "<div class=\"infobox-focus\"><img src=\"/images/infobox/type-";
    const QString diffIni = "<div class=\"infobox-difficulty\"><img src=\"/images/infobox/difficulty-";
    const QString descrIni = "<div class=\"infotext\">";
    const QString ecIni = "<div class=\"infoec\"><strong>Extra Credit:</strong>";
    const QString pdfIni = "<div class=\"infodownload\"><a title=\"Download\" href=\"";
    //const QString picIni = "<figure class=\"none item-image\">\n<img src=\"";

    const int mapBeg = htmlContent.indexOf(mapIni);
    const int focusBeg = htmlContent.indexOf(focusIni);
    const int typeBeg = htmlContent.indexOf(typeIni);
    const int diffBeg = htmlContent.indexOf(diffIni);
    const int descrBeg = htmlContent.indexOf(descrIni);
    const int ecBeg = htmlContent.indexOf(ecIni);
    const int pdfBeg = htmlContent.indexOf(pdfIni);

    if (mapBeg != -1) {
        QString map = htmlContent.sliced(mapBeg + mapIni.size());
        int end = map.indexOf("\" alt=\"");
        if (end > 0 && end < map.size()) {
            map = map.sliced(0, end);
            QUrl mapUrl = "https://darebee.com" + map;
            QNetworkReply* reply = m_manager->get(QNetworkRequest(mapUrl));
            reply->setProperty("map", filePath);
            reply->setProperty("index", index);
            if (isDarebee) {
                m_activeDownloads++;
                qDebug() << "INFO: Another download added, left: " << m_activeDownloads;
                reply->setProperty("isDarebee", true);
                if (check) {
                    reply->setProperty("check", true);
                }
            }
        }
    }

    if (pdfBeg != -1) {
        QString pdf = htmlContent.sliced(pdfBeg + pdfIni.size());
        int end = pdf.indexOf("\" target=\"");
        if (end > 0 && end < pdf.size()) {
            pdf = pdf.sliced(0, end);
            QUrl pdfUrl = "https://darebee.com" + pdf;
            QNetworkReply* reply = m_manager->get(QNetworkRequest(pdfUrl));
            reply->setProperty("pdf", filePath);
            reply->setProperty("index", index);
            if (isDarebee) {
                m_activeDownloads++;
                qDebug() << "INFO: Another download added, left: " << m_activeDownloads;
                reply->setProperty("isDarebee", true);
                if (check) {
                    reply->setProperty("check", true);
                }
            }
        }
    }

    if (focusBeg != -1) {
        QString focus = htmlContent.sliced(focusBeg + focusIni.size());
        int end = focus.indexOf(".jpg\" alt=\"\"");
        if (end > 0 && end < focus.size()) {
            focus = focus.sliced(0, end);
            if (focus == "abs") {
                m_data[index].focus = Data::Focus::abs;
            }
            else if (focus == "back") {
                m_data[index].focus = Data::Focus::back;
            }
            else if (focus == "cardio") {
                m_data[index].focus = Data::Focus::cardio;
            }
            else if (focus == "fullbody") {
                m_data[index].focus = Data::Focus::fullbody;
            }
            else if (focus == "lowerbody") {
                m_data[index].focus = Data::Focus::lowerbody;
            }
            else if (focus == "upperbody") {
                m_data[index].focus = Data::Focus::upperbody;
            }
            else if (focus == "wellbeing") {
                m_data[index].focus = Data::Focus::wellbeing;
            }
            else {
                qDebug() << "WARNING: Found unknow focus: " << focus;
            }
            qDebug() << focus;
        }
    }
    else {
        m_data[index].focus = Data::Focus::unknown;
        qDebug() << "WARNING: Focus not found";
    }

    if (typeBeg != -1) {
        QString type = htmlContent.sliced(typeBeg + typeIni.size());
        int end = type .indexOf(".jpg\" alt=");
        if (end > 0 && end < type.size()) {
            type = type.sliced(0, end);
            if (type == "abs") {
                m_data[index].type = Data::Type::abs;
            }
            else if (type == "burn") {
                m_data[index].type = Data::Type::burn;
            }
            else if (type == "cardio") {
                m_data[index].type = Data::Type::cardio;
            }
            else if (type == "combat") {
                m_data[index].type = Data::Type::combat;
            }
            else if (type == "general") {
                m_data[index].type = Data::Type::general;
            }
            else if (type == "hiit") {
                m_data[index].type = Data::Type::hiit;
            }
            else if (type == "strength") {
                m_data[index].type = Data::Type::strength;
            }
            else if (type == "stretching") {
                m_data[index].type = Data::Type::stretching;
            }
            else if (type == "warmup") {
                m_data[index].type = Data::Type::warmup;
            }
            else if (type == "yoga") {
                m_data[index].type = Data::Type::yoga;
            }
            else {
                qDebug() << "WARNING: Found unknow type: " << type;
            }
            qDebug() << type;
        }
    }
    else {
        m_data[index].type = Data::Type::unknown;
        qDebug() << "WARNING: Type not found";
    }

    if (diffBeg != -1) {
        m_data[index].difficulty = htmlContent.sliced(diffBeg + diffIni.size(), 1).toInt();
        qDebug() << m_data[index].difficulty;
    }

    if (descrBeg != -1) {
        m_data[index].description = htmlContent.sliced(descrBeg + descrIni.size());
        int end = m_data[index].description.indexOf("</div>");
        if (end > 0 && end < m_data[index].description.size()) {
            m_data[index].description = m_data[index].description.sliced(0, end - 1);
            qDebug() << m_data[index].description;
        }
    }

    if (ecBeg != -1) {
        m_data[index].ec = htmlContent.sliced(ecBeg + ecIni.size() + 1);
        int end = m_data[index].ec.indexOf("</div>");
        if (end > 0 && end < m_data[index].ec.size()) {
            m_data[index].ec = m_data[index].ec.sliced(0, end - 1);
            qDebug() << m_data[index].ec;
        }
    }

    if (!isDarebee) {
        findDarebeeWoTools(index);
    }
    else {
        m_activeDownloads--;
        saveDarebeeWoConf();
    }
}

void Project::importConf(bool overwrite, const QString& confPath) {
    const QString& filePath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/projects.json";
    QFileInfo fileInfo(filePath);

    if (!fileInfo.exists() || overwrite) {
        QFile file(filePath);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            qDebug() << "ERROR: Could not open the configuration file";
            return;
        }
        QTextStream stream(&file);

        QFile cfg(confPath);
        if (!cfg.open(QIODevice::ReadOnly | QIODevice::Text)) {
            qDebug() << "ERROR: Could not open the download file";
            return;
        }

        QTextStream s(&cfg);
        while (!s.atEnd()) {
            QString line = s.readLine();
            stream << line << "\n";
        }
        cfg.close();
        file.close();

        readConf();
        return;
    }
    else {
        QFile file(filePath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            return;
        }
        QJsonDocument conf = QJsonDocument::fromJson(file.readAll());
        file.close();

        QFile newFile(confPath);
        if (!newFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
            return;
        }
        QJsonDocument newConf = QJsonDocument::fromJson(newFile.readAll());
        newFile.close();

        if (newConf.isNull()) {
            return;
        }

        QJsonObject obj = conf.object();
        QJsonObject newObj = newConf.object();

        if (newObj.contains("projects") && newObj["projects"].isArray()) {
            QJsonArray newArray = newObj["projects"].toArray();

            QJsonArray array = obj["projects"].toArray();

            for (const QJsonValue& newValue : std::as_const(newArray)) {
                QJsonObject newItem = newValue.toObject();

                QString newName = newItem["name"].toString();
                bool found = false;

                for (int i = 0; i < array.size(); i++) {
                    QJsonObject item = array[i].toObject();

                    QString name = item["name"].toString();

                    if (name == newName) {
                        array[i] = newItem;
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    array.append(newItem);
                }
            }

            QJsonObject mergedConf;
            mergedConf["projects"] = array;

            QJsonDocument mergedDoc(mergedConf);
            QByteArray updatedJsonData = mergedDoc.toJson();

            QFile file(filePath);
            if (file.open(QIODevice::WriteOnly)) {
                file.write(updatedJsonData);
                file.close();
            }

            readConf();
            return;
        }
    }
}

int Project::rowCount(const QModelIndex& parent) const {
    if (parent.isValid()) {
        return 0;
    }
    return m_data.count();
}

QVariant Project::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }
    const Data& data = m_data.at(index.row());
    switch(role) {
    case NameRole:
        return data.name;
        break;
    case AutomaticRole:
        return data.defAutomatic;
        break;
    case PrepareRole:
        return data.defPrepare;
        break;
    case InitRole:
        return data.initPrep;
        break;
    case SetsRole:
        return data.sets;
        break;
    case TimersRole:
        return data.timers.toQVariant();
        break;
    case ExTotalRole:
        return data.exTotal;
        break;
    case SetTotalRole:
        return data.setTotal;
        break;
    case WoTotalRole:
        return data.woTotal;
        break;
    case UrlRole:
        return data.url;
        break;
    case DescriptionRole:
        return data.description;
        break;
    case EcRole:
        return data.ec;
        break;
    case DifficultyRole:
        return data.difficulty;
        break;
    case FocusRole:
        return static_cast<int>(data.focus);
        break;
    case TypeRole:
        return static_cast<int>(data.type);
        break;
    case ToolsRole:
        return static_cast<int>(data.tools);
        break;
    case MapRole:
        return data.map;
        break;
    case PdfRole:
        return data.pdf;
        break;
    default:
        return QVariant();
        break;
    }
}

bool Project::setData(const QModelIndex& index, const QVariant& value, int role) {
    if (!index.isValid() || role < Qt::UserRole)
      return false;

    int row = index.row();

    if (row < 0 || row >= m_data.size())
      return false;

    Data& d = m_data[row];

    switch(role) {
    case NameRole:
        d.name = value.toString();
        break;
    case AutomaticRole:
        d.defAutomatic = value.toBool();
        break;
    case PrepareRole:
        d.defPrepare = value.toBool();
        break;
    case InitRole:
        d.initPrep = value.toBool();
        break;
    case SetsRole:
        d.sets = value.toInt();
        break;
    case TimersRole:
        d.timers = TimerClass(value);
        break;
        //others are set with Data.update()
    case UrlRole:
        d.url = value.toString();
        break;
    case DescriptionRole:
        d.description = value.toString();
        break;
    case DifficultyRole:
        d.difficulty = value.toInt();
        break;
    case FocusRole:
        d.focus = static_cast<Data::Focus>(value.toInt());
        break;
    case TypeRole:
        d.type = static_cast<Data::Type>(value.toInt());
        break;
    case ToolsRole:
        d.tools = static_cast<Data::Tools>(value.toInt());
        break;
    case MapRole:
        d.map = value.toString();
        break;
    case PdfRole:
        d.pdf = value.toString();
        break;
    default:
        return false;
        break;
    }

    emit dataChanged(index, index, {role});

    return true;
}

bool Project::move(int sourceFirst, int dest) {

    bool ret = beginMoveRows(QModelIndex(), sourceFirst, sourceFirst, QModelIndex(), dest);

    if (!ret) {
        return ret;
    }

    if (sourceFirst < dest) {
        m_data.move(sourceFirst, dest - 1);
    }
    else {
        m_data.move(sourceFirst, dest);
    }

    endMoveRows();

    //After that I added a QSortFilterProxyModel call to saveConf() has `ReferenceError: project
    //is not defined` only when called after move()
    saveConf();

    return ret;
}

bool Project::moveTimer(int index, int sourceFirst, int dest) {
    return m_data[index].timers.move(sourceFirst, dest);
}

void Project::add(const Data &data) {
    const int rowOfInsert = m_data.count();
    beginInsertRows(QModelIndex(), rowOfInsert, rowOfInsert);
    m_data.insert(rowOfInsert, data);
    endInsertRows();
}

void Project::copyData(int index) {
    Data d = Data(m_data[index]);
    bool first = true;
    int i = 2;

    while (true) {
        if (first) {
            d.name = d.name + " copy";
            first = false;
            if (checkName(m_data.count(), d.name)) {
                break;
            }
        }
        else {
            if (i == 2) {
                d.name.append(" " + QString::number(i++));
                if (checkName(m_data.count(), d.name)) {
                    break;
                }
            }
            else {
                d.name.chop(2);

                if (i < 11) {
                    d.name.append(" " + QString::number(i++));
                }
                else {
                    d.name.append(QString::number(i++));
                }

                if (checkName(m_data.count(), d.name)) {
                    break;
                }
            }
        }
    }

    add(d);
}

void Project::addNew(const QString name, bool defAutomatic, bool defPrepare, bool initPrep, int sets, const QString& url) {
    Data d = Data(name, defAutomatic, defPrepare, initPrep, sets, url);
    add(d);
}

void Project::addDarebeeWo(const QString& name, const QString& url, const int& id) {
    Data d = Data(name, url, id);
    add(d);
}

void Project::addTimer(int index, const QString name, bool automatic, bool prepare, int countdownValue) {
    const TimerData& td = TimerData(name, automatic, prepare, countdownValue);
    m_data[index].timers.add(td);
    updateData(index);
}

void Project::copyTimer(int index, int timerIndex) {
    const TimerData& td = m_data[index].timers.m_timerData[timerIndex];
    m_data[index].timers.add(td);
    updateData(index);
}

void Project::del(int first, int last) {
    beginRemoveRows(QModelIndex(), first, last);

    if (first == last) {
        m_data.remove(first);
    }
    else {
        m_data.remove(first, last);
    }

    endRemoveRows();
}

void Project::delTimer(int index, int first, int last) {
    m_data[index].timers.del(first, last);
    updateData(index);
}

void Project::updateData(int index) {
    m_data[index].update();
    QModelIndex m = QAbstractItemModel::createIndex(index, index);
    emit dataChanged(m, m);
}

bool Project::checkName(int index, const QString& name) {
    if (name.isEmpty()) {
        return false;
    }
    for (int i = 0; i < m_data.count(); ++i ) {
        if (i == index) {
            continue;
        }
        if (name == m_data[i].name) {
            return false;
        }
    }
    return true;
}

bool Project::checkNewName(const QString& name) {
    if (name.isEmpty()) {
        return false;
    }
    for (const Data& d : std::as_const(m_data)) {
        if (d.name == name) {
            return false;
        }
    }
    return true;
}

int Project::checkDarebeeWoId(const int& id) {
    int i = 0;
    for (const Data& d : std::as_const(m_data)) {
        if (d.id == id) {
            return i;
        }
        i++;
    }
    return -1;
}

bool Project::getDefAutomatic(int index) {
    return m_data[index].defAutomatic;
}

bool Project::getDefPrepare(int index) {
    return m_data[index].defPrepare;
}

QString Project::getUrl(int index) {
    QUrl url(m_data[index].url);
    if (url.isValid()) {
     return url.toString();
    }
    else {
        return QString("");
    }
}

void Project::setData(int index, const QString& name, bool defAutomatic, bool defPrepare, bool initPrep, int sets, const QString& url) {
    m_data[index].defAutomatic = defAutomatic;
    m_data[index].defPrepare = defPrepare;
    m_data[index].initPrep = initPrep;
    m_data[index].name = name;
    m_data[index].sets = sets;
    m_data[index].url = url;
    updateData(index);
}

void Project::setData(int index, const QString& url, const QString& descr, const QString& ec, int difficulty, Data::Focus fc, Data::Type tp, Data::Tools tools) {
    m_data[index].url = url;
    m_data[index].description = descr;
    m_data[index].ec = ec;
    m_data[index].difficulty = difficulty;
    m_data[index].focus = fc;
    m_data[index].type = tp;
    m_data[index].tools = tools;
    updateData(index);
}

void Project::setPdf(int index, const QString& pdf) {
    m_data[index].pdf = pdf;
    updateData(index);
}

void Project::setMap(int index, const QString& map) {
    m_data[index].map = map;
    updateData(index);
}

QVariant Project::getData(int index) {
    QVariantMap resultMap;
    resultMap["defAutomatic"]  = m_data[index].defAutomatic;
    resultMap["defPrepare"]  = m_data[index].defPrepare;
    resultMap["initPrep"] = m_data[index].initPrep;
    resultMap["name"] = m_data[index].name;
    resultMap["sets"] = m_data[index].sets;
    resultMap["woTotal"] = m_data[index].woTotal;
    resultMap["url"] = m_data[index].url;
    return QVariant::fromValue(resultMap);
}

QVariant Project::getOtherData(int index) {
    QVariantMap resultMap;
    resultMap["pdf"]  = m_data[index].pdf;
    resultMap["map"]  = m_data[index].map;
    resultMap["description"]  = m_data[index].description;
    resultMap["ec"] = m_data[index].ec;
    resultMap["difficulty"] = m_data[index].difficulty;
    resultMap["focus"] = static_cast<int>(m_data[index].focus);
    resultMap["type"] = static_cast<int>(m_data[index].type);
    resultMap["tools"] = static_cast<int>(m_data[index].tools);
    return QVariant::fromValue(resultMap);
}

int Project::getCount() {
    return m_data.count();
}

void Project::customAction(const QString& cmd, const QStringList& cmdArgs) {
    QProcess process;
    process.setProgram(cmd);
    process.setArguments(cmdArgs);
    process.startDetached();
}

///////////////////////////////////////////////////////////////ProxyModel

bool ProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const {
    if(this->sourceModel() == nullptr) {
        return false;
    }

    auto mdl = dynamic_cast<Project*>(this->sourceModel());
    auto index = this->sourceModel()->index(source_row, 0, source_parent);

    if (!index.isValid()) {
        return false;
    }

    const Data& d = mdl->m_data[index.row()];
    bool name = (m_nameFilter.isEmpty() || d.name.contains(m_nameFilter, Qt::CaseInsensitive));

    if (m_ss) {
        return name;
    }

    bool diff = true;
    int i = 1;
    for (auto a : m_diffFilter) {
        if (a) {
            if (d.difficulty != i) {
                diff = false;
            }
            else {
                diff = true;
                break;
            }
        }
        ++i;
    }
    //bool diff = (m_diffFilter == 0 || d.difficulty == m_diffFilter);
    bool focus = (m_focusFilter == Data::Focus::unknown || d.focus == m_focusFilter);
    bool type = (m_typeFilter == Data::Type::unknown || d.type == m_typeFilter);
    bool tools = (m_toolsFilter == Data::Tools::unknown || d.tools == m_toolsFilter);

    return name && diff && focus && type && tools;
}

void ProxyModel::setModel(QAbstractItemModel *sourceModel) {
    QSortFilterProxyModel::setSourceModel(sourceModel);
}

bool ProxyModel::getSs() const {
    return m_ss;
}

QString ProxyModel::getNameFilter() const {
    return m_nameFilter;
}

QVector<bool> ProxyModel::getDiffFilter() const {
    return m_diffFilter;
}

Data::Focus ProxyModel::getFocusFilter() const {
    return m_focusFilter;
}

Data::Type ProxyModel::getTypeFilter() const {
    return m_typeFilter;
}

Data::Tools ProxyModel::getToolsFilter() const {
    return m_toolsFilter;
}

void ProxyModel::setSs(const bool b) {
    if (b != m_ss) {
        m_ss = b;
        emit ssChanged();
    }
}

void ProxyModel::setNameFilter(const QString& s) {
    if (s != m_nameFilter) {
        m_nameFilter = s;
        emit nameFilterChanged();
    }
}

void ProxyModel::setDiffFilter(const QVector<bool> v) {
    if (v != m_diffFilter) {
        m_diffFilter = v;
        emit diffFilterChanged();
    }
}

void ProxyModel::setFocusFilter(const Data::Focus f) {
    if (f != m_focusFilter) {
        m_focusFilter = f;
        emit focusFilterChanged();
    }
}

void ProxyModel::setTypeFilter(const Data::Type t) {
    if (t != m_typeFilter) {
        m_typeFilter = t;
        emit typeFilterChanged();
    }
}

void ProxyModel::setToolsFilter(const Data::Tools t) {
    if (t != m_toolsFilter) {
        m_toolsFilter = t;
        emit toolsFilterChanged();
    }
}

int ProxyModel::findIndex(int idx) {
    return this->mapToSource(this->index(idx, 0)).row();
}
