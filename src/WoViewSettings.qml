import QtQuick
import QtQuick.Controls
//import QtQuick.Layouts

MyHeaderFooter {
    id: settingsFooter
    height: leaf.height - 200
    visible: false

    Column {
        id: settingColumn
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 15
        height: parent.height
        spacing: 5

        Row {
            spacing: 40

            Rectangle {
                width: 180
                height: 30
                color: palette.dark
                border.color: useSystemTheme ? palette.accent : "yellow"

                Text {
                    id: nameText
                    color: palette.windowText
                    text: "Initial countdown"
                    height: parent.height - 2
                    anchors.left: parent.left
                    anchors.margins: 8
                    width: 120
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                }
            }

            SpinBox {
                id: control
                value: defInitPrepVal
                editable: true
                height: 30
                to: 99
                from: 1
                onValueModified: defInitPrepVal = control.value
                contentItem: TextInput {
                    z: 2
                    text: control.value
                    font: control.font
                    color: palette.windowText
                    selectionColor: palette.highlight
                    selectedTextColor: palette.highlightedText
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    readOnly: !control.editable
                    validator: control.validator
                    inputMethodHints: Qt.ImhFormattedNumbersOnly
                }
                up.indicator: Rectangle {
                    x: control.mirrored ? 0 : parent.width - width
                    height: parent.height
                    implicitWidth: 35
                    //color: control.up.pressed ? "salmon" : "blue"
                    color: enabled ? "blue" : palette.disabled
                    border.color: enabled ? useSystemTheme ? palette.accent : "yellow" : palette.disabled.buttonText
                    Text {
                        text: "\uF055"
                        font.family: fontLoader.name
                        font.pixelSize: control.font.pixelSize * 2
                        color: palette.windowText
                        anchors.fill: parent
                        fontSizeMode: Text.Fit
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                }
                down.indicator: Rectangle {
                    x: control.mirrored ? parent.width - width : 0
                    height: parent.height
                    implicitWidth: 35
                    //color: control.up.pressed ? "salmon" : "blue"
                    color: enabled ? "blue" : palette.disabled
                    border.color: enabled ? useSystemTheme ? palette.accent : "yellow" : palette.disabled.buttonText
                    anchors.right: control.left
                    Text {
                        text: "\uF056"
                        font.family: fontLoader.name
                        font.pixelSize: control.font.pixelSize * 2
                        color: palette.windowText
                        anchors.fill: parent
                        fontSizeMode: Text.Fit
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            control.decrease()
                            control.valueModified()
                        }
                    }
                }
                background: Rectangle {
                    implicitWidth: 75
                    color: palette.dark
                    border.color: useSystemTheme ? palette.accent : "yellow"
                }
            }/*
            SpinBox {
                id: control2
                editable: true
                height: 30
                from: decimalToInt(0.8)
                value: decimalToInt(zoom)
                to: decimalToInt(6.5)
                stepSize: decimalFactor
                onValueModified: zoom = control2.value

                property int decimals: 2
                property real realValue: value / decimalFactor
                readonly property int decimalFactor: Math.pow(10, decimals)

                function decimalToInt(decimal) {
                    return decimal * decimalFactor
                }

                validator: DoubleValidator {
                    bottom: Math.min(control2.from, control2.to)
                    top:  Math.max(control2.from, control2.to)
                    decimals: control2.decimals
                    notation: DoubleValidator.StandardNotation
                }

                textFromValue: function(value, locale) {
                    return Number(value / decimalFactor).toLocaleString(locale, 'f', control2.decimals)
                }

                valueFromText: function(text, locale) {
                    return Math.round(Number.fromLocaleString(locale, text) * decimalFactor)
                }

                contentItem: TextInput {
                    z: 2
                    text: control2.value
                    font: control2.font
                    color: "lime"
                    selectionColor: "dodgerblue"
                    selectedTextColor: "lightgray"
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    readOnly: !control2.editable
                    inputMethodHints: Qt.ImhFormattedNumbersOnly
                }
                up.indicator: Rectangle {
                    x: control2.mirrored ? 0 : parent.width - width
                    height: parent.height
                    implicitWidth: 35
                    enabled: false
                    //color: control2.up.pressed ? "salmon" : "blue"
                    color: "blue"
                    border.color: enabled ? "yellow" : "white"
                    Text {
                        text: "\uF055"
                        font.family: fontLoader.name
                        font.pixelSize: control2.font.pixelSize * 2
                        color: "lime"
                        anchors.fill: parent
                        fontSizeMode: Text.Fit
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            control2.value += 0.1
                        }
                    }
                }
                down.indicator: Rectangle {
                    x: control2.mirrored ? parent.width - width : 0
                    height: parent.height
                    implicitWidth: 35
                    //color: control2.up.pressed ? "salmon" : "blue"
                    color: "blue"
                    border.color: enabled ? "yellow" : "white"
                    anchors.right: control2.left
                    Text {
                        text: "\uF056"
                        font.family: fontLoader.name
                        font.pixelSize: control2.font.pixelSize * 2
                        color: "lime"
                        anchors.fill: parent
                        fontSizeMode: Text.Fit
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            //control2.decrease()
                            control2.value -= 0.1
                        }
                    }
                }
                background: Rectangle {
                    implicitWidth: 75
                    border.color: "yellow"
                    color: "black"
                }
            }*/
        }

        Row {
            spacing: -5

            Rectangle {
                width: 180
                height: 30
                color: palette.dark
                border.color: useSystemTheme ? palette.accent : "yellow"

                Text {
                    color: palette.windowText
                    text: "Use system theme"
                    height: parent.height - 2
                    anchors.left: parent.left
                    anchors.margins: 8
                    width: 120
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                }
            }

            Rectangle {
                width: 50
                height: 30
                color: "transparent"

                Switch {
                    id: control2
                    anchors.verticalCenter: parent.verticalCenter
                    //anchors.horizontalCenter: parent.horizontalCenter
                    anchors.left: parent.left
                    anchors.margins: 16
                    height: 22
                    checked: useSystemTheme
                    indicator: Rectangle {
                        implicitWidth: 50
                        implicitHeight: 22
                        radius: 9
                        color: control2.checked ? "green" : "coral"
                        border.color: control2.checked ? "deepskyblue" : "khaki"
                        Rectangle {
                            x: control2.checked ? parent.width - width : 0
                            width: 22
                            height: 22
                            radius: 11
                            color: control2.down ? "blue" : "#494B53"
                            border.color: control2.checked ? (control2.down ? "indianred" : "black") : "navy"
                        }
                    }
                    onCheckedChanged: {
                        useSystemTheme = checked
                    }
                }
            }
        }

        Row {
            spacing: -5

            Rectangle {
                width: 180
                height: 30
                color: palette.dark
                border.color: useSystemTheme ? palette.accent : "yellow"

                Text {
                    color: palette.windowText
                    text: "Vibration (uses sxmo_vibrate)"
                    height: parent.height - 2
                    anchors.left: parent.left
                    anchors.margins: 8
                    width: 120
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                }
            }

            Rectangle {
                width: 50
                height: 30
                color: "transparent"

                Switch {
                    id: customActionControl
                    anchors.verticalCenter: parent.verticalCenter
                    //anchors.horizontalCenter: parent.horizontalCenter
                    anchors.left: parent.left
                    anchors.margins: 16
                    height: 22
                    checked: customAction
                    indicator: Rectangle {
                        implicitWidth: 50
                        implicitHeight: 22
                        radius: 9
                        color: customActionControl.checked ? "green" : "coral"
                        border.color: customActionControl.checked ? "deepskyblue" : "khaki"
                        Rectangle {
                            x: customActionControl.checked ? parent.width - width : 0
                            width: 22
                            height: 22
                            radius: 11
                            color: customActionControl.down ? "blue" : "#494B53"
                            border.color: customActionControl.checked ? (customActionControl.down ? "indianred" : "black") : "navy"
                        }
                    }
                    onCheckedChanged: {
                        customAction = checked
                    }
                }
            }
        }

        Row {
            spacing: 40

            Rectangle {
                width: 180
                height: 30
                color: palette.dark
                border.color: useSystemTheme ? palette.accent : "yellow"

                Text {
                    id: zoomText
                    color: palette.windowText
                    text: "Zoom"
                    height: parent.height - 2
                    anchors.left: parent.left
                    anchors.margins: 8
                    width: 120
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                }
            }

            SpinBox {
                id: zoomControl
                property real zoomReal: zoom * 100
                //property int zoomPercent: zoomReal * 10
                value: zoomReal
                editable: true
                height: 30
                to: 190
                from: 80
                stepSize: 10
                onValueModified: zoom = zoomControl.value / 100
                contentItem: TextInput {
                    z: 2
                    text: zoomControl.value
                    font: zoomControl.font
                    color: palette.windowText
                    selectionColor: palette.highlight
                    selectedTextColor: palette.highlightedText
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    readOnly: !zoomControl.editable
                    validator: zoomControl.validator
                    inputMethodHints: Qt.ImhFormattedNumbersOnly
                }
                up.indicator: Rectangle {
                    x: zoomControl.mirrored ? 0 : parent.width - width
                    height: parent.height
                    implicitWidth: 35
                    //color: zoomControl.up.pressed ? "salmon" : "blue"
                    color: enabled ? "blue" : palette.disabled
                    border.color: enabled ? useSystemTheme ? palette.accent : "yellow" : palette.disabled.buttonText
                    Text {
                        text: "\uF055"
                        font.family: fontLoader.name
                        font.pixelSize: zoomControl.font.pixelSize * 2
                        color: palette.windowText
                        anchors.fill: parent
                        fontSizeMode: Text.Fit
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                }
                down.indicator: Rectangle {
                    x: zoomControl.mirrored ? parent.width - width : 0
                    height: parent.height
                    implicitWidth: 35
                    //color: zoomControl.up.pressed ? "salmon" : "blue"
                    color: enabled ? "blue" : palette.disabled
                    border.color: enabled ? useSystemTheme ? palette.accent : "yellow" : palette.disabled.buttonText
                    anchors.right: zoomControl.left
                    Text {
                        text: "\uF056"
                        font.family: fontLoader.name
                        font.pixelSize: zoomControl.font.pixelSize * 2
                        color: palette.windowText
                        anchors.fill: parent
                        fontSizeMode: Text.Fit
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            zoomControl.decrease()
                            zoomControl.valueModified()
                        }
                    }
                }
                background: Rectangle {
                    implicitWidth: 75
                    color: palette.dark
                    border.color: useSystemTheme ? palette.accent : "yellow"
                }
            }
        }
    }

    MyButton {
        id: closeSettingsButton
        text: "\uF00C"
        tipEnabled: false
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 15
        height: 36
        width: 36
        onClicked: {
            toggleSettings()
        }
    }
}
