import QtQuick
import QtQuick.Controls

MyHeaderFooter {
    id: woViewHeader
    property color windowTextColor: palette.windowText
    property color textColor: palette.text
    property color windowColor: palette.window
    property color placeHolderTextColor: palette.placeholderText
    property color highlightColor: palette.highlight
    property color midColor: palette.mid
    property color lightColor: palette.light
    //property alias progressRow: progressCol.progressRow
    //property bool downloading: false

    MyButton {
        id: expButton
        //text: "\uF00D"
        text: woViewSearchLeaf.state === "" ? "" : ""
        tip: qsTr("Toggle the filter")
        pixelSide: 10
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 15
        anchors.topMargin: 7
        width: 42
        onClicked: {
            toggleFilter()
        }
    }

    VolumeBar {
        id: volBar
        space: 565
        anchors.left: expButton.right
        anchors.margins: 4
    }

    MyButton {
        id: startButton
        text: "󰁪"
        tip: qsTr("Update the missing data only")
        anchors.left: volBar.right
        anchors.leftMargin: 12
        anchors.top: parent.top
        anchors.margins: 6
        anchors.topMargin: 7
        enabled: !leaf.downloading && view.count
        onClicked: {
            leaf.downloading = true
            darebeeProject.checkDarebeeWo()
        }
    }

    MyButton {
        id: dbDownload
        text: "\u{F1463}"
        tip: qsTr("Download workouts/check for new ones from Darebee©")
        anchors.left: startButton.right
        anchors.top: parent.top
        anchors.margins: 6
        anchors.topMargin: 7
        enabled: !leaf.downloading
        onClicked: {
            leaf.downloading = true
            darebeeProject.downloadDarebeeWo()
        }
    }

    Column {
        id: progressCol
        anchors.left: dbDownload.right
        anchors.leftMargin: 10
        anchors.verticalCenter: parent.verticalCenter
        spacing: 5
        //property alias progressRow: progressRow

        Text {
            id: progressText
            text: darebeeWoIndex + " / " + darebeeWoLastIndex
            //text: darebeeWoLastIndex - darebeeWoIndex + " / " + darebeeWoLastIndex
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
            visible: parent.state === "" ? true : false
            color: useSystemTheme ? palette.highlight : "lime"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            style: Text.Outline
            styleColor: useSystemTheme ? palette.accent : "black"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    progressRow.toggleState()
                }
            }
        }

        MyProgressBar {
            id: bar
            //value: (darebeeWoLastIndex - darebeeWoIndex) / darebeeWoLastIndex
            value: darebeeWoIndex / darebeeWoLastIndex
        }
    }

    BusyIndicator {
        id: busyInd
        running: leaf.downloading
        //running: true
        anchors.left: progressCol.right
        anchors.verticalCenter: parent.verticalCenter
    }/*
        contentItem: Item {
            implicitWidth: 64
            implicitHeight: 64

            Item {
                id: item
                x: parent.width / 2 - 32
                y: parent.height / 2 - 32
                width: 64
                height: 64
                opacity: control.running ? 1 : 0

                Behavior on opacity {
                    OpacityAnimator {
                        duration: 250
                    }
                }

                RotationAnimator {
                    target: item
                    running: control.visible && control.running
                    from: 0
                    to: 360
                    loops: Animation.Infinite
                    duration: 1250
                }

                Repeater {
                    id: repeater
                    model: 6

                    Rectangle {
                        id: delegate
                        x: item.width / 2 - width / 2
                        y: item.height / 2 - height / 2
                        implicitWidth: 10
                        implicitHeight: 10
                        radius: 5
                        color: "#21be2b"

                        required property int index

                        transform: [
                            Translate {
                                y: -Math.min(item.width, item.height) * 0.5 + 5
                            },
                            Rotation {
                                angle: delegate.index / repeater.count * 360
                                origin.x: 5
                                origin.y: 5
                            }
                        ]
                    }
                }
            }
        }
    }*/

/*
        Row {
            id: progressRow
            spacing: 10

            function toggleState() {
                if (state === "") {
                    state = "editing"
                }
                else {
                    state = ""
                }
            }

            SmallIconButton {
                icon.source: progressText.visible ? "qrc:/icons/Adwaita/document-edit-symbolic.svg" : "qrc:/icons/Adwaita/object-select-symbolic.svg"
                tip: qsTr("Toggle editing the current page")
                onClicked: {
                    parent.toggleState()
                }
            }

            Text {
                id: progressText
                text: darebeeWoIndex + " / " + darebeeWoLastIndex
                //text: darebeeWoLastIndex - darebeeWoIndex + " / " + darebeeWoLastIndex
                //anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 12
                visible: parent.state === "" ? true : false
                color: useSystemTheme ? palette.highlight : "lime"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
                style: Text.Outline
                styleColor: useSystemTheme ? palette.accent : "black"

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        progressRow.toggleState()
                    }
                }
            }

            TextField {
                text: ""
                placeholderText: "0 / " + darebeeWoLastIndex
                placeholderTextColor: placeHolderTextColor
                validator: IntValidator { bottom: 0; top: darebeeWoLastIndex }
                font.pixelSize: 12
                focus: true
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: -1
                visible: parent.state === "" ? false : true
                color: useSystemTheme ? highlightColor : "lime"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                background: Rectangle {
                    //implicitWidth: 120
                    //height: 16
                    color: windowColor
                    radius: 2
                    //border.color: enabled ? lightColor : midColor
                }

                onTextEdited: {
                    if (text <= darebeeWoLastIndex) {
                        //darebeeProject.woIndex = darebeeWoLastIndex - text
                        darebeeProject.woIndex = text
                    }
                    else {
                        clear()
                    }
                }

                onEditingFinished: {
                    parent.state = ""
                }
*/
                /*
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        progressRow.toggleState()
                    }
                }
                *//*
            }

            states: [
                State {
                    name: "editing"
                }
            ]
        }

        MyProgressBar {
            id: bar
            //value: (darebeeWoLastIndex - darebeeWoIndex) / darebeeWoLastIndex
            value: darebeeWoIndex / darebeeWoLastIndex
        }
    }*/

    MyButton {
        id: quitButton
        //text: "\uF00D"
        text: "\uF053"
        tip: qsTr("Go back to the initial page")
        pixelSide: 10
        anchors.right: parent.right
        anchors.margins: 15
        anchors.verticalCenter: parent.verticalCenter
        width: 42
        onClicked: {
            popStack()
        }
    }
}

