import QtQuick
import QtQuick.Controls

Column {
    width: parent.width
    spacing: 6

    Row {
        anchors.left: parent.left
        anchors.margins: 6
        spacing: 6

        SmallButton {
            text: "\uF00D"
            tip: qsTr("Remove this filter")
            height: 20
            width: 20
            onClicked: {
                resetType()
            }
        }

        Text {
            text: qsTr("Type")
            font.bold: true
            color: palette.text
        }
    }

    TypeComboBox {
        id: typeBox
    }

    function resetType() {
        typeBox.currentIndex = 0
        tp = 0
    }
}
