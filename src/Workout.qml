import QtQuick

Item {
    id: woLeaf
    width: parent.width
    height: 30

    Rectangle {
        id: wrapper
        color: currentWo == woLeaf ? useSystemTheme ? palette.highlight : palette.highlightedText :
        useSystemTheme ? palette.midlight : palette.mid
        border.color: currentWo == woLeaf ? (useSystemTheme ? palette.dark : "salmon") : palette.light
        border.width: 2
        width: parent.width
        height: parent.height
        Text {
            id: woText
            font.pixelSize: parent.height - 8
            anchors.left: parent.left
            anchors.leftMargin: buttonRow.width + 5
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 10
            color: useSystemTheme ? palette.text : palette.window
            elide: Text.ElideRight
            width: parent.width - (anchors.leftMargin + exerciseCount.width + 7)// no expand button
            //width: parent.width - (anchors.leftMargin + exerciseCount.width + 35)
            text: model.name
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if (view.interactive) {
                view.currentIndex = index
            }
        }
        onDoubleClicked: {
            start()
        }
        onPressAndHold: {
            start()
        }
    }

    MyPopup {
        id: errDialog
        width: errText.width + 18
        Text {
            id: errText
            text: "Project must have at least\n one timer to be started"
            wrapMode: Text.WordWrap
            anchors.centerIn: parent
            anchors.verticalCenterOffset: -25
            horizontalAlignment: Text.AlignHCenter
            color: "red"
        }
    }

    function start() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        view.forceActiveFocus()
        //view.currentIndex = index
        view.currentIndex = proxyModel.findIndex(index)
        if (!model.exTotal) {
            errDialog.open()
            return
        }
        var timerView = Qt.createComponent("qrc:/TimerView.qml")
        if (timerView.status === Component.Ready) {
            stack.push(timerView.createObject(stack, {"model": model.timers, "initPrep": model.initPrep, "sets": model.sets,
                                       "exTotal": model.exTotal, "setTotal": model.setTotal, "woTotal": model.woTotal,
                                       "pdf": model.pdf}))
        }
        else {
            console.error("ERROR: Cannot create TimerView component: " + timerView.errorString())
        }
    }

    function editWo() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        view.currentIndex = proxyModel.findIndex(index)
        var editWo = Qt.createComponent("EditWo.qml")
        if (editWo.status === Component.Ready) {
            editWo.createObject(stack)
            mainEsc.enabled = false
            mainConfirm.enabled = false
            view.interactive = false
        }
        else {
            console.error("ERROR: Cannot create EditWo component: " + editWo.errorString())
        }
    }

    function edit() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        view.currentIndex = proxyModel.findIndex(index)
        var exView = Qt.createComponent("ExView.qml")
        if (exView.status === Component.Ready) {
            stack.push(exView.createObject(stack, {"model": model.timers, "proIndex": view.currentIndex}))
        }
        else {
            console.error("ERROR: Cannot create ExView component: " + exView.errorString())
        }
    }

    function copy() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        project.copyData(proxyModel.findIndex(index))
        view.currentIndex = view.count - 1
    }

    function delWo() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        project.del(proxyModel.findIndex(index), proxyModel.findIndex(index))
        project.saveConf()
    }

    function expand() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        view.currentIndex = proxyModel.findIndex(index)
        var woExp = Qt.createComponent("WoExp.qml")
        if (woExp.status === Component.Ready) {
            mainConfirm.enabled = false
            stack.push(woExp.createObject(stack, {"proIndex": view.currentIndex, "name": model.name, "pdf": model.pdf,
                "url": model.url, "map": model.map, "descr": model.description, "ec": model.ec, "difficulty": model.difficulty,
                "fc": model.focus, "tp": model.type, "tools": model.tools}))
        }
        else {
            console.error("ERROR: Cannot create WoExp component: " + woExp.errorString())
        }
    }

    Row {
        id: buttonRow
        anchors.left: parent.left
        anchors.margins: 2
        anchors.verticalCenter: parent.verticalCenter
        spacing: 2

        SmallButton {
            id: startButton
            text: "\uE0B0"
            tip: qsTr("Start the selected project")
            pixelSide: 6
            onClicked: {
                start()
            }
        }

        SmallButton {
            id: expButton
            //anchors.top: parent.top
            //anchors.margins: 2
            //anchors.right: parent.right
            text: "\uF50C"
            tip: qsTr("WIP Open the expanded view of the selected project")
            onClicked: {
                expand()
            }
        }

        SmallButton {
            id: editWoButton
            text: "\uF013"
            tip: qsTr("Edit the selected project")
            onClicked: {
                editWo()
            }
        }

        SmallButton {
            id: editButton
            tip: qsTr("Open the selected project edit page with the timers")
            text: "\uEB6A"
            onClicked: {
                edit()
            }
        }

        SmallButton {
            text: "\uF0C5"
            tip: qsTr("Make a copy of the selected project")
            onClicked: {
                copy()
            }
        }

        SmallButton {
            id: upButton
            text: "\u2191"
            visible: !search
            tip: qsTr("Move up the selected project, press and hold to move to the beginning")
            onClicked: {
                moveUp()
            }
            onPressAndHold: {
                moveStart()
            }
        }

        SmallButton {
            id: downButton
            text: "\u2193"
            visible: !search
            tip: qsTr("Move down the selected project, press and hold to move to the end")
            onClicked: {
                moveDown()
            }
            onPressAndHold: {
                moveEnd()
            }
        }

        SmallButton {
            id: deleteButton
            text: "\uF00D"
            tip: qsTr("Delete the selected project")
            onClicked: {
                delWo()
            }
        }
    }

    SmallButton {
        id: exerciseCount
        anchors.top: parent.top
        anchors.margins: 2
        anchors.right: parent.right
        text: model.exTotal
        tip: qsTr("Number of timers of the selected project")
        width: text.width
        pixelSide: 8
    }
}
/*
    SmallButton {
        id: expButton
        anchors.top: parent.top
        anchors.margins: 2
        anchors.right: parent.right
        text: "\uF50C"
        tip: qsTr("Open the expanded view the selected project")
        onClicked: {
            if (parent.state === "") {
                parent.state = "expanded"
            }
            else {
                parent.state = ""
            }
        }
    }

    Text {
        id: description
        //text: model.description
        text: ""
        anchors.centerIn: parent
        color: palette.text
        visible: false
    }

    Rectangle {
        id: image
        width: view.width
        height: 26
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 2
        color: "black"

        Image {
            anchors.fill:parent
            fillMode: Image.PreserveAspectFit
            //source: model.image
        }
    }

    states: [
        State {
            name: "expanded"
            PropertyChanges { target: woLeaf; height: woViewLeaf.view.height }
            PropertyChanges { target: wrapper; height: woViewLeaf.view.height }
            PropertyChanges { target: description; visible: true }
            PropertyChanges { target: image; visible: true }
            PropertyChanges { target: buttonRow; visible: false }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation {
                duration: 200;
                properties: "height,visible"
            }
        }
    ]

    onStateChanged: {
        console.log("State changed to: " + state);
    }*/
//}
