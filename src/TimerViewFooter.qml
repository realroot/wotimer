import QtQuick

MyHeaderFooter {
    id: timerViewFooter

    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 6

        MyButton {
            id: restartButton
            text: "\u{F04AB}"
            tip: qsTr("Restart the current timer")
            pixelSide: 4
            onClicked: {
                restartTimer()
            }
        }

        MyButton {
            id: backwardButton
            text: "\u{F04AE}"
            tip: qsTr("Send 10 seconds back the current timer")
            pixelSide: 4
            onClicked: {
                backward()
            }
        }

        MyButton {
            id: playButton
            text: {
                if (currentTimer.countdownTimer.running)
                    "\uF04C"
                else
                    "\uE0B0" //Play
            }
            tip: qsTr("Play/pause the current timer")
            onClicked: {
                play()
            }
        }

        MyButton {
            id: forwardButton
            text: "\u{F04AD}"
            tip: qsTr("Send 10 seconds forward the current timer")
            pixelSide: 4
            onClicked: {
                forward()
            }
        }

        MyButton {
            id: endButton
            text: "\u{F04AC}"
            tip: qsTr("End the current timer")
            pixelSide: 4
            onClicked: {
                endTimer()
            }
        }
    }
}
