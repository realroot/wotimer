import QtQuick
import QtQuick.Controls

MyComboBox {
    id: control
    //signal doFunction(int i)

    delegate: MyButton {
        text: items[index].text
        tip: items[index].tip

        background: Rectangle {
            color: palette.button
            border.color: hovered ? useSystemTheme ? palette.accent : "dodgerblue" : palette.buttonText
            border.width: 1
        }

        onClicked: {
            eval(items[index].command)
            //doFunction(index)
        }
    }

    Text {
        text: "\u{F085}"
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.margins: 4
        font.family: parent.font.family
        font.pixelSize: 22
        color: hovered ? useSystemTheme ? palette.accent : "dodgerblue" : palette.buttonText
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }
}

