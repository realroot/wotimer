import QtQuick
import QtQuick.Controls

Column {
    width: parent.width
    spacing: 6

    Row {
        anchors.left: parent.left
        anchors.margins: 6
        spacing: 6

        SmallButton {
            text: "\uF00D"
            tip: qsTr("Remove this filter")
            height: 20
            width: 20
            onClicked: {
                searchBox.text = ""
            }
        }

        Text {
            text: qsTr("Name")
            font.bold: true
            color: palette.text
        }
    }

    TextField {
        id: searchBox
        text: qsTr("")
        placeholderText: qsTr("Filter by name")
        //placeholderText: qsTr("Filter by name (insensitive case)")
        placeholderTextColor: "darkgray"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 6
        padding: 6
        height: 30
        font.pixelSize: 12
        color: useSystemTheme ? windowTextColor : "yellow"

        background: Rectangle {
            color: palette.dark
            border.color: useSystemTheme ? palette.accent : "yellow"
            radius: 6
        }

        onTextChanged: {
            nameChanged(text)
        }
    }
}
