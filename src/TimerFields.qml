import QtQuick
import QtQuick.Layouts
import "my_functions.js" as Functions

ColumnLayout {
    id: timerFields
    spacing: 0
    //anchors.centerIn: parent
    property alias ani: warning.ani

    Rectangle {
        id: warning
        color: "red"
        border.color: "coral"
        border.width: 2
        Layout.fillWidth: true
        Layout.preferredHeight: 30
        visible: false
        property alias ani: ani

        Text {
            id: fieldText
            color: "yellow"
            text: "Value or name cannot be empty"
            height: parent.height - 2
            font.bold: true
            anchors.left: parent.left
            anchors.margins: 8
            width: parent.width - 10
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            elide: Text.ElideRight
        }

        SequentialAnimation {
            id: ani
            running: false
            ColorAnimation {
                target: warning
                property: "color"
                to: "red"
                duration: 400
            }
            ColorAnimation {
                target: warning
                property: "color"
                to: "transparent"
                duration: 400
            }
            ColorAnimation {
                target: warning
                property: "color"
                to: "red"
                duration: 400
            }
            ColorAnimation {
                target: warning
                property: "color"
                to: "transparent"
                duration: 400
            }
            onStarted: {
                warning.visible = true
            }
            onStopped: {
                warning.visible = false
            }
        }
    }

    Field {
        text: "Timer name"
        valuePlaceholder: "New"
        tip: qsTr("Name cannot be empty but it can be repeated")
        valueText: name
        validator: null
        function updateValue(text) {
            name = text
        }
    }

    Item {
        Layout.fillWidth: true
        Layout.preferredHeight: 13
    }

    Field {
        text: "Hours"
        tip: qsTr("Number of hours of the timer")
        valueText: Functions.formatTime(countdownValue, "h")
        validator: IntValidator { bottom: 0; top: 100000 }
        function updateValue(text) {
            if (text === "") {
                hours = 0
            } else {
                hours = text
            }
        }
    }

    Field {
        text: "Minutes"
        tip: qsTr("Number of minutes of the timer")
        valueText: Functions.formatTime(countdownValue, "m")
        validator: IntValidator { bottom: 0; top: 100000 }
        function updateValue(text) {
            if (text === "") {
                minutes = 0
            } else {
                minutes = text
            }
        }
    }

    Field {
        text: "Seconds"
        tip: qsTr("Number of seconds of the timer")
        valueText: Functions.formatTime(countdownValue, "s")
        validator: IntValidator { bottom: 0; top: 100000 }
        function updateValue(text) {
            if (text === "") {
                seconds = 0
            } else {
                seconds = text
            }
        }
    }

    Item {
        Layout.fillWidth: true
        Layout.preferredHeight: 13
    }

    SwitchField {
        text: "Autostart"
        tip: qsTr("Start the timer automatically when the previous one ends")
        checked: automatic
        onCheckedChanged: {
            automatic = checked
        }
    }

    SwitchField {
        text: "Alert"
        tip: qsTr("5 seconds before that the timer ends play a voice saying \"Get ready!\"")
        checked: prepare
        onCheckedChanged: {
            prepare = checked
        }
    }

    Item {
        Layout.fillWidth: true
        Layout.preferredHeight: 8
    }

    Item {
        Layout.fillWidth: true
        Layout.preferredHeight: 36

        MyButton {
            id: confirmButton
            text: "\uF00C"
            tipEnabled: false
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenterOffset: -32
            onClicked: {
                confirm()
            }
        }

        MyButton {
            id: discardButton
            text: "\uF00D"
            tipEnabled: false
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenterOffset: 32
            onClicked: {
                quitEditView()
            }
        }
    }

    Shortcut {
        sequences: ["esc", "ctrl+q"]
        onActivated: {
            quitEditView()
        }
    }

    Shortcut {
        sequences: ["return", "space", "ctrl+space", "ctrl+return"]
        onActivated: {
            confirm()
        }
    }
}
