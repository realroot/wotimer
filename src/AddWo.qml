import QtQuick
import QtQuick.Controls.Fusion
import QtQuick.Layouts

Page {
    id: viewEditleaf
    anchors.centerIn: parent
    width: 264
    height: 290

    background: Rectangle {
        anchors.fill: parent
        color: palette.window
        border.color: palette.accent
        border.width: 2
        radius: 10
    }

    property int sets: 1
    property string name: ""
    property string url: ""
    property bool initPrep: true
    property bool defAutomatic: true
    property bool defPrepare: true

    Flickable {
    id: flickable
        anchors.fill: parent
        contentHeight: woFields.height
        topMargin: (height - woFields.height) / 2
        leftMargin: 11
        clip: true
        flickableDirection: Flickable.VerticalFlick

        onContentYChanged: {
            if (leaf.height < viewEditleaf.height) {
                contentY = contentY
            }
        }

        WoFields { id: woFields }
    }

    function confirm() {
        name = name.trim()
        if (project.checkNewName(name)) {
            if (sets < 1) {
                woFields.setError = true
                woFields.ani.start()
                return
            }
            else {
                project.addNew(name, defAutomatic, defPrepare, initPrep, sets, url)
                project.saveConf()
                quitEditView()
            }
        }
        else {
            woFields.ani.start()
        }
    }

    function quitEditView() {
        viewEditleaf.destroy()
        mainEsc.enabled = true
        mainConfirm.enabled = true
        view.interactive = true
    }
}
