import QtQuick
import QtQuick.Controls

MyHeaderFooter {
    id: woViewHeader
    property color windowTextColor: palette.windowText

    MyButton {
        id: settingsButton
        text: "\uF013"
        tip: qsTr("Overall settings")
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: 15
        anchors.topMargin: 7
        pixelSide: 8
        onClicked: {
            toggleSettings()
        }
    }

    MyButton {
        text: "🔍"
        tip: qsTr("Filter projects by name (insensitive case)")
        anchors.right: darebbeWoButton.left
        anchors.top: parent.top
        anchors.margins: 6
        anchors.topMargin: 7
        contentItem: Text {
            text: parent.text
            font.family: parent.font.family
            font.pixelSize: parent.height - 14
            color: parent.hovered ? useSystemTheme ? palette.accent : "dodgerblue" : palette.buttonText
            horizontalAlignment: Text.AlignHCenter
            //verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }
        onClicked: {
            toggleSearch()
        }
    }

    MyButton {
        id: darebbeWoButton
        //text: "󱉶"
        //text: "D©"
        text: "󱅝"
        /*
        height: 36
        width: 50
        icon.source: "qrc:/icons/darebee-wo.svg"
        icon.height: 170
        icon.width: 176*/
        tip: qsTr("Open the Darebee© workouts page")
        anchors.right: aboutButton.left
        anchors.top: parent.top
        anchors.margins: 6
        anchors.topMargin: 7
        pixelSide: 8
        //pixelSide: 14
        onClicked: {
            darebeeWo()
        }
    }

    MyButton {
        id: aboutButton
        text: "\uEB32"
        tip: qsTr("About this application")
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 15
        anchors.topMargin: 7
        pixelSide: 8
        onClicked: {
            aboutDialog.open()
        }
    }

    VolumeBar {
        id: volBar
        space: 426
        anchors.left: settingsButton.right
        anchors.margins: 4
    }

    Text {
        id: projectsText
        text: view.count + qsTr(" project/s")
        anchors.left: volBar.right
        anchors.margins: 16
        anchors.top: parent.top
        anchors.topMargin: 14
        //verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
        font.pixelSize: 14
        height: parent.height - 2
        font.bold: true
        width: parent.width - 432
        color: useSystemTheme ? palette.windowText : "royalblue"
        style: Text.Outline
        styleColor: useSystemTheme ? palette.accent : "black"
    }

    function toggleSearch() {
        if (state === "expanded") {
            searchBox.text = ""
            state = ""
        }
        else {
            state = "expanded"
            searchBox.forceActiveFocus()
        }
    }

    function closeSearch() {
        searchBox.text = ""
        state = ""
    }

    function toggleAdvSearch() {
        if (state === "advSearch") {
            searchBox.text = ""
            state = ""
        }
        else {
            state = "advSearch"
        }
    }

    function toggleSearches() {
        if (state === "advSearch") {
            state = "expanded"
        }
        else {
            state = "advSearch"
        }
    }

    states: [
        State {
            name: "expanded"

            PropertyChanges {
                target: woViewHeader; height: 90
            }
            PropertyChanges {
                target: proxyModel; ss: true
            }
        },
        State {
            name: "advSearch"

            PropertyChanges {
                target: woViewHeader; height: 180
            }
            PropertyChanges {
                target: proxyModel; ss: false
            }
        }
    ]

    transitions: Transition {
        NumberAnimation { properties: "height"; duration: 300 }
    }

    onStateChanged:  {
        if (state !== "advSearch") {
            woViewAdvSearch.resetAll()
        }
    }

    SmallButton {
        id: toggleAdvButton
        text: woViewHeader.state === "advSearch" ? "" : ""
        width: 30
        height: 30
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 10
        anchors.topMargin: 52
        visible: woViewHeader.state === "" ? false : true
        tip: qsTr("Open advanced search")

        Behavior on visible {
            NumberAnimation { duration: visible ? 0 : 400 }
        }

        onClicked: {
            toggleSearches()
        }
    }

    SmallButton {
        id: resetNameButton
        text: "\uF00D"
        tip: qsTr("Remove the name filter")
        height: 23
        width: 23
        anchors.top: parent.top
        anchors.left: toggleAdvButton.right
        anchors.margins: 10
        anchors.topMargin: 52
        visible: woViewHeader.state === "" ? false : true
        onClicked: {
            searchBox.text = ""
        }
    }

    TextField {
        id: searchBox
        text: qsTr("")
        //visible: parent.state === "expanded" ? true : false
        visible: woViewHeader.state === "" ? false : true
        placeholderText: qsTr("Filter projects by name (insensitive case)")
        placeholderTextColor: "darkgray"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        anchors.top: parent.top
        //anchors.left: parent.left
        anchors.left: resetNameButton.right
        anchors.right: parent.right
        anchors.margins: 10
        anchors.topMargin: 52
        padding: 6
        height: 30
        font.pixelSize: 12
        color: useSystemTheme ? windowTextColor : "yellow"

        Behavior on visible {
            NumberAnimation { duration: visible ? 0 : 400 }
        }

        background: Rectangle {
            color: palette.dark
            border.color: useSystemTheme ? palette.accent : "yellow"
            radius: 6
        }

        //onTextEdited: {
        onTextChanged: {
            proxyModel.nameFilter = text
            proxyModel.invalidate()
        }
    }

    WoViewAdvSearch {
        id: woViewAdvSearch
        visible: woViewHeader.state === "advSearch" ? true : false
        anchors.top: searchBox.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 15
    }
}
