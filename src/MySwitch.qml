import QtQuick
import QtQuick.Controls

Switch {
    id: control
    anchors.verticalCenter: parent.verticalCenter
    //anchors.horizontalCenter: parent.horizontalCenter
    anchors.left: parent.left
    anchors.margins: 16
    height: 22
    //checked:
    indicator: Rectangle {
        implicitWidth: 50
        implicitHeight: 22
        radius: 9
        color: control.checked ? "green" : "coral"
        border.color: control.checked ? "deepskyblue" : "khaki"
        Rectangle {
            x: control.checked ? parent.width - width : 0
            width: 22
            height: 22
            radius: 11
            color: control.down ? "blue" : "#494B53"
            border.color: control.checked ? (control.down ? "indianred" : "black") : "navy"
        }
    }
}
