#ifndef PROJECT_H
#define PROJECT_H

#include <QAbstractListModel>
#include <QVariant>
#include <QSortFilterProxyModel>
//save
#include <QFile>
#include <QTextStream>
#include <QStandardPaths>
//json file
#include <QDir>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
// download conf
#include <QNetworkAccessManager>
#include <QNetworkReply>
// custom action
#include <QProcess>
#include <QQueue>

struct TimerData {
    TimerData() {}
    TimerData(const QString name, bool automatic, bool prepare, int countdownValue) :
        name(name), automatic(automatic), prepare(prepare), countdownValue(countdownValue) {}
    QString name;
    bool automatic;
    bool prepare;
    int countdownValue;
};

class TimerClass : public QAbstractListModel {
    Q_OBJECT
    friend class Project;
    friend class Data;

public:
    QHash<int, QByteArray>roleNames() const override;

    enum Roles {
        NameRole = Qt::UserRole,
        AutomaticRole,
        ValueRole,
        PrepareRole,
    };

    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
    TimerClass() {};
    TimerClass(const TimerClass& other);
    TimerClass& operator=(const TimerClass& other);
    TimerClass(const QVariant& value);
    QVariant toQVariant() const;

public slots:
    void add(const TimerData& timerData);
    bool move(int sourceFirst, int dest);
    void del(int first, int last);

private:
    QVector<TimerData> m_timerData;
};

struct Data {
    Data() {}
    Data(const QString& name, bool defAutomatic, bool defPrepare, bool initPrep, int sets, const QString& url)
        : name(name), defAutomatic(defAutomatic), defPrepare(defPrepare), initPrep(initPrep),
        sets(sets), exTotal(0), setTotal(0), woTotal(0), difficulty(0), url(url), description(""),
        ec(""), focus(Focus::unknown), type(Type::unknown), tools(Tools::unknown) {}
    Data(const QString& name, const QString& url, const int& id) : name(name), url(url), id(id){}

    QString name;
    bool defAutomatic;
    bool defPrepare;
    bool initPrep;
    int sets;

    int exTotal;
    int setTotal;
    int woTotal;
    int difficulty;

    QString url;
    QString description;
    QString ec;

    int id;

    enum class Focus {
        unknown,
        abs,
        back,
        cardio,
        fullbody,
        lowerbody,
        upperbody,
        wellbeing,
    };

    enum class Type {
        unknown,
        abs,
        burn,
        cardio,
        combat,
        general,
        hiit,
        strength,
        stretching,
        warmup,
        yoga,
    };

    enum class Tools {
        unknown,
        none,
        dumbbels,
        pullUpBar,
        other,
    };

    Focus focus;
    Type type;
    Tools tools;

    QString map;
    QString pdf;

    TimerClass timers;

    Data& operator=(Data&& other) noexcept {
        if (this != &other) {
            name = std::move(other.name);
            defAutomatic = std::move(other.defAutomatic);
            defPrepare = std::move(other.defPrepare);
            initPrep = std::move(other.initPrep);
            sets = std::move(other.sets);
            exTotal = std::move(other.exTotal);
            setTotal = std::move(other.setTotal);
            woTotal = std::move(other.woTotal);
            timers = std::move(other.timers);
            url = std::move(other.url);
            description = std::move(other.description);
            ec = std::move(other.ec);
            difficulty = std::move(other.difficulty);
            focus = std::move(other.focus);
            type = std::move(other.type);
            tools = std::move(other.tools);
            map = std::move(other.map);
            pdf = std::move(other.pdf);
            id = std::move(other.id);
        }
        return * this;
    }

    Data(const Data& other) {
        name = other.name;
        defAutomatic = other.defAutomatic;
        defPrepare = other.defPrepare;
        initPrep = other.initPrep;
        sets = other.sets;
        exTotal = other.exTotal;
        setTotal = other.setTotal;
        woTotal = other.woTotal;
        timers = other.timers;
        url = other.url;
        description = other.description;
        ec = other.ec;
        difficulty = other.difficulty;
        focus = other.focus;
        type = other.type;
        tools = other.tools;
        map = other.map;
        pdf = other.pdf;
        id = other.id;
    }

    Data& operator=(const Data& other) noexcept {
        if (this != &other) {
            name = other.name;
            defAutomatic = other.defAutomatic;
            defPrepare = other.defPrepare;
            initPrep = other.initPrep;
            sets = other.sets;
            exTotal = other.exTotal;
            setTotal = other.setTotal;
            woTotal = other.woTotal;
            timers = other.timers;
            url = other.url;
            description = other.description;
            ec = other.ec;
            difficulty = other.difficulty;
            focus = other.focus;
            type = other.type;
            tools = other.tools;
            map = other.map;
            pdf = other.pdf;
            id = other.id;
        }
        return *this;
    }

    void update() {
        exTotal = timers.m_timerData.count();
        setTotal = 0;
        for (int i = 0; i < exTotal; i++) {
            setTotal += timers.m_timerData[i].countdownValue;
        }
        woTotal = setTotal * sets;
    }
};

//Project

class Project : public QAbstractListModel {
    Q_OBJECT
    Q_PROPERTY(bool stopDownload READ getStopDownload WRITE setStopDownload NOTIFY stopDownloadChanged)
    Q_PROPERTY(int woIndex READ getWoIndex WRITE setWoIndex NOTIFY woIndexChanged)
    Q_PROPERTY(int woLastIndex READ getWoLastIndex WRITE setWoLastIndex NOTIFY woLastIndexChanged)
    friend class ProxyModel;

public:
    Project(QObject *parent = nullptr) : QAbstractListModel(parent),
        m_manager(new QNetworkAccessManager(this)), m_stopDownload(false), m_woLastIndex(2420),
        m_woIndex(m_woLastIndex) {
        connect(m_manager, &QNetworkAccessManager::finished, this, &Project::downloadFinished);
    }

    QHash<int, QByteArray>roleNames() const override;

    enum Roles {
        NameRole = Qt::UserRole,
        AutomaticRole,
        PrepareRole,
        InitRole,
        SetsRole,
        TimersRole,
        ExTotalRole,
        SetTotalRole,
        WoTotalRole,
        UrlRole,
        DescriptionRole,
        EcRole,
        DifficultyRole,
        FocusRole,
        TypeRole,
        ToolsRole,
        MapRole,
        PdfRole,
    };

    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

public:
    bool getStopDownload() const;
    int getWoIndex() const;
    int getWoLastIndex() const;
    void setStopDownload(const bool b);
    void setWoIndex(const int i);
    void setWoLastIndex(const int i);

public slots:
    void add(const Data& data);
    void copyData(int index);
    void addNew(const QString name, bool defAutomatic, bool defPrepare, bool initPrep, int sets, const QString &url);
    void addTimer(int index, const QString name, bool automatic, bool prepare, int countdownValue);
    void copyTimer(int index, int timerIndex);
    bool move(int sourceFirst, int dest);
    bool moveTimer(int index, int sourceFirst, int dest);
    void del(int first, int last);
    void delTimer(int index, int first, int last);
    void updateData(int index);
    void readConf();
    void readDarebeeWoConf();
    void clearData();
    void saveConf();
    void saveDarebeeWoConf();
    void loadConf();
    void downloadConf();
    void downloadDarebeeWo();
    void findDarebeeWoTools(int index);
    void addDarebeeWo(const QString& name, const QString& url, const int& id);
    void checkDarebeeWo();
    void downloadDarebeeWoData();
    void downloadDarebeeWoDataCheck();
    void parseDarebeeWoJson(const QString& woJson, bool upd = true);
    void parseDarebeeWoJson(const QString& woJson, int index);
    void downloadData(int index, bool isDarebee = false, bool check = false);
    void processData(QString& htmlContent, int index, bool isDarebee = false, bool check = false);
    void importConf(bool overwrite = false, const QString& confPath = ":/projects.json");
    void exportConf(const QString& filePath);
    bool checkName(int index, const QString& name);
    bool checkNewName(const QString& name);
    int checkDarebeeWoId(const int& id);
    bool getDefAutomatic(int index);
    bool getDefPrepare(int index);
    QString getUrl(int index);
    void setData(int index, const QString& name, bool defAutomatic, bool defPrepare, bool initPrep, int sets, const QString& url);
    void setData(int index, const QString& url, const QString& descr, const QString& ec, int difficulty, Data::Focus fc, Data::Type tp, Data::Tools tools);
    void setPdf(int index, const QString& pdf);
    void setMap(int index, const QString& map);
    QVariant getData(int index);
    QVariant getOtherData(int index);
    int getCount();
    void downloadFinished(QNetworkReply *reply);
    void customAction(const QString& cmd = "sxmo_vibrate", const QStringList& cmdArgs = {"2000", "65535"});

signals:
    void stopDownloadChanged();
    void woIndexChanged();
    void woLastIndexChanged();
    void darebeeWoDownloadDone();
    void darebeeWoDownloadError();
    void downloadSuccess(bool success);

private:
    QVector<Data> m_data;
    QNetworkAccessManager* m_manager;
    QQueue<int> m_downloadPageQueue;
    int m_activeDownloads;
    bool m_stopDownload;
    int m_woLastIndex;
    int m_woIndex;
};

// ProxyModel

class ProxyModel : public QSortFilterProxyModel {
    Q_OBJECT
    Q_PROPERTY(bool ss READ getSs WRITE setSs NOTIFY ssChanged)
    Q_PROPERTY(QString nameFilter READ getNameFilter WRITE setNameFilter NOTIFY nameFilterChanged)
    Q_PROPERTY(QVector<bool> diffFilter READ getDiffFilter WRITE setDiffFilter NOTIFY diffFilterChanged)
    Q_PROPERTY(Data::Focus focusFilter READ getFocusFilter WRITE setFocusFilter NOTIFY focusFilterChanged)
    Q_PROPERTY(Data::Type typeFilter READ getTypeFilter WRITE setTypeFilter NOTIFY typeFilterChanged)
    Q_PROPERTY(Data::Tools toolsFilter READ getToolsFilter WRITE setToolsFilter NOTIFY toolsFilterChanged)

public:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    bool getSs() const;
    QString getNameFilter() const;
    QVector<bool> getDiffFilter() const;
    Data::Focus getFocusFilter() const;
    Data::Type getTypeFilter() const;
    Data::Tools getToolsFilter() const;
    void setSs(const bool b);
    void setNameFilter(const QString& s);
    void setDiffFilter(const QVector<bool> v);
    void setFocusFilter(const Data::Focus f);
    void setTypeFilter(const Data::Type t);
    void setToolsFilter(const Data::Tools t);

public slots:
    void setModel(QAbstractItemModel* sourceModel);
    int findIndex(int idx);

signals:
    void ssChanged();
    void nameFilterChanged();
    void diffFilterChanged();
    void focusFilterChanged();
    void typeFilterChanged();
    void toolsFilterChanged();

private:
    bool m_ss = true;
    QString m_nameFilter = "";
    QVector<bool> m_diffFilter = {false, false, false, false, false};
    Data::Focus m_focusFilter = Data::Focus::unknown;
    Data::Type m_typeFilter = Data::Type::unknown;
    Data::Tools m_toolsFilter = Data::Tools::unknown;
};

#endif // PROJECT_H
