import QtQuick

Item {
    id: timerListViewfooter
    width: parent.width
    height: timerWo.visible ? (timerSet.visible ? timerSet.height * 2 : timerSet.height)
                            : (timerSet.visible ? timerSet.height : 0)
    property alias timerSet: timerSet
    property alias timerWo: timerWo

    SetTimer {
        id: timerSet
        visible: timerListView.count > 1? true : false
    }

    TotalTimer {
        id: timerWo
        anchors.top: timerSet.visible? timerSet.bottom : parent.top
        visible: timerSet.visible? (multiset ? true : false) : true
    }

    property bool blockSet: false
    property bool blockWo: false

    Connections {
        target: currentTimer
        function onValueUpdated(diff) {
            if (!blockSet) {
                if (!timerViewLeaf.setEnded) {
                    timerSet.recalculate(diff)
                }
                else {
                    timerViewLeaf.setEnded = false
                }
            }
            else {
                blockSet = false
            }
            if (!blockWo) {
                timerWo.recalculate(diff)
            }
            else {
                blockWo = false
            }
        }
    }

    Connections {
        target: currentTimer
        function onTimerCompleted() {

        }
    }

    Connections {
        target: timerViewLeaf
        function onSetCompleted() {
            timerSet.resetTimer()
            blockSet = true
        }
        function onWoCompleted() {
            timerSet.endTimer()
            timerWo.endTimer()
            blockWo = true
        }
    }
}
