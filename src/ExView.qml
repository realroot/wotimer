import QtQuick
import QtQuick.Controls.Fusion

Page {
    id: exViewLeaf
    width: parent.width
    height: parent.height
    property alias currentEx: view.currentItem
    property alias model: view.model
    property int proIndex: 0
    property bool hasDarebeeUrl: false
    property bool isExView: true
    property string url: ""
    onUrlChanged: {
        if (url === "") {
            hasDarebeeUrl = false
        }
        else {
            hasDarebeeUrl = url.startsWith("https://darebee.com/workouts/")
        }
    }

    //Using palette.text won't work for Exercise
    property color baseColor: palette.base
    property color textColor: palette.text
    property color placeHolderTextColor: palette.placeholderText
    property color midColor: palette.mid
    property color lightColor: palette.light

    Rectangle {
        id: background
        anchors.fill: parent
        color: palette.window
    }

    ListView {
        id: view
        anchors.fill: parent
        anchors.margins: 15
        spacing: 10
        model: ListModel {}
        //highlightMoveDuration: 150
        //highlightMoveVelocity: -1

        delegate: Component {
            id: delegateComp
            Exercise {
                id: exercise
            }
        }
        //header: ExViewListViewHeader { id: viewHeader }
    }

    header: ExViewHeader { id: header }

    footer: ExViewFooter { id: footer }

    property bool shortcutEnabled: stack.currentItem === exViewLeaf ? true : false

    function add() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        var addTimer = Qt.createComponent("AddTimer.qml")
        if (addTimer.status === Component.Ready) {
            addTimer.createObject(stack)
            mainEsc.enabled = false
            mainConfirm.enabled = false
            view.interactive = false
        }
        else {
            console.error("ERROR: Cannot create AddTimer component: " + addTimer.errorString())
        }
    }

    function editWo() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        var editWo = Qt.createComponent("EditWo.qml")
        if (editWo.status === Component.Ready) {
            editWo.createObject(stack)
            mainEsc.enabled = false
            mainConfirm.enabled = false
            view.interactive = false
        }
        else {
            console.error("ERROR: Cannot create EditWo component: " + editWo.errorString())
        }
    }
/*
    function start() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        view.currentIndex = index
        if (!view.count) {
            errDialog.open()
            return
        }
        var timerView = Qt.createComponent("TimerView.qml")
        if (timerView.status === Component.Ready) {
            stack.push(timerView.createObject(stack, {"model": model}))
        }
    }
*/
    function incIndex() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        if (view.currentIndex === view.count -1) {
            view.currentIndex = -1
        }
        view.incrementCurrentIndex()
    }

    function decIndex() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        if (view.currentIndex === 0) {
            view.currentIndex = view.count
        }
        view.decrementCurrentIndex()
    }

    function download() {
        leaf.downloading = true
        project.downloadData(proIndex)
    }

    function handleQuit() {
        stack.currentItem.destroy()
        stack.pop()
        stack.currentItem.header.closeSearch()
    }

    function moveUp() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        var currentIndex = view.currentIndex
        if (currentIndex > 0) {
            project.moveTimer(proIndex, currentIndex, currentIndex - 1)
            project.saveConf()
            view.currentIndex -= 1
        }
        else {
            moveEnd()
        }
    }

    function moveStart() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        var currentIndex = view.currentIndex
        if (currentIndex > 0) {
            project.moveTimer(proIndex, currentIndex, 0)
            project.saveConf()
            view.currentIndex = 0
            view.positionViewAtBeginning()
        }
    }

    function moveDown() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        var currentIndex = view.currentIndex
        if (currentIndex < view.count - 1) {
            project.moveTimer(proIndex, currentIndex, currentIndex + 2)
            project.saveConf()
            view.currentIndex += 1
        }
        else {
            moveStart()
        }
    }

    function moveEnd() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        var currentIndex = view.currentIndex
        if (currentIndex < view.count - 1) {
            project.moveTimer(proIndex, currentIndex, view.count)
            project.saveConf()
            view.currentIndex = view.count - 1
            view.positionViewAtEnd()
        }
    }

    Shortcut {
        sequence: "ctrl+a"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            add()
        }
    }

    Shortcut {
        sequence: "ctrl+up"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            moveUp()
        }
    }

    Shortcut {
        sequence: "ctrl+shift+up"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            moveStart()
        }
    }

    Shortcut {
        sequence: "ctrl+down"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            moveDown()
        }
    }

    Shortcut {
        sequence: "ctrl+shift+down"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            moveEnd()
        }
    }

    Shortcut {
        sequence: "ctrl+c"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            currentEx.copy()
        }
    }

    Shortcut {
        sequence: "ctrl+d"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            currentEx.delEx()
        }
    }

    Shortcut {
        sequence: "ctrl+e"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            editWo()
        }
    }

    Shortcut {
        sequence: "left"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            handleQuit()
        }
    }

    Shortcut {
        sequence: "right"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            currentEx.edit()
        }
    }

    Shortcut {
        sequence: "up"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            decIndex()
        }
    }

    Shortcut {
        sequence: "down"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            incIndex()
        }
    }
/*
    Shortcut {
        sequence: "ctrl+p"
        onActivated: {
            toggleSettings()
        }
    }*/
    Component.onCompleted: {
        var s = project.getUrl(proIndex)
        url = s
    }
}
