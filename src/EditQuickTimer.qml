import QtQuick
import QtQuick.Controls.Fusion
import QtQuick.Layouts

import "my_functions.js" as Functions

Page {
    id: viewEditleaf
    anchors.centerIn: parent
    width: 264
    height: 360

    background: Rectangle {
        anchors.fill: parent
        color: palette.window
        border.color: palette.accent
        border.width: 2
        radius: 10
    }

    property int sets: quickTimerSets
    property int countdownValue: quickTimerValue
    property string name: quickTimerName
    property bool initPrep: quickTimerInitPrep
    property bool automatic: quickTimerAutomatic
    property bool prepare: quickTimerPrepare

    property int hours: Functions.formatTime(countdownValue, "h")
    property int minutes: Functions.formatTime(countdownValue, "m")
    property int seconds: Functions.formatTime(countdownValue, "s")

    Flickable {
    id: flickable
        anchors.fill: parent
        contentHeight: col.height
        topMargin: (height - col.height) / 2
        leftMargin: 11
        clip: true
        flickableDirection: Flickable.VerticalFlick

        onContentYChanged: {
        //console.log("Y position: " + contentY)
        //console.log("leaf height: " + leaf.height)
            if (leaf.height < viewEditleaf.height) {
                /*if (contentY < -65 || contentY > 15) {
                    return
                }*/
                contentY = contentY
            }
        }

        ColumnLayout {
            id: col
            spacing: 0
            //height is 310

            Rectangle {
                id: warning
                color: "red"
                border.color: "coral"
                border.width: 2
                Layout.fillWidth: true
                Layout.preferredHeight: 30
                visible: false

                Text {
                    id: fieldText
                    color: "yellow"
                    text: "Sets or value cannot be 0"
                    height: parent.height - 2
                    font.bold: true
                    anchors.left: parent.left
                    anchors.margins: 8
                    width: parent.width - 10
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    elide: Text.ElideRight
                }

                SequentialAnimation {
                    id: ani
                    running: false
                    ColorAnimation {
                        target: warning
                        property: "color"
                        to: "red"
                        duration: 400
                    }
                    ColorAnimation {
                        target: warning
                        property: "color"
                        to: "transparent"
                        duration: 400
                    }
                    ColorAnimation {
                        target: warning
                        property: "color"
                        to: "red"
                        duration: 400
                    }
                    ColorAnimation {
                        target: warning
                        property: "color"
                        to: "transparent"
                        duration: 400
                        }
                    onStarted: {
                        warning.visible = true
                    }
                    onStopped: {
                        warning.visible = false
                    }
                }
            }

            Field {
                text: "Sets"
                tip: qsTr("Number of times to run all timers")
                valueText: sets
                function updateValue(text) {
                    sets = Number(text)
                }
            }

            Field {
                text: "Timer name"
                tip: qsTr("Name here can be empty")
                valueText: name
                    validator: null
                    function updateValue(text) {
                        name = text
                    }
            }

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: 13
            }

            Field {
                text: "Hours"
                tip: qsTr("Number of hours of the timer")
                valueText: Functions.formatTime(countdownValue, "h")
                function updateValue(text) {
                    hours = text
                }
            }

            Field {
                text: "Minutes"
                tip: qsTr("Number of minutes of the timer")
                valueText: Functions.formatTime(countdownValue, "m")
                function updateValue(text) {
                    minutes = text
                }
            }

            Field {
                text: "Seconds"
                tip: qsTr("Number of seconds of the timer")
                valueText: Functions.formatTime(countdownValue, "s")
                function updateValue(text) {
                    seconds = text
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: 13
            }

            SwitchField {
                text: "Initial countdown"
                tip: qsTr("When the quick timer starts show a countdown (once)")
                checked: initPrep
                onCheckedChanged: {
                    initPrep = checked
                }
            }

            SwitchField {
                text: "Autostart"
                tip: qsTr("Start the timer automatically when the previous one ends")
                checked: automatic
                onCheckedChanged: {
                    automatic = checked
                }
            }

            SwitchField {
                text: "Alert"
                tip: qsTr("5 seconds before that the timer ends play a voice saying \"Get ready!\"")
                checked: prepare
                onCheckedChanged: {
                    prepare = checked
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: 8
            }

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: 36

                MyButton {
                    id: confirmButton
                    text: "\uF00C"
                    tipEnabled: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenterOffset: -32
                    onClicked: {
                        confirm()
                    }
                }

                MyButton {
                    id: discardButton
                    text: "\uF00D"
                    tipEnabled: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenterOffset: 32
                    onClicked: {
                        quitEditView()
                    }
                }
            }
        }
    }

    function confirm() {
        var val = Functions.convertToSeconds(hours, minutes, seconds)
        if (val > 0 && sets > 0) {
            woViewLeaf.customTimer(val, sets, name, initPrep, automatic, prepare)
            quickTimerInitPrep = initPrep
            quickTimerAutomatic = automatic
            quickTimerPrepare = prepare
            quickTimerName = name
            quickTimerValue = val
            quickTimerSets = sets
            quitEditView()
        }
        else {
            ani.start()
        }
    }

    function quitEditView() {
        viewEditleaf.destroy()
        mainEsc.enabled = true
        mainConfirm.enabled = true
        view.interactive = true
    }

    Shortcut {
        sequences: ["esc", "ctrl+q"]
        onActivated: {
            quitEditView()
        }
    }

    Shortcut {
        sequences: ["return", "space", "ctrl+space"]
        onActivated: {
            confirm()
        }
    }
}
