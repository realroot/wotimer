import QtQuick
import QtQuick.Controls

Button {
    id: button
    text: qsTr("")
    width: 20
    height: 20
    font: fontLoader.font
    hoverEnabled: true
    onHoveredChanged: tipTimer.check()
    display: AbstractButton.TextUnderIcon
    icon.color: useSystemTheme ? palette.buttonText : palette.window
    //icon.width: width
    //icon.height: height

    property alias tip: toolTip.text
    property bool tipVisible: false
    property bool tipEnabled: true
    //property int pixelSide: 4

    Timer {
        id: tipTimer
        interval: 1000
        repeat: false
        running: button.hovered
        onTriggered: {
            tipVisible = true
        }

        function check() {
            if (tipVisible) {
                tipVisible = false
            }
        }
    }

    MyToolTip { id: toolTip }

    background: Rectangle {
        color: hovered ? useSystemTheme ? palette.dark : "dodgerblue" : palette.accent
        border.color: useSystemTheme ? palette.dark : palette.window
        radius: height / 4
    }

    function destroyTip() {
        tipTimer.destroy()
        toolTip.destroy()
    }

    Component.onCompleted: {
        if (!tipEnabled) {
            destroyTip()
        }
    }
}
