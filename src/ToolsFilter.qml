import QtQuick
import QtQuick.Controls

Column {
    width: parent.width
    spacing: 6

    Row {
        anchors.left: parent.left
        anchors.margins: 6
        spacing: 6

        SmallButton {
            text: "\uF00D"
            tip: qsTr("Remove this filter")
            height: 20
            width: 20
            onClicked: {
                resetTools()
            }
        }

        Text {
            text: qsTr("Tools")
            font.bold: true
            color: palette.text
        }
    }

    ToolsComboBox {
        id: toolsBox
    }

    function resetTools() {
        toolsBox.currentIndex = 0
        tools = 0
    }
}
