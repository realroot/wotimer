import QtQuick

Palette {
    id: myPalette
    property color darkergreen: "#07250F"
    property color darkgreen: "#0B3516"
    property color darkergray: "#1E1D1D"
    property color darkgray: "#494B53"

    accent: "lime"
    alternateBase: darkergreen
    base: "black"
    button: "black"
    buttonText: "lime"
    dark: "black"
    highlight: "teal"
    highlightedText: "gold"//"#FFD400"
    light: "gray"
    mid: darkgray
    midlight: "darkgray"
    shadow: "black"
    placeholderText: "#80ffffff"
    text: "deepskyblue"
    window: "black"
    windowText: "lime"

    disabled {
        accent: "#A0A0A0"
        //alternateBase: darkergreen
        //base: "#A0A0A0"
        //button: "#A0A0A0"
        buttonText: "#A0A0A0"
        //dark: "#A0A0A0"
        //highlight: "#A0A0A0"
        //highlightedText: "#A0A0A0"
        //light: "#A0A0A0"
        //mid: "#A0A0A0"
        //midlight: "#A0A0A0"
        //shadow: "#A0A0A0"
        placeholderText: "#A0A0A0"
        text: "#A0A0A0"
        //window: "#A0A0A0"
        windowText: "#A0A0A0"
    }

    inactive {
        accent: "#A0A0A0"
        //alternateBase: darkergreen
        //base: "#A0A0A0"
        //button: "#A0A0A0"
        buttonText: "#A0A0A0"
        //dark: "#A0A0A0"
        //highlight: "#A0A0A0"
        //highlightedText: "#A0A0A0"
        //light: "#A0A0A0"
        //mid: "#A0A0A0"
        //midlight: "#A0A0A0"
        //shadow: "#A0A0A0"
        placeholderText: "#A0A0A0"
        text: "#A0A0A0"
        //window: "#A0A0A0"
        windowText: "#A0A0A0"
    }
}

/*
KvDarkRed system theme colors
accent: #308cc6
alternateBaseColor: #383838
baseColor: #2e2e2e
buttonColor: #3d3d3e
buttonTextColor: #ffffff
colorGroupEnum: 0
darkColor: #1e1e1e
highlightColor: #8c1011
highlightedTextColor: #ffffff
lightColor: #646464
midColor: #3c3c3c
midlightColor: #555555
placeholderTextColor: #80ffffff
shadowColor: #000000
textColor: #ffffff
windowColor: #3d3d3e
windowTextColor: #ffffff

SystemPalette {
    id: systemPalette

    Component.onCompleted: {
        console.log("--------------------")
        console.log("accent: " + systemPalette.accent)
        console.log("alternateBaseColor: " + systemPalette.alternateBase)
        console.log("baseColor: " + systemPalette.base)
        console.log("buttonColor: " + systemPalette.button)
        console.log("buttonTextColor: " + systemPalette.buttonText)
        console.log("colorGroupEnum: " + systemPalette.colorGroup)
        console.log("darkColor: " + systemPalette.dark)
        console.log("highlightColor: " + systemPalette.highlight)
        console.log("highlightedTextColor: " + systemPalette.highlightedText)
        console.log("lightColor: " + systemPalette.light)
        console.log("midColor: " + systemPalette.mid)
        console.log("midlightColor: " + systemPalette.midlight)
        console.log("placeholderTextColor: " + systemPalette.placeholderText)
        console.log("shadowColor: " + systemPalette.shadow)
        console.log("textColor: " + systemPalette.text)
        console.log("windowColor: " + systemPalette.window)
        console.log("windowTextColor: " + systemPalette.windowText)
        console.log("--------------------")
        console.log("accent: " + systemPalette.inactiveaccent)
        console.log("alternateBaseColor: " + systemPalette.inactivealternateBase)
        console.log("baseColor: " + systemPalette.inactivebase)
        console.log("buttonColor: " + systemPalette.inactivebutton)
        console.log("buttonTextColor: " + systemPalette.inactivebuttonText)
        console.log("colorGroupEnum: " + systemPalette.inactivecolorGroup)
        console.log("darkColor: " + systemPalette.inactivedark)
        console.log("highlightColor: " + systemPalette.inactivehighlight)
        console.log("highlightedTextColor: " + systemPalette.inactivehighlightedText)
        console.log("lightColor: " + systemPalette.inactivelight)
        console.log("midColor: " + systemPalette.inactivemid)
        console.log("midlightColor: " + systemPalette.inactivemidlight)
        console.log("placeholderTextColor: " + systemPalette.inactiveplaceholderText)
        console.log("shadowColor: " + systemPalette.inactiveshadow)
        console.log("textColor: " + systemPalette.inactivetext)
        console.log("windowColor: " + systemPalette.inactivewindow)
        console.log("windowTextColor: " + systemPalette.inactivewindowText)
        console.log("--------------------")
        console.log("accent: " + systemPalette.disabledaccent)
        console.log("alternateBaseColor: " + systemPalette.disabledalternateBase)
        console.log("baseColor: " + systemPalette.disabledbase)
        console.log("buttonColor: " + systemPalette.disabledbutton)
        console.log("buttonTextColor: " + systemPalette.disabledbuttonText)
        console.log("colorGroupEnum: " + systemPalette.disabledcolorGroup)
        console.log("darkColor: " + systemPalette.disableddark)
        console.log("highlightColor: " + systemPalette.disabledhighlight)
        console.log("highlightedTextColor: " + systemPalette.disabledhighlightedText)
        console.log("lightColor: " + systemPalette.disabledlight)
        console.log("midColor: " + systemPalette.disabledmid)
        console.log("midlightColor: " + systemPalette.disabledmidlight)
        console.log("placeholderTextColor: " + systemPalette.disabledplaceholderText)
        console.log("shadowColor: " + systemPalette.disabledshadow)
        console.log("textColor: " + systemPalette.disabledtext)
        console.log("windowColor: " + systemPalette.disabledwindow)
        console.log("windowTextColor: " + systemPalette.disabledwindowText)
    }
}
*/
