import QtQuick
import QtQuick.Layouts

ColumnLayout {
    id: woFields
    spacing: 0
    //anchors.centerIn: parent
    property alias ani: warning.ani
    property bool setError: false

    Rectangle {
        id: warning
        color: "red"
        border.color: "coral"
        border.width: 2
        Layout.fillWidth: true
        Layout.preferredHeight: 30
        visible: false
        property alias ani: ani

        Text {
            id: errorText
            color: "yellow"
            text: "Name must be unique and not empty"
            height: parent.height - 2
            visible: setError ? false : true
            font.bold: true
            anchors.left: parent.left
            anchors.margins: 8
            width: parent.width - 10
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            elide: Text.ElideRight
        }

        Text {
            id: setsText
            color: "yellow"
            text: "At least one set is needed"
            height: parent.height
            visible: setError ? true : false
            font.bold: true
            anchors.left: parent.left
            anchors.margins: 8
            width: parent.width - 10
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            elide: Text.ElideRight
        }

        SequentialAnimation {
            id: ani
            running: false
            ColorAnimation {
                target: warning
                property: "color"
                to: "red"
                duration: 400
            }
            ColorAnimation {
                target: warning
                property: "color"
                to: "transparent"
                duration: 400
            }
            ColorAnimation {
                target: warning
                property: "color"
                to: "red"
                duration: 400
            }
            ColorAnimation {
                target: warning
                property: "color"
                to: "transparent"
                duration: 400
            }
            onStarted: {
                warning.visible = true
            }
            onStopped: {
                warning.visible = false
                setError = false
            }
        }
    }

    Field {
        text: qsTr("Url (optional)")
        valuePlaceholder: "https://darebee.com/workouts/workout.html"
        tip: qsTr("If it's a Darebee© url it can be used to download data")
        valueText: url
        validator: null
        function updateValue(text) {
            url = text
        }
    }

    Field {
        text: "Project name"
        valuePlaceholder: "New"
        tip: qsTr("Name must be unique and not empty")
        valueText: name
        validator: null
        function updateValue(text) {
            name = text
        }
    }

    Field {
        text: "Sets"
        tip: qsTr("Number of times to run all timers")
        //valuePlaceholder: "1"
        valueText: sets

        validator: IntValidator { bottom: 1; top: 100000 }
        function updateValue(text) {
            if ( text === "") {
                sets = 0
            }
            else {
                sets = text
            }
        }
    }

    Item {
        Layout.fillWidth: true
        Layout.preferredHeight: 13
    }

    SwitchField {
        text: "Initial countdown"
        tip: qsTr("When the project starts show a countdown (once)")
        checked: initPrep
        onCheckedChanged: {
            initPrep = checked
        }
    }

    SwitchField {
        text: "Default autostart"
        tip: qsTr("Default value of \"Autostart\" for new timers")
        checked: defAutomatic
        onCheckedChanged: {
            defAutomatic = checked
        }
    }

    SwitchField {
        text: "Default alert"
        tip: qsTr("Default value of \"Alert\" for new timers")
        checked: defPrepare
        onCheckedChanged: {
            defPrepare = checked
        }
    }

    Item {
        Layout.fillWidth: true
        Layout.preferredHeight: 8
    }

    Item {
        id: buttonItem
        Layout.fillWidth: true
        Layout.preferredHeight: 36

        MyButton {
            id: confirmButton
            text: "\uF00C"
            tipEnabled: false
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenterOffset: -32
            onClicked: {
                confirm()
            }
        }

        MyButton {
            id: discardButton
            text: "\uF00D"
            tipEnabled: false
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenterOffset: 32
            onClicked: {
                quitEditView()
            }
        }
    }

    Shortcut {
        sequences: ["esc", "ctrl+q"]
        onActivated: {
            quitEditView()
        }
    }

    Shortcut {
        sequences: ["return", "space", "ctrl+space", "ctrl+return"]
        onActivated: {
            confirm()
        }
    }
}
