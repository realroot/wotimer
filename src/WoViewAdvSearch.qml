import QtQuick
import QtQuick.Controls

Row {
    id: filterLeaf
    width: parent.width
    height: 200

    property bool d0: false
    property bool d1: false
    property bool d2: false
    property bool d3: false
    property bool d4: false
    property var difficulty: [false, false, false, false, false]
    property int fc: 0
    property int tp: 0
    property int tools: 0

    function diffChanged(i) {
        switch (i) {
        case 0:
            d0 = !d0
            break
        case 1:
            d1 = !d1
            break
        case 2:
            d2 = !d2
            break
        case 3:
            d3 = !d3
            break
        case 4:
            d4 = !d4
            break
        default:
            console.error( "ERROR: Invalid difficulty value passed")
            break
        }

        difficulty[i] = !difficulty[i]
        proxyModel.diffFilter = difficulty
        proxyModel.invalidate()
    }

    function removeDiff() {
        difficulty = [false, false, false, false, false]
        d0 = false
        d1 = false
        d2 = false
        d3 = false
        d4 = false
        proxyModel.diffFilter = difficulty
        proxyModel.invalidate()
    }

    function nameChanged(text) {
        proxyModel.nameFilter = text
        proxyModel.invalidate()
    }

    onFcChanged: {
        proxyModel.focusFilter = fc
        proxyModel.invalidate()
    }

    onTpChanged: {
        proxyModel.typeFilter = tp
        proxyModel.invalidate()
    }

    onToolsChanged: {
        proxyModel.toolsFilter = tools
        proxyModel.invalidate()
    }

    function resetAll() {
        difficulty = [false, false, false, false, false]
        proxyModel.diffFilter = difficulty
        d0 = false
        d1 = false
        d2 = false
        d3 = false
        d4 = false
        focusFilter.resetFocus()
        typeFilter.resetType()
        toolsFilter.resetTools()
        fc = 0
        tp = 0
        tools = 0
        proxyModel.invalidate()
    }

    DifficultyFilter {
        width: 155
    }

    FocusFilter {
        id: focusFilter
        width: 135
    }

    TypeFilter {
        id: typeFilter
        width: 135
    }

    ToolsFilter {
        id: toolsFilter
        width: 135
    }
}
