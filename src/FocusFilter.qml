import QtQuick
import QtQuick.Controls

Column {
    width: parent.width
    spacing: 6

    Row {
        anchors.left: parent.left
        anchors.margins: 6
        spacing: 6

        SmallButton {
            text: "\uF00D"
            tip: qsTr("Remove this filter")
            height: 20
            width: 20
            onClicked: {
                resetFocus()
            }
        }

        Text {
            text: qsTr("Focus")
            font.bold: true
            color: palette.text
        }
    }

    FocusComboBox {
        id: focusBox
    }

    function resetFocus() {
        focusBox.currentIndex = 0
        fc = 0
    }
}
