import QtQuick
import QtQuick.Controls

Row {
    id: switchField
    width: parent.width
    height: 30
    property alias text: switchName.text
    property alias tip: switchName.tip
    property alias checked: switchValue.checked

    Rectangle {
        id: switchName
        width: 161
        height: parent.height
        color: "transparent"
        border.color: palette.accent
        property alias text: textName.text
        property alias tip: tipButton.tip

        Text {
            id: textName
            color: palette.text
            text: ""
            //font.pixelSize: 12
            height: parent.height - 2
            //font.bold: true
            anchors.left: parent.left
            anchors.margins: 8
            // - 10 without tipButton
            width: parent.width - 25
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            elide: Text.ElideRight
        }

        ToolTipButton {
            id: tipButton
        }
    }

    Rectangle {
        id: switchValue
        width: 81
        height: parent.height
        color: "transparent"
        border.color: palette.light
        property alias checked: control.checked

        MySwitch { id: control }
    }
}
