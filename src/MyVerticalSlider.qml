import QtQuick
import QtQuick.Controls

Slider {
    id: slider
    //anchors.left: parent.left
    //anchors.margins: 6
    anchors.verticalCenter: parent.verticalCenter
    anchors.verticalCenterOffset: pressed ? 60 : -0
    orientation: Qt.Vertical
    from: 1
    to: 0
    value: vol
    onValueChanged: vol = value
    rotation: 180
    height: pressed ? 160 : 38

    background: Rectangle {
        x: slider.leftPadding + slider.availableWidth / 2 - width / 2
        y: slider.topPadding
        implicitWidth: 12
        implicitHeight: parent.width
        width: implicitWidth
        height: slider.availableHeight
        radius: 6
        color: "#bdbebf"
        border.color: "black"

        Rectangle {
            width: parent.width
            //height: parent.height - (slider.visualPosition * parent.height)
            height: slider.visualPosition * parent.height
            color: useSystemTheme ? palette.highlight : "dodgerblue"
            border.color: "black"
            radius: 6
        }
    }

    handle: Rectangle {
        x: slider.leftPadding + slider.availableWidth / 2 - width / 2
        y: slider.topPadding + slider.visualPosition * (slider.availableHeight - height)
        implicitWidth: 20
        implicitHeight: 20
        radius: 10
        color: vol === 0 ? "red" : "#f6f6f6"
        //opacity: slider.pressed ? 0.9 : 0.6
        opacity: 0.6
        border.color: "black"
    }
}
