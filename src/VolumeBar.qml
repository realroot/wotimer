import QtQuick
import QtQuick.Controls

Item {
    property int space: 620
    width: volBar.visible ? volBar.width : expVol.width

    Row {
        id: volBar
        //anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 0
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: 25
        visible: parent.parent.width < space ? true : false
/*
        Text {
            text: (vol * 100).toFixed(0) + "%"
            //font: fontLoader.font
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 9
            color: vol === 0 ? "red" : "lime"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            style: Text.Outline
            styleColor: "black"
        }
*/
        SmallButton {
            id: volButton
            text: (vol * 100).toFixed(0) + "%"
            width: 34
            height: 10
            tipEnabled: false
            font: fontLoader.font
            anchors.verticalCenter: parent.verticalCenter
            contentItem: Text {
                text: parent.text
                font.family: parent.font.family
                font.pixelSize: 8
                color: vol === 0 ? "red" : useSystemTheme ? palette.highlight : "lime"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
                style: Text.Outline
                styleColor: useSystemTheme ? palette.accent : "black"
            }
            background: Rectangle {
                color: "transparent"
                //border.color: "lime"
                //radius: height / 4
            }
        }

        MyVerticalSlider {}
    }

    Column {
        id: expVol
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 6
        anchors.verticalCenter: parent.verticalCenter
        spacing: 6
        visible: volBar.visible ? false : true

        SmallButton {
            id: expVolButton
            text: (vol * 100).toFixed(0) + "%"
            width: 44
            height: 10
            tipEnabled: false
            font: fontLoader.font
            anchors.horizontalCenter: parent.horizontalCenter
            contentItem: Text {
                text: parent.text
                font.family: parent.font.family
                font.pixelSize: 12
                color: vol === 0 ? "red" : useSystemTheme ? palette.highlight : "lime"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
                style: Text.Outline
                styleColor: useSystemTheme ? palette.accent : "black"
            }
            background: Rectangle {
                color: "transparent"
                //border.color: "lime"
                //radius: height / 4
            }
        }

        Slider {
            id: slider
            //anchors.left: parent.left
            //anchors.margins: 6
            //anchors.verticalCenter: parent.verticalCenter
            //anchors.verticalCenterOffset: pressed ? -80 : 0
            orientation: Qt.Horizontal
            from: 0
            to: 1
            value: vol
            onValueChanged: vol = value
            //rotation: 180
            //height: pressed ? 200 : 38

            background: Rectangle {
                x: slider.leftPadding
                y: slider.topPadding + slider.availableHeight / 2 - height / 2
                implicitWidth: 160
                implicitHeight: 12
                width: implicitWidth
                height: implicitHeight
                radius: 6
                color: "#bdbebf"
                border.color: "black"

                Rectangle {
                    width: slider.visualPosition * parent.width
                    height: parent.height
                    color: useSystemTheme ? palette.highlight : "dodgerblue"
                    border.color: "black"
                    radius: 6
                }
            }

            handle: Rectangle {
                x: slider.leftPadding + slider.visualPosition * (slider.availableWidth - width)
                y: slider.topPadding + slider.availableHeight / 2 - height / 2
                implicitWidth: 20
                implicitHeight: 20
                radius: 10
                color: vol === 0 ? "red" : "#f6f6f6"
                //opacity: slider.pressed ? 0.9 : 0.6
                opacity: 0.6
                border.color: "black"
            }
        }
    }
}
