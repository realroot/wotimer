import QtQuick

Column {
    width: parent.width
    spacing: 6

    Row {
        anchors.left: parent.left
        anchors.margins: 6
        spacing: 6

        SmallButton {
            text: "\uF00D"
            tip: qsTr("Remove this filter")
            height: 20
            width: 20
            onClicked: {
                removeDiff()
            }
        }

        Text {
            text: qsTr("Difficulty")
            font.bold: true
            color: palette.text
        }
    }

    Row {
        anchors.left: parent.left
        anchors.margins: 6
        spacing: 10

        Rectangle {
            width: 20
            height: 20
            border.width: 1
            border.color: "black"
            color: d0 ? palette.accent : palette.disabled.accent
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    diffChanged(0)
                }
            }
        }

        Rectangle {
            width: 20
            height: 20
            border.width: 1
            border.color: "black"
            color: d1 ? palette.accent : palette.disabled.accent
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    diffChanged(1)
                }
            }
        }

        Rectangle {
            width: 20
            height: 20
            border.width: 1
            border.color: "black"
            color: d2 ? palette.accent : palette.disabled.accent
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    diffChanged(2)
                }
            }
        }

        Rectangle {
            width: 20
            height: 20
            border.width: 1
            border.color: "black"
            color: d3 ? palette.accent : palette.disabled.accent
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    diffChanged(3)
                }
            }
        }

        Rectangle {
            width: 20
            height: 20
            border.width: 1
            border.color: "black"
            color: d4 ? palette.accent : palette.disabled.accent
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    diffChanged(4)
                }
            }
        }
    }
}
