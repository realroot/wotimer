import QtQuick

MyComboBox {
    id: toolsBox
    width: 120
    anchors.left: parent.left
    anchors.margins: 6
    //editable: true
    model: [
        { text: qsTr("All") },
        { text: qsTr("None") },
        { text: qsTr("Dumbbells") },
        { text: qsTr("Pull-up bar") },
        { text: qsTr("Other") },
    ]
    onActivated: {
        tools = currentIndex
    }
}
