import QtQuick

MyComboBox {
    id: typeBox
    width: 120
    anchors.left: parent.left
    anchors.margins: 6
    //editable: true
    model: [
        { text: qsTr("All") },
        { text: qsTr("Abs") },
        { text: qsTr("Burn") },
        { text: qsTr("Cardio") },
        { text: qsTr("Combat") },
        { text: qsTr("General") },
        { text: qsTr("HIIT") },
        { text: qsTr("Strength") },
        { text: qsTr("Stretching") },
        { text: qsTr("Warmup") },
        { text: qsTr("Yoga") },
    ]
    onActivated: {
        tp = currentIndex
    }
}
