import QtQuick
import QtQuick.Controls

MyHeaderFooter {
    id: woExpHeader

    MyButton {
        id: dwButton
        text: "\uF019"
        anchors.left: parent.left
        anchors.margins: 15
        anchors.verticalCenter: parent.verticalCenter
        enabled: hasDarebeeUrl && !leaf.downloading
        visible: isWoViewSearchExp ? false : true
        tip: qsTr("DownLoad data from the Darebee© url")
        onClicked: {
            download()
        }
    }

    BusyIndicator {
        id: busyInd
        running: leaf.downloading
        //running: true
        anchors.left: dwButton.right
        anchors.verticalCenter: parent.verticalCenter
    }

    Text {
        id: nameText
        text: qsTr("Project \"") + name + "\""
        anchors.left: isWoViewSearchExp ? parent.left : dwButton.right
        anchors.leftMargin: leaf.downloading ? 42 : 16
        anchors.margins: 16
        anchors.top: parent.top
        anchors.topMargin: 14
        //verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
        font.pixelSize: 14
        height: parent.height - 2
        font.bold: true
        width: leaf.downloading ? parent.width - 168 : parent.width - 146
        color: useSystemTheme ? palette.windowText : "royalblue"
        style: Text.Outline
        styleColor: useSystemTheme ? palette.accent : "black"
    }

    MyButton {
        id: quitButton
        //text: "\uF00D"
        text: "\uF053"
        tip: qsTr("Go back to the initial page")
        pixelSide: 10
        anchors.right: parent.right
        anchors.margins: 15
        anchors.verticalCenter: parent.verticalCenter
        width: 42
        onClicked: {
            popStack()
        }
    }
}
