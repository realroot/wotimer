import QtQuick
import QtQuick.Controls

ProgressBar {
    id: bar
    width: 120
    height: 12
    padding: 2
    from: 0
    to: 1

    background: Rectangle {
        id: barBackground
        implicitWidth: 200
        implicitHeight: 6
        color: "#494B53"
        radius: height / 2
    }

    contentItem: Item {
        implicitWidth: 200
        implicitHeight: 4
        Rectangle {
            id: completedBar
            width: bar.visualPosition * parent.width
            height: parent.height
            radius: height / 2
            color: "lime"
        }
    }
}
