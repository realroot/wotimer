import QtQuick
import QtQuick.Controls.Fusion

ToolTip {
    id: toolTip
    visible: tipVisible && parent.hovered

    contentItem: Text {
        text: tip
        color: palette.windowText
        font: parent.font
    }

    background: Rectangle {
        border.color: palette.accent
        color: palette.base
        radius: height / 16
    }

    enter: Transition {
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
    }
    exit: Transition {
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
    }
}
