import QtQuick

Rectangle {
    id: myHeaderFooter
    height: 50
    gradient: Gradient {
        GradientStop { position: 0.0; color: useSystemTheme ? palette.dark : myPalette.darkergreen }
        GradientStop { position: 1.0; color: useSystemTheme ? palette.light : myPalette.darkgreen }
    }
}
