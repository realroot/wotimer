import QtQuick
import "my_functions.js" as Functions

Item {
    id: exLeaf
    width: parent.width
    height: 30

    Rectangle {
        color: currentEx == exLeaf ? useSystemTheme ? palette.highlight : palette.highlightedText :
                                     useSystemTheme ? palette.midlight : palette.mid
        border.color: currentEx == exLeaf ? (useSystemTheme ? palette.dark : "salmon") : palette.light
        border.width: 2
        width: parent.width
        height: 30
        Text {
            id: exText
            font.pixelSize: parent.height - 8
            anchors.left: parent.left
            //Moltiply 28 * number of buttons
            //anchors.leftMargin: 28 * 5 + 5
            anchors.leftMargin: buttonRow.width + 5
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 10
            color: useSystemTheme ? palette.text : palette.window
            elide: Text.ElideRight
            width: parent.width - (anchors.leftMargin + valueButton.width + 7)
            text: model.name
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if (view.interactive) {
                view.currentIndex = index
            }
        }
        onDoubleClicked: {
            edit()
        }
        onPressAndHold: {
            edit()
        }
    }

    function copy() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        project.copyTimer(proIndex, index)
        view.currentIndex = view.count - 1
    }

    function edit() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        view.currentIndex = index
        var editEx = Qt.createComponent("EditEx.qml")
        if (editEx.status === Component.Ready) {
            editEx.createObject(stack)
            //editEx.createObject(stack, {"name": model.name, "automatic": model.automatic, "prepare": model.prepare,
                                      //"countdownValue": model.countdownValue})
            mainEsc.enabled = false
            mainConfirm.enabled = false
            view.interactive = false
        }
        else {
            console.error("ERROR: Cannot create EditEx component: " + editEx.errorString())
        }
    }

    function delEx() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        project.delTimer(proIndex, index, index)
        project.saveConf()
    }

    Row {
        id: buttonRow
        anchors.left: parent.left
        anchors.margins: 2
        anchors.verticalCenter: parent.verticalCenter
        spacing: 2
        /*
        SmallButton {
            id: soundButton
            anchors.top: parent.top
            anchors.margins: 2
            anchors.left: parent.left
            text: "\u{F071F}"
            contentItem: Text {
                text: parent.text
                font.family: parent.font.family
                font.pixelSize: parent.height - 6
                color: "black"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }
            onClicked: {
                if (!shortcutEnabled || !view.interactive) {
                    return
                }
                player.play()
            }
        }
*/

        SmallButton {
            id: editButton
            text: "\uF013"
            tip: qsTr("Edit the selected timer")
            onClicked: {
                edit()
            }
        }

        SmallButton {
            text: "\uF0C5"
            tip: qsTr("Make a copy of the selected timer")
            onClicked: {
                copy()
            }
        }

        SmallButton {
            id: upButton
            text: "\u2191"
            tip: qsTr("Move up the selected timer, press and hold to move to the beginning")
            onClicked: {
                clickedItem()
                moveUp()
            }
            onPressAndHold: {
                clickedItem()
                moveStart()
            }
        }

        SmallButton {
            id: downButton
            text: "\u2193"
            tip: qsTr("Move down the selected timer, press and hold to move to the end")
            onClicked: {
                clickedItem()
                moveDown()
            }
            onPressAndHold: {
                clickedItem()
                moveEnd()
            }
        }

        SmallButton {
            id: deleteButton
            text: "\uF00D"
            tip: qsTr("Delete the selected timer")
            onClicked: {
                delEx()
            }
        }
    }

    SmallButton {
        id: valueButton
        anchors.top: parent.top
        anchors.margins: 2
        anchors.right: parent.right
        text: Functions.formatTime(countdownValue, "a")
        tip: qsTr("Countdown value of the selected timer")
        width: text.width
        pixelSide: 8
    }
}
