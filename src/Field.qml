import QtQuick
import QtQuick.Controls.Fusion

Row {
    id: field
    width: parent.width
    height: 30
    property alias text: fieldTextBackground.text
    property alias tip: fieldTextBackground.tip
    //property alias hover: fieldTextBackground.hover
    property alias valueText: fieldValueBackground.valueText
    property alias valuePlaceholder: fieldValueBackground.valuePlaceholder
    property alias validator: fieldValueBackground.validator
    property alias textWidth: fieldTextBackground.width
    //property alias valueWidth: fieldValueBackground.width
    //Using palette.text won't work for fieldValue
    property color textColor: palette.text
    property color placeHolderTextColor: palette.placeholderText
    property color midColor: palette.mid
    property color lightColor: palette.light

    function updateValue(text) {}

    Rectangle {
        id: fieldTextBackground
        width: 136
        height: parent.height
        color: "transparent"
        border.color: palette.accent
        property alias text: fieldText.text
        property alias tip: tipButton.tip
        //property alias hover: tipButton.hoverEnabled

        Text {
            id: fieldText
            color: palette.text
            text: ""
            //font.pixelSize: 12
            height: parent.height - 2
            //font.bold: true
            anchors.left: parent.left
            anchors.margins: 8
            // - 10 without tipButton
            width: parent.width - 25
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            elide: Text.ElideRight
        }

        ToolTipButton {
            id: tipButton
        }
    }

    Rectangle {
        id: fieldValueBackground
        width: 106
        height: parent.height
        color: "transparent"
        //border.color: "lime"
        property alias valueText: fieldValue.text
        property alias valuePlaceholder: fieldValue.placeholderText
        property alias validator: fieldValue.validator

        TextField {
            id: fieldValue
            text: ""
            placeholderText: ""
            placeholderTextColor: placeHolderTextColor
            validator: IntValidator { bottom: 1; top: 10000 }
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignLeft
            //anchors.margins: 0
            padding: 6
            width: parent.width
            height: parent.height
            implicitWidth: parent.width
            color: textColor
            background: Rectangle {
                color: fieldValue.enabled ? "transparent" : lightColor
                border.color: fieldValue.enabled ? lightColor : midColor
                //height: parent.height - 6
            }
            onTextEdited: {
                updateValue(text)
            }
        }
    }
}
