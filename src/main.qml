import QtQuick
//import QtQuick.Window
import QtMultimedia
import QtQuick.Controls
//import QtQuick.Layouts
//import QtCore
import Qt.labs.settings
import Qt.labs.platform
//import Qt.labs.folderlistmodel
//import QtQuick.LocalStorage

import com.realroot

ApplicationWindow {
    id: leaf
    width: 640
    height: 480
    visible: true
    title: qsTr("Timers")
    property int defInitPrepVal: 10
    property bool useSystemTheme: true
    property bool customAction: false
    property bool quickTimerInitPrep: true
    property bool quickTimerAutomatic: true
    property bool quickTimerPrepare: false
    property string quickTimerName: "quick timer"
    property int quickTimerValue: 45
    property int quickTimerSets: 1
    property int darebeeWoIndex: 0
    property int darebeeWoLastIndex: 2420
    property real vol: 1
    property real zoom: 1
    property bool downloading: false
    property alias stack: stack

    background: Rectangle {
        id: background
        anchors.fill: parent
        color: palette.window
    }

    palette: useSystemTheme ? systemPalette : myPalette

    Palette {
        id: systemPalette
    }

    MyPalette {
        id: myPalette
    }

    FontLoader {
        id: fontLoader
        source: "qrc:/fonts/SymbolsNerdFont-Regular.ttf"
    }

    Settings {
        category: "General"
        property alias x: leaf.x
        property alias y: leaf.y
        property alias width: leaf.width
        property alias height: leaf.height
        //property alias lastLocation: fileDialog.currentFolder for QtCore
        property alias lastSaveLocation: saveFileDialog.folder
        //property alias lastSaveFile: saveFileDialog.file  not saving the file name
        property alias lastOpenLocation: openFileDialog.folder
        property alias lastPdfLocation: openPdfDialog.folder
        property alias lastMapLocation: openMapDialog.folder
        property alias useSystemTheme: leaf.useSystemTheme
        property alias customAction: leaf.customAction
        property alias defInitPrepVal: leaf.defInitPrepVal
        property alias quickTimerInitPrep: leaf.quickTimerInitPrep
        property alias quickTimerAutomatic: leaf.quickTimerAutomatic
        property alias quickTimerPrepare: leaf.quickTimerPrepare
        property alias quickTimerName: leaf.quickTimerName
        property alias quickTimerValue: leaf.quickTimerValue
        property alias quickTimerSets: leaf.quickTimerSets
        property alias darebeeWoIndex: leaf.darebeeWoIndex
        property alias darebeeWoLastIndex: leaf.darebeeWoLastIndex
        property alias vol: leaf.vol
        property alias zoom: leaf.zoom
    }

    /* for the one from QtCore instead of Qt.labs.settings, not used cause bug
        FileDialog {
            id: fileDialog
            currentFolder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
            fileMode: FileDialog.SaveFile
            //PlatformTheme: linux
            onAccepted: {
                var fileName = fileDialog.selectedFile.toString().replace("file://", "")
                myFunctions.save(fileName, output.text)
            }
        }
    */
    FileDialog {
        id: saveFileDialog
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        fileMode: FileDialog.SaveFile
        nameFilters: ["JSON files (*.json)"]
        onAccepted: {
            var fileName = saveFileDialog.currentFile.toString().replace("file://", "")
            project.exportConf(fileName)
        }
    }

    FileDialog {
        id: openFileDialog
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        fileMode: FileDialog.OpenFile
        nameFilters: ["JSON files (*.json)"]
        onAccepted: {
            var fileName = openFileDialog.currentFile.toString().replace("file://", "")
            project.importConf(false, fileName)
        }
    }

    FileDialog {
        id: openPdfDialog
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        fileMode: FileDialog.OpenFile
        nameFilters: ["Image and pdf files (*pdf *.png *.xpm *.jpg *.jpeg *.bmp *.gif *.tiff *.tif *.svg)"]
        onAccepted: {
            var fileName = openPdfDialog.currentFile.toString()
            stack.currentItem.updPdf(fileName)
        }
    }

    FileDialog {
        id: openMapDialog
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
        fileMode: FileDialog.OpenFile
        nameFilters: ["Image and pdf files (*pdf *.png *.xpm *.jpg *.jpeg *.bmp *.gif *.tiff *.tif *.svg)"]
        onAccepted: {
            var fileName = openMapDialog.currentFile.toString()
            stack.currentItem.updMap(fileName)
        }
    }

    MediaPlayer {
        id: player
        source: "qrc:/sounds/alarm_001.mp3"
        //audioOutput: AudioOutput {}
        audioOutput: audioOutput
    }

    MediaPlayer {
        id: playerPrep
        source: "qrc:/sounds/get_ready.mp3"
        audioOutput: prepAudioOutput
    }

    MyPopup {
        id: dwDialog
        width: netErrText.width + 18

        Text {
            id: netErrText
            text: "Download could not be completed\nAre you offline?"
            wrapMode: Text.WordWrap
            anchors.centerIn: parent
            anchors.verticalCenterOffset: -25
            horizontalAlignment: Text.AlignHCenter
            color: "red"
            linkColor: "red"
            onLinkActivated: Qt.openUrlExternally(link)
        }
    }

    Project {
        id: project
        onDownloadSuccess: success => {
            stack.currentItem.showNetErr(success)
        }

        onDarebeeWoDownloadError: {
            leaf.downloading = false
            dwDialog.open()
        }

        onDarebeeWoDownloadDone: {
            leaf.downloading = false
            if (stack.currentItem.isWoExp) {
                stack.currentItem.updPage()
            }
        }

        Component.onCompleted: {
            loadConf()
            readConf()
        }
    }

    Project {
        id: darebeeProject/*
        onDownloadSuccess: success => {
            stack.currentItem.showNetErr(success)
        }*/
        onDarebeeWoDownloadError: {
            leaf.downloading = false
            dwDialog.open()
        }

        onDarebeeWoDownloadDone: {
            leaf.downloading = false
        }

        onWoIndexChanged: {
            leaf.darebeeWoIndex = darebeeProject.woIndex
        }

        onWoLastIndexChanged: {
            leaf.darebeeWoLastIndex = darebeeProject.woLastIndex
        }

        Component.onCompleted: {
            //leaf.darebeeWoIndex = darebeeProject.woIndex
            //leaf.darebeeWoLastIndex = darebeeProject.woLastIndex
            readDarebeeWoConf()
            woIndex = leaf.darebeeWoIndex
        }
    }

    ProxyModel {
        id: proxyModel
        Component.onCompleted: {
            setModel(project)
        }
    }

    ProxyModel {
        id: darebeeProxyModel
        Component.onCompleted: {
            setModel(darebeeProject)
        }
    }

    AudioOutput {
        id: audioOutput
        volume: vol
    }

    AudioOutput {
        id: prepAudioOutput
        volume: vol
    }

    StackView {
        id: stack
        anchors.fill: parent
        initialItem: WoView { id: woView }

        popEnter: Transition {
            XAnimator {
                from: (stack.mirrored ? -1 : 1) * -stack.width
                to: 0
                duration: 400
                easing.type: Easing.OutCubic
            }
        }

        popExit: Transition {
            XAnimator {
                from: 0
                to: (stack.mirrored ? -1 : 1) * stack.width
                duration: 400
                easing.type: Easing.OutCubic
            }
        }
    }

    Shortcut {
        id: mainEsc
        sequences: ["esc", "ctrl+q"]
        onActivated: {
            if (stack.depth === 1) {
                if (stack.currentItem.settingsFooter.visible) {
                    stack.currentItem.toggleSettings()
                }
                else if (stack.currentItem.header.state === "expanded") {
                    stack.currentItem.header.toggleSearch()
                }
                else if (stack.currentItem.header.state === "advSearch") {
                    stack.currentItem.header.toggleAdvSearch()
                }
                else {
                    Qt.quit()
                }
            }
            else {
                if (stack.currentItem.isTimerView) {
                    if (stack.currentItem.prepTextBackground.visible) {
                        stack.currentItem.thaw()
                    }
                    else {
                        stack.currentItem.handleQuit()
                    }
                }
                else if (stack.currentItem.isWoViewSearch) {
                    /*if (stack.currentItem.progressRow.state === "editing") {
                        stack.currentItem.progressRow.state = ""
                    }
                    else {*/
                        popStack()
                    //}
                }
                else {
                    popStack()
                }
            }
        }
    }

    function popStack() {
        mainConfirm.enabled = true
        stack.currentItem.destroy()
        stack.pop()
        stack.currentItem.header.closeSearch()
        stack.currentItem.view.forceActiveFocus()
    }

    Shortcut {
        id: mainConfirm
        sequences: ["enter", "return", "space", "ctrl+space", "ctrl+return", "ctrl+enter"]
        onActivated: {
            if (stack.depth === 1) {
                if (stack.currentItem.settingsFooter.visible) {
                    stack.currentItem.toggleSettings()
                }
                else {
                    stack.currentItem.currentWo.start()
                }
            }
            else {
                if (stack.currentItem.isTimerView) {
                    if (stack.currentItem.prepTextBackground.visible) {
                        stack.currentItem.thaw()
                        return
                    }
                    if (stack.currentItem.state === "init") {
                        return
                    }
                    stack.currentItem.play()
                }
                else if (stack.currentItem.isWoViewSearch) {
                    stack.currentItem.currentWo.expand()
                }
                else if (stack.currentItem.isExView) {
                    stack.currentItem.currentEx.edit()
                }
            }
        }
    }

    MyPopup {
        id: aboutDialog
        width: aboutText.width + 22
        Text {
            id: aboutText
            text: '<html><style type="text/css"></style><a href="https://codeberg.org/realroot/wotimer">QML timer with WorkOuts in mind</a></html>'
            wrapMode: Text.WordWrap
            anchors.centerIn: parent
            anchors.verticalCenterOffset: -35
            horizontalAlignment: Text.AlignHCenter
            color: palette.windowText
            linkColor: palette.accent
            onLinkActivated: Qt.openUrlExternally(link)
        }
        Text {
            id: donateText
            text: '<html><style type="text/css"></style><a href="https://liberapay.com/realroot/donate">Donate on liberapay</a></html>'
            wrapMode: Text.WordWrap
            anchors.centerIn: parent
            anchors.verticalCenterOffset: -15
            horizontalAlignment: Text.AlignHCenter
            color: palette.windowText
            linkColor: palette.accent
            onLinkActivated: Qt.openUrlExternally(link)
        }
    }

    Component.onCompleted: {
        /*if (darebeeWoIndex > darebeeWoLastIndex) {
            darebeeWoIndex = darebeeWoLastIndex
        }*/
        //darebeeWoIndex = darebeeProject.getCount()
    }
}
