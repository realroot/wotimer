import QtQuick
import QtQuick.Controls

ComboBox {
    id: control
    textRole: "text"
    valueRole: "value"
    model: [qsTr("First"), qsTr("Second"), qsTr("Third")]
    height: 36
    width: 50
    font: fontLoader.font
    hoverEnabled: true

    delegate: ItemDelegate {
        id: delegate
        width: control.width
        hoverEnabled: true
        required property var model
        required property int index

        contentItem: Text {
            text: delegate.model[control.textRole]
            color: hovered ? palette.highlightedText : palette.buttonText
            font: control.font
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
        }
        background: Rectangle {
            anchors.fill: parent
            color: hovered ? palette.highlight : palette.button
        }
        highlighted: control.highlightedIndex === index
    }

    indicator: Canvas {
        id: canvas
        x: control.width - width - 4
        y: control.topPadding + (control.availableHeight - height) / 2
        width: 12
        height: 8
        contextType: "2d"

        Connections {
            target: control
            function onHoveredChanged() { canvas.requestPaint(); }
        }

        onPaint: {
            context.reset()
            context.moveTo(0, 0)
            context.lineTo(width, 0)
            context.lineTo(width / 2, height)
            context.closePath()
            context.fillStyle = hovered ? useSystemTheme ? palette.accent : "dodgerblue" : palette.buttonText
            context.fill()
        }
    }

    contentItem: Text {
        leftPadding: 6
        rightPadding: control.indicator.width + control.spacing

        text: control.displayText
        font: control.font
        color: hovered ? useSystemTheme ? palette.accent : "dodgerblue" : palette.buttonText
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        implicitHeight: 36
        implicitWidth: 50
        color: palette.button
        border.color: hovered ? useSystemTheme ? palette.accent : "dodgerblue" : palette.buttonText
        border.width: control.visualFocus ? 2 : 1
        radius: 2
    }

    popup: Popup {
        y: control.height - 1
        width: control.width
        implicitHeight: contentItem.implicitHeight
        padding: 0

        contentItem: ListView {
            clip: true
            implicitHeight: contentHeight
            model: control.popup.visible ? control.delegateModel : null
            currentIndex: control.highlightedIndex
            ScrollIndicator.vertical: ScrollIndicator { }
        }

        background: Rectangle {
            border.color: palette.accent
            radius: 2
        }
    }
}
