import QtQuick
import QtQuick.Controls

Popup {
    id: popup
    modal: true
    width: popupText.width + 18
    height: 130
    anchors.centerIn: parent
    background: Rectangle {
        border.color: palette.windowText
        //color: "#02121A"
        color: palette.window
        radius: height / 16
    }
    enter: Transition {
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
    }
    exit: Transition {
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
    }
    Text {
        id: popupText
        text: '<html><style type="text/css"></style><a href="https://codeberg.org/realroot/<app>"><description></a></html>'
        wrapMode: Text.WordWrap
        anchors.centerIn: parent
        anchors.verticalCenterOffset: -25
        horizontalAlignment: Text.AlignHCenter
        color: palette.windowText
        linkColor: palette.accent
        onLinkActivated: Qt.openUrlExternally(link)
    }
    MyButton {
        width: 42
        tipEnabled: false
        anchors.margins: 10
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        text: "\uF00D"
        onClicked: {
            popup.close()
        }
    }
    Shortcut {
        sequences: ["esc", "ctrl+q"]
        enabled: popup.visible ? true : false
        onActivated: {
            popup.close()
        }
    }
}
