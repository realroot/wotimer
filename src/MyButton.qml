import QtQuick
import QtQuick.Controls

IconButton {
    property int pixelSide: 14

    contentItem: Text {
        text: parent.text
        font.family: parent.font.family
        font.pixelSize: parent.height - pixelSide
        color: hovered ? useSystemTheme ? palette.accent : "dodgerblue" : palette.buttonText
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }
}
