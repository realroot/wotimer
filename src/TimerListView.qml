import QtQuick

ListView {
    id: timerListView
    //anchors.fill: parent
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.top: parent.top
    width: parent.width - 30
    height: (24 * zoom + 30) * count
    anchors.margins: 15
    clip : true
    highlight: highlightComponent
    highlightMoveDuration: 200
    highlightMoveVelocity: -1
    spacing: 0
    model: ListModel {}
    snapMode: ListView.SnapToItem
    delegate: delegate
    //highlightFollowsCurrentItem: false
    //highlightRangeMode: ListView.StrictlyEnforceRange
    highlightRangeMode: ListView.ApplyRange
    //boundsBehavior: Flickable.DragOverBounds
    //preferredHighlightBegin: 0
    //preferredHighlightEnd: 24 * zoom + 30
    cacheBuffer: 50 * count
    //interactive: false

    Component {
        id: highlightComponent
        Rectangle {
            id: highlightRectangle
            width: timerListView.currentItem.width
            height: timerListView.currentItem.height
            //y: timerListView.currentItem.y
            /*Behavior on y {
                SpringAnimation {
                    spring: 3
                    damping: 0.2
                }
            }*/
            opacity: 1
            color: "transparent"
            border.color: currentTimer.countdownTimer.running ? "salmon" : "#FFD400"
            border.width: 5
            radius: height / 4
        }
    }

    Component {
        id: delegate
        MyTimer {
            automatic: model.automatic
            countdownValue: model.countdownValue
            timerName: model.name
            prepare: model.prepare
        }/*
        Component.onCompleted: {
            console.log("TTTTT " + model.sets)
        }*/
    }
}
