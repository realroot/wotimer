import QtQuick
import QtQuick.Controls

SmallIconButton {
    width: 26
    height: 26
    property int pixelSide: 4

    contentItem: Text {
        text: parent.text
        font.family: parent.font.family
        font.pixelSize: parent.height - pixelSide
        color: useSystemTheme ? palette.buttonText : palette.window
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }
}

/*
Button {
    id: button
    text: qsTr("")
    width: 26
    height: 26
    font: fontLoader.font
    hoverEnabled: true
    onHoveredChanged: tipTimer.check()

    property alias tip: toolTip.text
    property bool tipVisible: false
    property bool tipEnabled: true
    property int pixelSide: 4

    Timer {
        id: tipTimer
        interval: 1000
        repeat: false
        running: button.hovered
        onTriggered: {
            tipVisible = true
        }

        function check() {
            if (tipVisible) {
                tipVisible = false
            }
        }
    }

    MyToolTip { id: toolTip }

    contentItem: Text {
        text: parent.text
        font.family: parent.font.family
        font.pixelSize: parent.height - pixelSide
        color: useSystemTheme ? palette.buttonText : palette.window
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        //implicitWidth: parent.width
        color: hovered ? useSystemTheme ? palette.dark : "dodgerblue" : palette.accent
        border.color: useSystemTheme ? palette.dark : palette.window
        radius: height / 4
    }

    function destroyTip() {
        tipTimer.destroy()
        toolTip.destroy()
    }

    Component.onCompleted: {
        if (!tipEnabled) {
            destroyTip()
        }
    }
}
*/
