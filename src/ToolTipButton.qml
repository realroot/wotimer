import QtQuick
import QtQuick.Controls

Button {
    id: button
    text: qsTr("\uF29C")
    width: 20
    height: 20
    anchors.right: parent.right
    anchors.margins: 1
    anchors.verticalCenter: parent.verticalCenter
    font: fontLoader.font
    hoverEnabled: true

    property alias tip: toolTip.text

    MyToolTip {
        id: toolTip
        visible: button.hovered
    }

    contentItem: Text {
        text: parent.text
        font.family: parent.font.family
        font.pixelSize: parent.height - 4
        color: palette.accent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        //implicitWidth: parent.width
        color: "transparent"
        //border.color: "lime"
        //radius: height / 4
    }
}
