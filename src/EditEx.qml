import QtQuick
import QtQuick.Controls.Fusion
import QtQuick.Layouts

import "my_functions.js" as Functions

Page {
    id: viewEditleaf
    anchors.centerIn: parent
    width: 264
    height: 300

    background: Rectangle {
        anchors.fill: parent
        color: palette.window
        border.color: palette.accent
        border.width: 2
        radius: 10
    }

    property int countdownValue: model.countdownValue
    property string name: model.name
    property bool automatic: model.automatic
    property bool prepare: model.prepare

    property int hours: Functions.formatTime(countdownValue, "h")
    property int minutes: Functions.formatTime(countdownValue, "m")
    property int seconds: Functions.formatTime(countdownValue, "s")

    Flickable {
    id: flickable
        anchors.fill: parent
        contentHeight: timerFields.height
        topMargin: (height - timerFields.height) / 2
        leftMargin: 11
        clip: true
        flickableDirection: Flickable.VerticalFlick

        onContentYChanged: {
            if (leaf.height < viewEditleaf.height) {
                contentY = contentY
            }
        }

        TimerFields { id: timerFields }
    }

    function confirm() {
        var val = Functions.convertToSeconds(hours, minutes, seconds)
        name = name.trim()
        if (val > 0 && name !== "") {
            model.countdownValue = val
            model.name = name
            model.automatic = automatic
            model.prepare = prepare
            project.updateData(proIndex)
            project.saveConf()
            quitEditView()
        }
        else {
            timerFields.ani.start()
        }
    }

    function quitEditView() {
        viewEditleaf.destroy()
        mainEsc.enabled = true
        mainConfirm.enabled = true
        view.interactive = true
    }
}
