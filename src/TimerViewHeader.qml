import QtQuick
import QtQuick.Controls

MyHeaderFooter {
    id: timerViewHeader

    //property alias text: woText.text

    /*
    MyButton {
        id: menu
        text: "\u2630"
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.margins: 6
        onClicked: {

        }
    }*/

    MyButton {
        id: setButton
        text: "Set " + currentSet + "/" + sets
        tip: qsTr("Current set / Total sets")
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.left: parent.left
        anchors.margins: 15
        width: text.width
        anchors.verticalCenter: parent.verticalCenter
    }

    MyButton {
        id: addSetButton
        text: "\uF055"
        tip: qsTr("Add one set to the elapsed sets")
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.left: setButton.right
        anchors.margins: 10
        anchors.verticalCenter: parent.verticalCenter
        width: 36
        onClicked: {
            addSet()
        }
    }

    MyButton {
        id: removeSetButton
        text: "\uF056"
        tip: qsTr("Remove one set to the elapsed sets")
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.left: addSetButton.right
        anchors.margins: 4
        anchors.verticalCenter: parent.verticalCenter
        width: 36
        onClicked: {
            removeSet()
        }
    }

    MyButton {
        id: restartButton
        text: "\uF021"
        tip: qsTr("Restart the project completely")
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.left: removeSetButton.right
        anchors.margins: 10
        anchors.verticalCenter: parent.verticalCenter
        width: 36
        onClicked: {
            restartPopup.open()
        }
    }

    VolumeBar {
        id: volBar
        space: 465
        anchors.left: restartButton.right
        anchors.margins: 4
    }

    Text {
        id: woText
        //text: "Set " + currentSet + "/" + model.sets + " \u{F0B2A} " +
              //currentTimer.timerName + " \u{F051B} " + currentTimer.countdown.toFixed(0)
        text: isQuickTimer ? qsTr("Quick Timer") : "\"" + name + "\""
        anchors.left: restartButton.right
        anchors.margins: 180
        //visible: parent.width > 510 ? true : false
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
        font.pixelSize: 14
        height: parent.height - 2
        font.bold: true
        width: parent.width - 470
        color: useSystemTheme ? palette.windowText : "royalblue"
        style: Text.Outline
        styleColor: useSystemTheme ? palette.accent : "black"
        /*
    Text {
        id: fieldText
        color: "royalblue"
        style: Text.Outline
        styleColor: "black"
        text: "\"" + name + "\" | " + view.count + " timer/s | " + sets + " set/s | total time: " + Functions.formatTime(woTotal, "a")
        font.pixelSize: 14
        height: parent.height - 2
        font.bold: true
        width: parent.width - 10
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        elide: Text.ElideRight
    }*/
    }

    MyButton {
        id: quitButton
        //text: "\uF00D"
        text: "\uF053"
        tip: qsTr("Go back to the initial page")
        pixelSide: 10
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.right: parent.right
        anchors.margins: 15
        anchors.verticalCenter: parent.verticalCenter
        width: 42
        onClicked: {
            handleQuit()
        }
    }
}
