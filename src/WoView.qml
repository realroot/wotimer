import QtQuick
import QtQuick.Controls

Page {
    id: woViewLeaf
    width: parent.width
    height: parent.height
    property alias currentWo: view.currentItem
    property alias view: view
    property alias settingsFooter: settingsFooter

    Rectangle {
        id: background
        anchors.fill: parent
        color: palette.window
    }

    ListView {
        id: view
        anchors.fill: parent
        anchors.margins: 15
        spacing: 10
        model: proxyModel
        //highlightMoveDuration: 150
        //highlightMoveVelocity: -1

        delegate: //Component {
            //id: delegateComp
            Workout {
                id: workout
            //}
        }
    }

    header: WoViewHeader { id: header }

    footer: WoViewFooter { id: footer}

    WoViewSettings { id: settingsFooter }

    ListModel {
        id: listModel
        ListElement {}
    }

    property bool shortcutEnabled: stack.currentItem === woViewLeaf ? true : false
    property bool search: header.state === "" ? false : true

    function darebeeWo() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        var woViewSearch = Qt.createComponent("qrc:/WoViewSearch.qml")
        if (woViewSearch.status === Component.Ready) {
            stack.push(woViewSearch.createObject(stack))
        }
        else {
            console.error("ERROR: Cannot create woViewSearch component: " + woViewSearch.errorString())
        }
    }

    function selectTime() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        var editView = Qt.createComponent("EditQuickTimer.qml")
        if (editView.status === Component.Ready) {
            editView.createObject(stack)
            mainEsc.enabled = false
            mainConfirm.enabled = false
            view.interactive = false
        }
        else {
            console.error("ERROR: Cannot create EditQuickTimer component: " + editView.errorString())
        }
    }

    function customTimer(val, setVal, name, initPrep, automatic, prepare) {
        var timerViewC = Qt.createComponent("TimerView.qml")
        if (timerViewC.status === Component.Ready) {         
            listModel.set(0, {"automatic": automatic, "prepare": prepare, "countdownValue": val, "name": name})
            stack.push(timerViewC.createObject(stack, {"model": listModel, "sets": setVal, "setTotal": val,
                                                   "woTotal": setVal * val, "initPrep": initPrep, "isQuickTimer": true}))
        }
        else {
            console.error("ERROR: Cannot create TimerView component: " + timerViewC.errorString())
        }
    }

    function addWo() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        header.closeSearch()
        var addWo = Qt.createComponent("AddWo.qml")
        if (addWo.status === Component.Ready) {
            addWo.createObject(stack)
            mainEsc.enabled = false
            mainConfirm.enabled = false
            view.interactive = false
        }
        else {
            console.error("ERROR: Cannot create AddWo component: " + addWo.errorString())
        }
    }

    function incIndex() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        if (view.currentIndex === view.count -1) {
            view.currentIndex = -1
        }
        view.incrementCurrentIndex()
    }

    function decIndex() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        if (view.currentIndex === 0) {
            view.currentIndex = view.count
        }
        view.decrementCurrentIndex()
    }

    function toggleSettings() {
        if (!shortcutEnabled || !view.interactive) {
            return
        }
        if (woViewLeaf.footer === footer) {
            woViewLeaf.footer = settingsFooter
            settingsFooter.visible = true
        }
        else {
            woViewLeaf.footer = footer
            settingsFooter.visible = false
        }
    }

    function showNetErr(success) {
        dwDialog.show(success)
    }

    function moveUp() {
        if (!shortcutEnabled || !view.interactive || search) {
            //if (!shortcutEnabled || !view.interactive) {
            return
        }
        view.currentIndex = proxyModel.findIndex(view.currentIndex)
        var currentIndex = view.currentIndex
        if (currentIndex > 0) {
            project.move(currentIndex, currentIndex - 1)
            //project.saveConf()
            view.currentIndex -= 1
        }
        else {
            moveEnd()
        }
    }

    function moveStart() {
        if (!shortcutEnabled || !view.interactive || search) {
            //if (!shortcutEnabled || !view.interactive) {
            return
        }
        view.currentIndex = proxyModel.findIndex(view.currentIndex)
        var currentIndex = view.currentIndex
        if (currentIndex > 0) {
            project.move(currentIndex, 0)
            //project.saveConf()
            view.currentIndex = 0
            view.positionViewAtBeginning()
        }
    }

    function moveDown() {
        if (!shortcutEnabled || !view.interactive || search) {
            //if (!shortcutEnabled || !view.interactive) {
            return
        }
        view.currentIndex = proxyModel.findIndex(view.currentIndex)
        var currentIndex = view.currentIndex
        if (currentIndex < view.count - 1) {
            project.move(currentIndex, currentIndex + 2)
            //project.saveConf()
            view.currentIndex += 1
        }
        else {
            moveStart()
        }
    }

    function moveEnd() {
        if (!shortcutEnabled || !view.interactive || search) {
            //if (!shortcutEnabled || !view.interactive) {
            return
        }
        view.currentIndex = proxyModel.findIndex(view.currentIndex)
        var currentIndex = view.currentIndex
        if (currentIndex < view.count - 1) {
            project.move(currentIndex, view.count)
            //project.saveConf()
            view.currentIndex = view.count - 1
            view.positionViewAtEnd()
        }
    }

    MyPopup {
        id: dwDialog
        width: netErrText.width + 18
        Text {
            id: dwText
            text: "Downloading the file..."
            wrapMode: Text.WordWrap
            anchors.centerIn: parent
            anchors.verticalCenterOffset: -25
            horizontalAlignment: Text.AlignHCenter
            color: palette.windowText
            linkColor: palette.accent
            onLinkActivated: Qt.openUrlExternally(link)
            visible: true
        }

        Text {
            id: okText
            text: "Downloaded completed"
            wrapMode: Text.WordWrap
            anchors.centerIn: parent
            anchors.verticalCenterOffset: -25
            horizontalAlignment: Text.AlignHCenter
            color: palette.windowText
            linkColor: palette.accent
            onLinkActivated: Qt.openUrlExternally(link)
            visible: false
        }

        Text {
            id: netErrText
            text: '<html><style type="text/css"></style><a href="https://codeberg.org/realroot/wotimer-darebee-config/raw/branch/main/wotimer.json">\
                File</a></html> could not be downloaded are you offline?'
            wrapMode: Text.WordWrap
            anchors.centerIn: parent
            anchors.verticalCenterOffset: -25
            horizontalAlignment: Text.AlignHCenter
            color: "red"
            linkColor: "red"
            onLinkActivated: Qt.openUrlExternally(link)
            visible: false
        }

        function show(success) {
            dwText.visible = false
            if (success) {
                okText.visible = true
            }
            else {
                netErrText.visible = true
            }
        }

        onClosed: {
            dwText.visible = true
            okText.visible = false
            netErrText.visible = false
        }
    }

    Shortcut {
        sequences: ["alt+w", "ctrl+w"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            selectTime()
        }
    }

    Shortcut {
        sequences: ["alt+a", "ctrl+a"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            addWo()
        }
    }

    Shortcut {
        sequences: ["alt+up", "ctrl+up"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            moveUp()
        }
    }

    Shortcut {
        sequences: ["alt+shift+up", "ctrl+shift+up"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            moveStart()
        }
    }

    Shortcut {
        sequences: ["alt+down", "ctrl+down"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            moveDown()
        }
    }

    Shortcut {
        sequences: ["alt+shift+down", "ctrl+shift+down"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            moveEnd()
        }
    }

    Shortcut {
        sequences: ["alt+c", "ctrl+c"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            currentWo.copy()
        }
    }

    Shortcut {
        sequences: ["alt+d", "ctrl+d"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            currentWo.delWo()
        }
    }

    Shortcut {
        sequences: ["alt+e", "ctrl+e"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            currentWo.editWo()
        }
    }

    Shortcut {
        sequences: ["alt+f", "ctrl+f"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            header.toggleSearch()
        }
    }

    Shortcut {
        sequences: ["alt+b", "ctrl+b"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            currentWo.expand()
        }
    }

    Shortcut {
        sequences: ["alt+g", "ctrl+g"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            darebeeWo()
        }
    }

    Shortcut {
        sequences: ["alt+shift+f", "ctrl+shift+f"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            header.toggleAdvSearch()
        }
    }

    Shortcut {
        sequences: ["alt+left", "ctrl+left", "left"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            currentWo.editWo()
        }
    }

    Shortcut {
        sequences: ["alt+right", "ctrl+right", "right"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            currentWo.edit()
        }
    }

    Shortcut {
        sequence: "up"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            decIndex()
        }
    }

    Shortcut {
        sequence: "down"
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            incIndex()
        }
    }

    Shortcut {
        sequences: ["alt+p", "ctrl+p"]
        enabled: shortcutEnabled && view.interactive ? true : false
        onActivated: {
            toggleSettings()
        }
    }
}
