import QtQuick

MyHeaderFooter {
    id: woViewFooter
    property bool collapsed: width > 460 ? false : true

    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: 6

        MyButton {
            id: startButton
            text: "\uE0B0"
            tip: qsTr("Start the selected project")
            onClicked: {
                currentWo.start()
            }
        }

        MyButton {
            id: addButton
            text: "\u002B"
            tip: qsTr("Add a new project")
            pixelSide: 0
            onClicked: {
                addWo()
            }
        }

        MyButton {
            id: quickTimerButton
            text: "\u{F051B}"
            tip: qsTr("Open the quick timer pop-up")
            pixelSide: 8
            onClicked: {
                selectTime()
            }
        }

        MyButton {
            id: downloadButton
            visible: !collapsed
            text: "\u{F1463}"
            //text: "\uF0AC"
            tip: qsTr("Download DAREBEE© workouts from internet\nthey cannot be included because of the licensing")
            pixelSide: 8
            onClicked: {
                project.downloadConf()
                dwDialog.open()
            }
        }

        MyButton {
            id: importButton
            visible: !collapsed
            text: "\uF019"
            tip: qsTr("Load projects from a file")
            onClicked: {
                openFileDialog.open()
            }
        }

        MyButton {
            id: exportButton
            visible: !collapsed
            text: "\uF093"
            tip: qsTr("Save the projects to a file")
            onClicked: {
                saveFileDialog.open()
            }
        }

        MyButton {
            id: resetButton
            visible: !collapsed
            text: "\u{F0450}"
            //text: "\uF021"
            tip: qsTr("Restore the default projects")
            pixelSide: 8
            onClicked: {
                project.importConf(true)
            }
        }

        MyButton {
            id: clearButton
            visible: !collapsed
            text: "\uF00D"
            //text: "\uF021"
            tip: qsTr("Delete ALL projects")
            pixelSide: 8
            onClicked: {
                project.clearData()
                project.saveConf()
            }
        }

        MyComboButton {
            id: projectsControl
            visible: collapsed
            model: [1, 2, 3, 4, 5]
            property var items: [
                { text: qsTr("\u{F1463}"), tip: qsTr("Download DAREBEE© workouts from internet\nit cannot be included due the licensing"),
                    command: "project.downloadConf()"},
                { text: qsTr("\uF019"), tip: qsTr("Load projects from a file"), command: "openFileDialog.open()" },
                { text: qsTr("\uF093"), tip: qsTr("Save the projects to a file"), command: "saveFileDialog.open()" },
                { text: qsTr("\u{F0450}"), tip: qsTr("Restore the default projects"), command: "project.importConf(true)" },
                { text: qsTr("\uF00D"), tip: qsTr("Delete ALL projects"), command: "project.clearData();project.saveConf()" }
            ]
        }
/*
        Connections {
            target: projectsControl
            function onDoFunction(i) {

            }
        }*/
    }
}
