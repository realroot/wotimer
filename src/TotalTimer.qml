import QtQuick
import QtQuick.Controls
import "my_functions.js" as Functions

Item {
    id: totalTimerLeaf
    width: parent.width
    height: timerBackground.height
    property int countdownValue: woTotal
    property string timerName: "Total (Set " + currentSet + "/" + sets + ")"
    property real countdown: countdownValue
    property real progress: timerBar.to / (countdownValue * 10)
    property alias value: timerBar.value
    property alias from: timerBar.from
    property alias to: timerBar.to
    property alias timerBackground: timerBackground
    property alias countdownText: countdownText

    Rectangle {
        id: timerBackground
        color: "transparent"
        width: parent.width
        height: timerBar.height + 30
        property alias completedAnimation: completedAnimation
        property alias countdownText: countdownText
        property alias timerBar: timerBar
        states: [
            State {
                name: "completed"
            }
        ]
        ProgressBar {
            id: timerBar
            width: parent.width - 24
            height: 24 * zoom
            padding: 2
            anchors.centerIn: parent
            from: 0
            to: 1
            property alias completedAnimation: completedAnimation
            Text {
                id: countdownText
                text: timerBackground.state === "completed" ? "Done" : Functions.formatTime(countdown, "a")
                font.pixelSize: parent.height
                anchors.left: parent.left
                //anchors.verticalCenter: parent.verticalCenter
                //anchors.margins: 6
                anchors.centerIn: parent
                color: timerBackground.state === "completed" ? "forestgreen" : useSystemTheme ? Qt.darker(palette.text, 1.5) : "dodgerblue"
                style: Text.Outline
                styleColor: useSystemTheme ? palette.accent : "black"
            }
            Text {
                id: timerText
                text: timerName
                font.pixelSize: parent.height <= 55 ? parent.height - 2 : 54
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: 10
                color: timerBackground.state === "completed" ? "forestgreen" : useSystemTheme ? Qt.darker(palette.text, 1.5) : "dodgerblue"
                style: Text.Outline
                styleColor: useSystemTheme ? palette.accent : "black"
                elide: Text.ElideRight
                width: parent.width / 2 - countdownText.width / 2 - 8
            }
            background: Rectangle {
            id: timerBarBackground
            implicitWidth: 200
            implicitHeight: 6
            color: "#494B53"
            radius: height / 2
            }
            contentItem: Item {
                implicitWidth: 200
                implicitHeight: 4
                Rectangle {
                    id: completedBar
                    width: timerBar.visualPosition * parent.width
                    height: parent.height
                    radius: height / 2
                    color: "forestgreen"
                }
            }
        }
    }

    property bool checkDone: false

    function endTimer() {
        timerBar.value = timerBar.to
        completedAnimation.start()
        timerBackground.state = "completed"
    }

    function recalculate(diff) {
        countdown += diff
        value -= progress * diff * 10
    }

    function resetTimer() {
        timerBackground.state = ""
        if (completedAnimation.running) {
            completedAnimation.stop()
        }
        value = 0
        countdown = countdownValue
        resetAnimation.start()
    }

    function checkTotalSetEnd() {
        recalculate((timerSet.countdownValue * (sets - currentSet))
                    + timerSet.countdown - countdown)
        blockWo = true
    }

    function checkTotalTimerEnd() {
        recalculate((timerSet.countdownValue * (sets - currentSet)) - countdown)
        blockWo = true
    }

    SequentialAnimation {
        id: completedAnimation
        running: false
        ColorAnimation {
            target: completedBar
            property: "color"
            to: "#4D23A7"
            duration: 400
        }
        ColorAnimation {
            target: completedBar
            property: "color"
            to: "forestgreen"
            duration: 400
        }
        ColorAnimation {
            target: completedBar
            property: "color"
            to: "#4D23A7"
            duration: 400
        }
        ColorAnimation {
            target: completedBar
            property: "color"
            to: "forestgreen"
            duration: 300
        }
        ParallelAnimation {
            ColorAnimation {
                target: timerBarBackground
                property: "color"
                to: "royalblue"
                duration: 0
            }
            ColorAnimation {
                target: completedBar
                property: "color"
                to: "dodgerblue"
                duration: 0
            }
        }
    }

    ParallelAnimation {
        id: resetAnimation
        ColorAnimation {
            target: timerBarBackground
            property: "color"
            to: "#494B53"
            duration: 0
        }
        ColorAnimation {
            target: completedBar
            property: "color"
            to: "forestgreen"
            duration: 0
        }
    }
}
