import QtQuick
import QtQuick.Controls

Page {
    id: woExpLeaf
    width: parent.width
    height: parent.height
    property int proIndex: 0
    property string url: ""
    property bool isWoExp: true
    property bool hasDarebeeUrl: false
    property bool isWoViewSearchExp: false
    property bool isReady: false
    property string name: ""
    property string pdf: ""
    property string map: ""
    property string descr: ""
    property string ec: ""
    property int difficulty: 0
    property int fc: 0
    property int tp: 0
    property int tools: 0

    function updProject() {
        if (isWoViewSearchExp) {
            darebeeProject.setData(proIndex, url, descr, ec, difficulty, fc, tp, tools)
            darebeeProject.saveDarebeeWoConf()
        }
        else {
            project.setData(proIndex, url, descr, ec, difficulty, fc, tp, tools)
            project.saveConf()
        }
    }

    onUrlChanged: {
        if (!isReady) {
            return
        }
        updProject()
        hasDarebeeUrl = url.startsWith("https://darebee.com/workouts/")
    }

    onDescrChanged: {
        if (!isReady) {
            return
        }
        updProject()
    }

    onEcChanged: {
        if (!isReady) {
            return
        }
        updProject()
    }

    onDifficultyChanged: {
        if (!isReady) {
            return
        }
        updProject()
    }

    onFcChanged: {
        if (!isReady) {
            return
        }
        updProject()
    }

    onTpChanged: {
        if (!isReady) {
            return
        }
        updProject()
    }

    onToolsChanged: {
        if (!isReady) {
            return
        }
        updProject()
    }

    header: WoExpHeader { id: header }

    signal updData()

    function download() {
        leaf.downloading = true
        project.downloadData(proIndex)
    }

    function updPage() {
        var data = project.getOtherData(proIndex)
        pdf = data.pdf
        map = data.map
        descr = data.description
        ec = data.ec
        difficulty = data.difficulty
        fc = data.focus
        tp = data.type
        tools = data.tools
        updData()
    }

    function updPdf(pdf) {
        if (isWoViewSearchExp) {
            darebeeProject.setPdf(proIndex, pdf)
            darebeeProject.saveConf()
        }
        else {
            project.setPdf(proIndex, pdf)
            project.saveConf()
        }
        updPage()
    }


    function updMap(map) {
        if (isWoViewSearchExp) {
            darebeeProject.setMap(proIndex, map)
            darebeeProject.saveConf()
        }
        else {
            project.setMap(proIndex, map)
            project.saveConf()
        }
        updPage()
    }

    Flickable {
        anchors.fill: parent
        anchors.margins: 10
        contentWidth: col.width
        contentHeight: col.height

        Column {
            id: col
            spacing: 10
            //padding: 6
            //width: implicitWidth

            Rectangle {
                width: 720
                height: 80
                color: "transparent"

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: 10
                    color: "transparent"
                    border.color: palette.accent
                    border.width: 2

                    Column {
                        anchors.fill: parent
                        spacing: 5
                        Text {
                            text: qsTr("Url")
                            font.bold: true
                            color: palette.text
                            leftPadding : 10
                            topPadding: 10
                        }

                        Flickable {
                            width: parent.width
                            height: parent.height - 40
                            leftMargin: 10
                            TextInput {
                                id: descrInput
                                anchors.fill: parent
                                color: palette.text
                                wrapMode: TextArea.WordWrap
                                //anchors.fill: parent
                                //placeholderTextColor: palette.placeholderText
                                //placeholderText: qsTr("Enter the url")
                                text: url
                                onEditingFinished: {
                                    url = text
                                }
                            }
                        }
                    }
                }
            }

            Flow {
                topPadding: 10
                leftPadding: 10
                spacing: 10

                Image {
                    id: pdfPage
                    width: 228
                    height: 318
                    //width: pdf === "" ? 248 : doc.pageWidth
                    //height: pdf === "" ? 318 : doc.pageHeight
                    //width: 595.22
                    //height: 842
                    source: pdf === "" ? "qrc:/pdfs/empty.pdf" : pdf
                    fillMode: Image.PreserveAspectFit
                    /*
                    onStatusChanged: {
                        console.log('Status changed ' + status)
                    }

                    Component.onCompleted: {
                        console.log('OnCompleted status ' + status)
                        console.log('OnCompleted PDF ' + pdf)
                        console.log('OnCompleted DOC.SOURCE ' + doc.source)
                    }*/

                    SmallIconButton {
                        icon.source: "qrc:/icons/Adwaita/document-edit-symbolic.svg"
                        width: 25
                        height: 25
                        anchors.top: parent.top
                        anchors.right: removePdfButton.left
                        anchors.rightMargin: 6
                        tip: qsTr("Select a pdf/image for this project")
                        onClicked: {
                            openPdfDialog.open()
                        }
                    }

                    SmallButton {
                        id: removePdfButton
                        text: "\uF00D"
                        width: 25
                        height: 25
                        anchors.top: parent.top
                        anchors.right: parent.right
                        tip: qsTr("Remove the pdf/image of this project")
                        onClicked: {
                            updPdf("")
                            pdf = ""
                        }
                    }

                    Component.onCompleted: {
                        if ( status !== 1) {
                            source = "qrc:/pdfs/error.pdf"
                        }
                    }
                }

                Image {
                    id: mapPage
                    width: 228
                    height: 318
                    source: map === "" ? "qrc:/pdfs/empty.pdf" : map
                    fillMode: Image.PreserveAspectFit

                    SmallIconButton {
                        icon.source: "qrc:/icons/Adwaita/document-edit-symbolic.svg"
                        width: 25
                        height: 25
                        anchors.top: parent.top
                        anchors.right: removeMapButton.left
                        anchors.rightMargin: 6
                        tip: qsTr("Select an pdf/image for the muscle map of this project")
                        onClicked: {
                            openMapDialog.open()
                        }
                    }

                    SmallButton {
                        id: removeMapButton
                        text: "\uF00D"
                        width: 25
                        height: 25
                        anchors.top: parent.top
                        anchors.right: parent.right
                        tip: qsTr("Remove the muscle map's pdf/image of this project")
                        onClicked: {
                            updMap("")
                            map = ""
                        }
                    }

                    Component.onCompleted: {
                        if ( status !== 1) {
                            source = "qrc:/pdfs/error.pdf"
                        }
                    }
                }
            }

            Row {
                Rectangle {
                    width: 180
                    height: 100
                    color: "transparent"

                    Rectangle {
                        anchors.fill: parent
                        anchors.margins: 10
                        color: "transparent"
                        border.color: palette.accent
                        border.width: 2

                        Column {
                            anchors.fill: parent
                            anchors.margins: 10
                            spacing: 10

                            Item {
                                height: 20
                                width: parent.width

                                Text {
                                    text: qsTr("Difficulty")
                                    font.bold: true
                                    color: palette.text
                                }

                                SmallButton {
                                    text: "\uF00D"
                                    tip: qsTr("Set to unknown or N/A")
                                    anchors.right: parent.right
                                    height: 20
                                    width: 20
                                    onClicked: {
                                        difficulty = 0
                                    }
                                }
                            }

                            Rectangle {
                                width: 140
                                height: 20
                                color: "transparent"

                                Row {
                                    spacing: 10

                                    Rectangle {
                                        width: 20
                                        height: 20
                                        border.width: 1
                                        border.color: "black"
                                        color: difficulty > 0 ? palette.accent : palette.disabled.accent
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                difficulty = 1
                                            }
                                        }
                                    }

                                    Rectangle {
                                        width: 20
                                        height: 20
                                        border.width: 1
                                        border.color: "black"
                                        color: difficulty > 1 ? palette.accent : palette.disabled.accent
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                difficulty = 2
                                            }
                                        }
                                    }

                                    Rectangle {
                                        width: 20
                                        height: 20
                                        border.width: 1
                                        border.color: "black"
                                        color: difficulty > 2 ? palette.accent : palette.disabled.accent
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                difficulty = 3
                                            }
                                        }
                                    }

                                    Rectangle {
                                        width: 20
                                        height: 20
                                        border.width: 1
                                        border.color: "black"
                                        color: difficulty > 3 ? palette.accent : palette.disabled.accent
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                difficulty = 4
                                            }
                                        }
                                    }

                                    Rectangle {
                                        width: 20
                                        height: 20
                                        border.width: 1
                                        border.color: "black"
                                        color: difficulty > 4 ? palette.accent : palette.disabled.accent
                                        MouseArea {
                                            anchors.fill: parent
                                            onClicked: {
                                                difficulty = 5
                                            }
                                        }
                                    }
                                }
/*
                                Row {
                                    spacing: 10

                                    Repeater {
                                        model: difficulty > 0 ? difficulty : 0
                                        delegate: Rectangle {
                                            width: 20
                                            height: 20
                                            border.width: 1
                                            border.color: "black"
                                            color: palette.accent
                                        }
                                    }
                                }*/
                            }
                            /*
                        Text {
                            text: qsTr("Difficulty")
                        }*/
                        }
                    }
                }

                Rectangle {
                    width: 180
                    height: 100
                    color: "transparent"

                    Rectangle {
                        anchors.fill: parent
                        anchors.margins: 10
                        color: "transparent"
                        border.color: palette.accent
                        border.width: 2

                        Column {
                            anchors.fill: parent
                            anchors.margins: 10
                            spacing: 5

                            Text {
                                //leftPadding: 10
                                text: qsTr("Focus")
                                font.bold: true
                                color: palette.text
                            }

                            FocusComboBox {
                                id: fcComboBox
                                width: parent.width
                                anchors.margins: 0
                                model: [
                                    { text: qsTr("Unknown or N/A") },
                                    { text: qsTr("Abs") },
                                    { text: qsTr("Back") },
                                    { text: qsTr("Cardio") },
                                    { text: qsTr("Fullbody") },
                                    { text: qsTr("Lowerbody") },
                                    { text: qsTr("Upperbody") },
                                    { text: qsTr("Wellbeing") },
                                ]
                                Component.onCompleted: {
                                    currentIndex = fc
                                }

                                Connections {
                                    target: woExpLeaf
                                    function onUpdData() {
                                        fcComboBox.currentIndex = fc
                                    }
                                }
                            }
                        }
                    }
                }

                Rectangle {
                    width: 180
                    height: 100
                    color: "transparent"

                    Rectangle {
                        anchors.fill: parent
                        anchors.margins: 10
                        color: "transparent"
                        border.color: palette.accent
                        border.width: 2

                        Column {
                            anchors.fill: parent
                            anchors.margins: 10
                            spacing: 5

                            Text {
                                //leftPadding: 10
                                text: qsTr("Type")
                                font.bold: true
                                color: palette.text
                            }

                            TypeComboBox {
                                id: tpComboBox
                                width: parent.width
                                anchors.margins: 0
                                model: [
                                    { text: qsTr("Unknown or N/A") },
                                    { text: qsTr("Abs") },
                                    { text: qsTr("Burn") },
                                    { text: qsTr("Cardio") },
                                    { text: qsTr("Combat") },
                                    { text: qsTr("General") },
                                    { text: qsTr("HIIT") },
                                    { text: qsTr("Strength") },
                                    { text: qsTr("Stretching") },
                                    { text: qsTr("Warmup") },
                                    { text: qsTr("Yoga") },
                                ]
                                Component.onCompleted: {
                                    currentIndex = tp
                                }

                                Connections {
                                    target: woExpLeaf
                                    function onUpdData() {
                                        tpComboBox.currentIndex = tp
                                    }
                                }
                            }
                        }
                    }
                }

                Rectangle {
                    width: 180
                    height: 100
                    color: "transparent"

                    Rectangle {
                        anchors.fill: parent
                        anchors.margins: 10
                        color: "transparent"
                        border.color: palette.accent
                        border.width: 2

                        Column {
                            anchors.fill: parent
                            anchors.margins: 10
                            spacing: 5

                            Text {
                                //leftPadding: 10
                                text: qsTr("Tools")
                                font.bold: true
                                color: palette.text
                            }

                            ToolsComboBox {
                                id: toolsComboBox
                                width: parent.width
                                anchors.margins: 0
                                model: [
                                    { text: qsTr("Unknown or N/A") },
                                    { text: qsTr("None") },
                                    { text: qsTr("Dumbbells") },
                                    { text: qsTr("Pull-up bar") },
                                    { text: qsTr("Other") },
                                ]
                                Component.onCompleted: {
                                    currentIndex = tools
                                }

                                Connections {
                                    target: woExpLeaf
                                    function onUpdData() {
                                        toolsComboBox.currentIndex = tools
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Column {
                Rectangle {
                    width: 720
                    height: 345
                    color: "transparent"

                    Rectangle {
                        anchors.fill: parent
                        anchors.margins: 10
                        color: "transparent"
                        border.color: palette.accent
                        border.width: 2

                        Column {
                            //width: woExpLeaf.width > 600 ? 600 : parent.width
                            anchors.fill: parent
                            spacing: 5

                            /*
            Flickable {
                width: woExpLeaf.width > 600 ? 600 : parent.width
                height: 500
                //contentItem: edit
*/
                            /*
                Rectangle {
                    width: parent.width
                    height: 2
                    color: palette.accent
                }
*/
                            Text {
                                text: qsTr("Description")
                                color: palette.text
                                font.bold: true
                                leftPadding : 10
                                topPadding: 10
                            }

                            Flickable {
                                width: parent.width
                                height: parent.height - 42
                                TextArea {
                                    anchors.fill: parent
                                    color: palette.text
                                    wrapMode: TextArea.WordWrap
                                    //anchors.fill: parent
                                    placeholderText: qsTr("Enter the description")
                                    text: descr
                                    onEditingFinished: {
                                        descr = text
                                    }
                                }
                            }
                        }
                    }
                }

                Rectangle {
                    width: 720
                    height: 80
                    color: "transparent"

                    Rectangle {
                        anchors.fill: parent
                        anchors.margins: 10
                        color: "transparent"
                        border.color: palette.accent
                        border.width: 2

                        Column {
                            //width: woExpLeaf.width > 600 ? 600 : parent.width
                            anchors.fill: parent
                            spacing: 5
                            Text {
                                text: qsTr("Extra Credit")
                                font.bold: true
                                color: palette.text
                                leftPadding : 10
                                topPadding: 10
                            }

                            Flickable {
                                width: parent.width
                                height: parent.height - 40
                                leftMargin: 10
                                TextInput {
                                    id: ecInput
                                    anchors.fill: parent
                                    color: palette.text
                                    wrapMode: TextArea.WordWrap
                                    //anchors.fill: parent
                                    //placeholderText: qsTr("Enter the extra credit")
                                    text: ec
                                    onEditingFinished: {
                                        ec = text
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        isReady = true
        hasDarebeeUrl = url.startsWith("https://darebee.com/workouts/")
    }
}
