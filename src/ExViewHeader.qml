import QtQuick

import "my_functions.js" as Functions

MyHeaderFooter {
    id: exHeader

    property int sets
    property int woTotal
    property string name
    property bool initPrep
    property bool defAutomatic
    property bool defPrepare

    function getText() {
        var data = project.getData(proIndex)
        sets = data.sets
        woTotal = data.woTotal
        name = data.name
        initPrep = data.initPrep
        defAutomatic = data.defAutomatic
        defPrepare = data.defPrepare
    }

    Connections {
        target: project
        function onDataChanged() {
            getText()
        }
    }

    Text {
        id: fieldText
        color: useSystemTheme ? palette.windowText : "royalblue"
        style: Text.Outline
        styleColor: useSystemTheme ? palette.accent : "black"
        text: "\"" + name + "\" | " + view.count + " timer/s | " + sets + " set/s | total time: " + Functions.formatTime(woTotal, "a")
        font.pixelSize: 14
        height: parent.height - 2
        font.bold: true
        width: parent.width - 66
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.left: parent.left
        anchors.margins: 10
        elide: Text.ElideRight
    }

    MyButton {
        id: quitButton
        text: "\uF053"
        tip: qsTr("Go back to the initial page")
        //anchors.horizontalCenter: parent.horizontalCenter
        pixelSide: 10
        anchors.right: parent.right
        anchors.margins: 10
        anchors.verticalCenter: parent.verticalCenter
        width: 42
        onClicked: {
            handleQuit()
        }
    }

    Component.onCompleted: {
        getText()
    }
}
