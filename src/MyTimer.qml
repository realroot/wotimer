import QtQuick
import QtQuick.Controls
import "my_functions.js" as Functions

Item {
    id: timerLeaf
    width: parent.width
    height: timerBackground.height
    property bool automatic: true
    property int countdownValue: 20
    property string timerName: ""
    /*
    enum timerType {
        rest,
        restNp,
        exercise,
        prepare
    }
    property timerType type: timerType.exercise*/
    property bool prepare: false

    property real countdown: countdownValue
    property real progress: timerBar.to / (countdownValue * 10)
    property alias value: timerBar.value
    property alias from: timerBar.from
    property alias to: timerBar.to
    property alias countdownTimer: countdownTimer
    property alias timerBackground: timerBackground
    property alias countdownText: countdownText

    Timer {
        id: countdownTimer
        interval: 100
        repeat: true
        running: false
        onTriggered: {
            if (countdown > 0.1) {
                countdown -= 0.1
                timerBar.value += progress
                updateSets(-0.1)
                if (prepare && countdown < 5.1 && countdown >= 5 ) {
                    playerPrep.play()
                }
            }
            else {
                endTimer()
            }
        }
    }

    Rectangle {
        id: timerBackground
        color: "transparent"
        //border.color: "#FFD400"
        //border.width: 1
        width: parent.width
        height: timerBar.height + 30
        //radius: height / 2
        property alias completedAnimation: completedAnimation
        property alias countdownText: countdownText
        property alias timerBar: timerBar      
        states: [
            State {
                name: "completed"
            }
        ]
        ProgressBar {
            id: timerBar
            width: parent.width - 24
            height: 24 * zoom
            padding: 2
            anchors.centerIn: parent
            from: 0
            to: 1
            property alias completedAnimation: completedAnimation
            Text {
                id: countdownText
                text: timerBackground.state === "completed" ? "Done" : Functions.formatTime(countdown, "a")
                font.pixelSize: parent.height
                anchors.left: parent.left
                //anchors.verticalCenter: parent.verticalCenter
                //anchors.margins: 6
                anchors.centerIn: parent
                color: timerBackground.state === "completed" ? "limegreen" : palette.text
                style: Text.Outline
                styleColor: useSystemTheme ? palette.accent : "black"
            }
            Text {
                id: timerText
                text: timerName
                font.pixelSize: parent.height <= 55 ? parent.height - 2 : 54
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: 10
                color: timerBackground.state === "completed" ? "limegreen" : palette.text
                style: Text.Outline
                styleColor: useSystemTheme ? palette.accent : "black"
                elide: Text.ElideRight
                width: parent.width / 2 - countdownText.width / 2 - 8
                /*
                text: " - " + timerName
                font.pixelSize: parent.height <= 55 ? parent.height - 2 : 54
                anchors.left: countdownText.right
                anchors.verticalCenter: parent.verticalCenter
                width: parent.width - countdownText.width - 8
                */
            }
            background: Rectangle {
            id: timerBarBackground
            implicitWidth: 200
            implicitHeight: 6
            color: "#494B53"
            radius: height / 2
            }
            contentItem: Item {
                implicitWidth: 200
                implicitHeight: 4
                Rectangle {
                    id: completedBar
                    width: timerBar.visualPosition * parent.width
                    height: parent.height
                    radius: height / 2
                    color: "lime"
                    //border.color: "blue"
                    //border.width: 1
                }
            }
        }
    }

    signal timerCompleted()
    signal valueUpdated(real diff)

    function endTimer() {
        player.play()
        countdownTimer.stop()
        timerBar.value = timerBar.to
        completedAnimation.start()
        timerBackground.state = "completed"
        timerCompleted()
    }

    function updateSets(diff) {
        valueUpdated(diff)
    }

    function recalculate(diff) {
        countdown += diff
        value -= progress * diff * 10
    }

    function resetTimer() {
        timerBackground.state = ""
        if (completedAnimation.running) {
            completedAnimation.stop()
        }
        value = 0
        countdown = countdownValue
        resetAnimation.start()
    }

    SequentialAnimation {
        id: completedAnimation
        running: false
        ColorAnimation {
            target: completedBar
            property: "color"
            to: "#4D23A7"
            duration: 400
        }
        ColorAnimation {
            target: completedBar
            property: "color"
            to: "lime"
            duration: 400
        }
        ColorAnimation {
            target: completedBar
            property: "color"
            to: "#4D23A7"
            duration: 400
        }
        ColorAnimation {
            target: completedBar
            property: "color"
            to: "lime"
            duration: 300
        }
        ParallelAnimation {
            ColorAnimation {
                target: timerBarBackground
                property: "color"
                to: "dodgerblue"
                duration: 0
            }
            ColorAnimation {
                target: completedBar
                property: "color"
                to: "royalblue"
                duration: 0
            }
        }
    }

    ParallelAnimation {
        id: resetAnimation
        ColorAnimation {
            target: timerBarBackground
            property: "color"
            to: "#494B53"
            duration: 0
        }
        ColorAnimation {
            target: completedBar
            property: "color"
            to: "lime"
            duration: 0
        }
    }
}
