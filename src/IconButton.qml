import QtQuick
import QtQuick.Controls

Button {
    id: button
    text: qsTr("")
    height: 36
    width: 50
    font: fontLoader.font
    hoverEnabled: true
    onHoveredChanged: tipTimer.check()
    display: AbstractButton.TextUnderIcon
    //icon.color: useSystemTheme ? palette.buttonText : palette.window
    //icon.width: width
    //icon.height: height

    property alias tip: toolTip.text
    property bool tipVisible: false
    property bool tipEnabled: true
    //property int pixelSide: 4

    Timer {
        id: tipTimer
        interval: 1000
        repeat: false
        running: button.hovered
        onTriggered: {
            tipVisible = true
        }

        function check() {
            if (tipVisible) {
                tipVisible = false
            }
        }
    }

    MyToolTip { id: toolTip }

    background: Rectangle {
        //implicitWidth: parent.width
        //color: "black"
        color: palette.button
        border.color: hovered ? useSystemTheme ? palette.accent : "dodgerblue" : palette.buttonText
        border.width: 1
        radius: height / 3
    }

    function destroyTip() {
        tipTimer.destroy()
        toolTip.destroy()
    }

    Component.onCompleted: {
        if (!tipEnabled) {
            destroyTip()
        }
    }
}
