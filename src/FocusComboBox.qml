import QtQuick

MyComboBox {
    id: focusBox
    width: 120
    anchors.left: parent.left
    anchors.margins: 6
    //editable: true
    model: [
        { text: qsTr("All") },
        { text: qsTr("Abs") },
        { text: qsTr("Back") },
        { text: qsTr("Cardio") },
        { text: qsTr("Fullbody") },
        { text: qsTr("Lowerbody") },
        { text: qsTr("Upperbody") },
        { text: qsTr("Wellbeing") },
    ]
    onActivated: {
        fc = currentIndex
    }
}
