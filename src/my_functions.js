
.pragma library

function convertToSeconds(h, m, s) {
    var totalSeconds = (h * 3600) + (m * 60) + Number(s)
    return totalSeconds
}

function formatTime(countdownValue, format) {
    countdownValue = countdownValue.toFixed(0)
    var hours = Math.floor(countdownValue / 3600)
    var minutes = Math.floor((countdownValue % 3600) / 60)
    var seconds = (countdownValue % 60)
    var fMinutes = minutes >= 10 ? minutes : "0" + minutes
    var fSeconds = seconds >= 10 ? seconds : "0" + seconds

    switch (format) {
        case 'h':
            return hours;
        case 'm':
            return minutes;
        case 'fm':
            return fMinutes;
        case 's':
            return seconds;
        case 'fs':
            return fSeconds
        case 'a':
            if (hours > 0)
                return hours + ":" + fMinutes + ":" + fSeconds;
            if (minutes > 0)
                return minutes + ":" + fSeconds;
            else
                return seconds;
    }
}
