import QtQuick
import QtQuick.Controls

SpinBox {
    id: control
    value: val
    editable: true
    height: 30
    to: 99
    from: 1
    onValueModified: val = control.value
    contentItem: TextInput {
        z: 2
        text: control.value
        font: control.font
        color: "lime"
        selectionColor: "dodgerblue"
        selectedTextColor: "lightgray"
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter
        readOnly: !control.editable
        validator: control.validator
        inputMethodHints: Qt.ImhFormattedNumbersOnly
    }
    up.indicator: Rectangle {
        x: control.mirrored ? 0 : parent.width - width
        height: parent.height
        implicitWidth: 35
        //color: control.up.pressed ? "salmon" : "blue"
        color: "blue"
        border.color: enabled ? "yellow" : "white"
        Text {
            text: "\uF055"
            font.family: fontLoader.name
            font.pixelSize: control.font.pixelSize * 2
            color: "lime"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }
    down.indicator: Rectangle {
        x: control.mirrored ? parent.width - width : 0
        height: parent.height
        implicitWidth: 35
        //color: control.up.pressed ? "salmon" : "blue"
        color: "blue"
        border.color: enabled ? "yellow" : "white"
        anchors.right: control.left
        Text {
            text: "\uF056"
            font.family: fontLoader.name
            font.pixelSize: control.font.pixelSize * 2
            color: "lime"
            anchors.fill: parent
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                control.decrease()
            }
        }
    }
    background: Rectangle {
        implicitWidth: 75
        border.color: "yellow"
        color: "black"
    }
}
